# template_vue

## Introduction
- 本项目基于vue的一个模板
- node v14.3.0
- npm 6.14.5
- yarn 1.16.0
- 代码规范: eslint+husky+prettier+lint-staged

## Project start
```
yarn
yarn dev
```

## Project package
```
yarn build
```

### Lints and fixes files
```
# 检查代码格式规范
yarn lint
# 代码自动格式化
yarn lint:fix
```



### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

___ 
### 项目所用技术
+ [Vue 2.6.11](https://cn.vuejs.org/v2)
+ [Vue-router 3.4.7](//router.vuejs.org/zh-cn)
+ [Vuex 3.5.1](//vuex.vuejs.org/zh-cn/)
+ [Es6](http://es6.ruanyifeng.com/)
+ [Webpack 4.35.0](//www.css88.com/doc/webpack2/guides/development/)
+ [Element-ui 2.13.0](http://element.eleme.io/)
+ [Mock](http://mockjs.com/examples.html)
+ [Node v14.3.0](https://nodejs.org/en/)
+ [Less](https://www.qikegu.com/docs/2663)
+ [Commintlint](https://commitlint.js.org/#/)

### Commintlint字段说明

| 枚举字段 | 说明 |
| :---| :--- |
|feat |新增功能 |
|fix |修复功能 |
|update |更新功能 |
|docs |更新文档 |
|style |更新样式 |
|refactor |重构 |
|test | 测试功能 |
|chore | 非功能调整 |
|release |发布代码 |
|revert |回滚代码 |


