import Mock from "mockjs"
import Demo from "./demo"
import User from "./user"
import Permission from "./permission"

const json = [
  ...Demo, ...User, ...Permission
]

json.forEach(el => {
  Mock.mock(el.url, el.method ? el.method : 'get', el.data);
})
