export default [{
  // 列表demo
  "url": "demo/power/get_menu",
  "method": 'get',
  "data": {
    "list": [
      {
        id: 1,
        name: "生产管理",
        pid: 1,
        path: "/production",
        nature: 0
      }, {
        id: 2,
        pid: 0,
        name: "物料管理",
        path: "/materiel",
        nature: 0,
      },
      {
        id: 3,
        pid: 0,
        name: "财务管理",
        path: "/finance",
        nature: 0,
      },
      {
        id: 4,
        pid: 0,
        name: "审核页面",
        path: "/examine",
        nature: 0,
      },
      {
        id: 5,
        pid: 0,
        name: "账号管理",
        path: "/account",
        nature: 0,
        children: [
          {
            id: 6,
            pid: 0,
            nature: 0,
            name: "部门管理",
            path: "/account/departmentManage"
          },
          {
            id: 7,
            pid: 0,
            nature: 0,
            name: "员工管理",
            path: "/account/employeeManage"
          },
          {
            id: 8,
            pid: 0,
            nature: 0,
            name: "角色管理",
            path: "/account/roleManage"
          },
          {
            id: 9,
            pid: 0,
            nature: 0,
            name: "菜单管理",
            path: "/account/menuManage"
          },
          {
            id: 10,
            pid: 0,
            nature: 0,
            name: "权限管理",
            path: "/account/powerManage"
          }
        ]
      }
    //   {
    //   name: "生产管理",
    //   path: "/production",
    // }, {
    //   name: "物料管理",
    //   path: "/materiel",
    // },
    // {
    //   name: "财务管理",
    //   path: "/finance",
    // },
    // {
    //   name: "审核页面",
    //   path: "/examine",
    // },
    // {
    //   name: "账号管理",
    //   path: "/account",
    //   children: [
    //     {
    //       name: "权限管理",
    //       path: "/account",
    //       children: [
    //         {
    //           name: "部门管理",
    //           path: "/account/departmentManage"
    //         },
    //         {
    //           name: "员工管理",
    //           path: "/account/employeeManage"
    //         },
    //         {
    //           name: "角色管理",
    //           path: "/account/roleManage"
    //         },
    //         {
    //           name: "菜单管理",
    //           path: "/account/menuManage"
    //         },
    //         {
    //           name: "权限管理",
    //           path: "/account/powerManage"
    //         }
    //       ]
    //     }
    //   ]}
    ]
  }
}]
