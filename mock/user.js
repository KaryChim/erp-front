export default [
  {
    "url": "demo/login",
    "method": 'post',
    "data": {
      "code": 20000,
      "message": "登录成功",
      "result": {
        "auth": [
          {
            "authority": "ROLE_ADMIN"
          }
        ],
        "menu": [
          {
            id: 1,
            name: "生产管理",
            pid: 1,
            path: "/production",
            nature: 0
          }, {
            id: 2,
            pid: 0,
            name: "物料管理",
            path: "/materiel",
            nature: 0,
          },
          {
            id: 3,
            pid: 0,
            name: "财务管理",
            path: "/finance",
            nature: 0,
          },
          {
            id: 4,
            pid: 0,
            name: "审核页面",
            path: "/examine",
            nature: 0,
          },
          {
            id: 5,
            pid: 0,
            name: "账号管理",
            path: "/account",
            nature: 0,
            children: [
              {
                id: 6,
                pid: 0,
                nature: 0,
                name: "部门管理",
                path: "/account/departmentManage"
              },
              {
                id: 7,
                pid: 0,
                nature: 0,
                name: "员工管理",
                path: "/account/employeeManage"
              },
              {
                id: 8,
                pid: 0,
                nature: 0,
                name: "角色管理",
                path: "/account/roleManage"
              },
              {
                id: 9,
                pid: 0,
                nature: 0,
                name: "菜单管理",
                path: "/account/menuManage"
              },
              {
                id: 10,
                pid: 0,
                nature: 0,
                name: "权限管理",
                path: "/account/powerManage"
              }
            ]
          }
        ],
        "username": "admin",
        // eslint-disable-next-line max-len
        "token": "eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE2MDYyNDU0MzIsInN1YiI6ImFkbWluIiwiaWF0IjoxNjA2MTU5MDMyMDQ0fQ.gjTBpLrfQeBykFiYuGeosC4w9Mf-FgaS6ap4EIkOySokyVG2W9zaFlQMO-S2HdE2EwCBcFC4wq0be6IXp38hCg"
      }
    }
  }, {
    "url": "demo/logout",
    "method": 'get',
    "data": {
      "code": 20000,
      "message": "退出成功",
      "result": true
    }
  },
  {
    "url": "demo/user/get_user_info",
    "method": 'get',
    "data": {
      "id|+1": 1,
      "username": "@cname",
      "access_token": "@string('lower', 20)",
      "power": [
        {
          "path": "/user",
          "name": "用户管理",
          "power": 1
        },
        {
          "path": "/permission",
          "name": "权限管理",
          "power": 1
        }
      ]
    }
  }
]
