// import {
//   http,
//   apiPath
// } from "@/api"
import created from "mixins/created.js"
import billexamined from '../billexamine/billexamined/index.vue';
import billnotexamine from './billnotexamine/index.vue';

export default created({
  props: {},
  components: {
    billexamined,
    billnotexamine,
  },
  mixins: [],
  data() {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js " + name + " App",
      examineBillNavArr: ['待审核', '审核通过'],
      billexaminedflag: false,
      billnotexamineflag: true,
    }
  },
  computed: {},
  created() {},
  destroyed() {},
  watch: {},
  methods: {
    // eslint-disable-next-line no-unused-vars
    handleSelect(key, keyPath) {
      if (key == '1-0') {
        this.billexaminedflag = false;
        this.billnotexamineflag = true;
      } else {
        this.$refs.mychild.getDate(); //调用子组件方法
        this.billexaminedflag = true;
        this.billnotexamineflag = false;
      }
    }
  }
})