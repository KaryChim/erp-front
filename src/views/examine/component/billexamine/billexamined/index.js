import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
import showinstockother from '../showbill/showinstockother/index.vue'
import showinstockproduct from '../showbill/showinstockproduct/index.vue'
import showinstockprofits from '../showbill/showinstockprofits/index.vue'
import showinstockpurchase from '../showbill/showinstockpurchase/index.vue'
import showoutstockdraw from '../showbill/showoutstockdraw/index.vue'
import showoutstockloss from '../showbill/showoutstockloss/index.vue'
import showoutstockother from '../showbill/showoutstockother/index.vue'
import showoutstocksale from '../showbill/showoutstocksale/index.vue'

export default created({
  props: {},
  components: {
    HelloWorld,
    showinstockother,
    showinstockproduct,
    showinstockprofits,
    showinstockpurchase,
    showoutstockdraw,
    showoutstockloss,
    showoutstockother,
    showoutstocksale
  },
  mixins: [],
  data() {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js " + name + " App",
      headers: [],
      bodies: [],
      loading: false,
      page: {
        currentPage: 1,
        pageSize: 10,
        total: 100
      },
      dialogVisible: false,
    }
  },
  computed: {},
  created() {},
  destroyed() {},
  watch: {},
  methods: {
    //获取单据数据
    async getDate() {
      this.loading = true;
      // eslint-disable-next-line max-len
      let params = Object.assign({ page: this.page.currentPage, size: this.page.pageSize, auditStatus: '审核通过' })
      http(apiPath.examine.billList, params, "GET").then(res => {
        const { headers } = res.result.table;
        const { bodies } = res.result.table;
        this.headers = headers;
        this.bodies = bodies;
        this.page.total = res.result.total;
        this.loading = false;
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //改变每页大小
    handleSizeChange(val) {
      this.page.pageSize = val
      this.getDate();
    },
    //跳转页数
    handleCurrentChange(val) {
      this.page.currentPage = val
      this.getDate();
    },
    //点击某行打开窗口
    rowClick(row) {
      console.log(row.documentNo);
      this.dialogVisible = true;
      console.log(row.documentType)
      switch (row.documentType) {
      case '其他入库':
        this.$refs['showinstockother'].toggle(row.documentNo);
        break;
      case '产品入库':
        this.$refs['showinstockproduct'].toggle(row.documentNo);
        break;
      case '盘盈入库':
        this.$refs['showinstockprofits'].toggle(row.documentNo);
        break;
      case '采购入库':
        this.$refs['showinstockpurchase'].toggle(row.documentNo);
        break;
      case '领料出库':
        this.$refs['showoutstockdraw'].toggle(row.documentNo);
        break;
      case '盘亏毁损':
        this.$refs['showoutstockloss'].toggle(row.documentNo);
        break;
      case '其他出库':
        this.$refs['showoutstockother'].toggle(row.documentNo);
        break;
      case '销售出库':
        this.$refs['showoutstocksale'].toggle(row.documentNo);
        break;
      }
    },
  },
  mounted() {
    this.getDate();
  }
})