/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"

const cityOptions = ['materialName', 'materialSap', 'warehouseName', 'batchNumber', 'unit', 'unitPrice', 'surplusLossNum', 'surplusLossMoney', 'remark', 'productionDate', 'materialExpirationDate', 'expiryDate', 'actualQuantity', 'depositQuantity'];
export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data() {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js " + name + " App",
      allow: [{
        value: true,
        label: "已审核"
      }, {
        value: false,
        label: "未审核"
      },],
      redSheet: [{
        value: true,
        label: "红单"
      }, {
        value: false,
        label: "蓝单"
      },],
      dialogVisible: false,
      checkAll: true, //默认全选
      checkedCities: ['materialName', 'materialSap', 'warehouseName', 'batchNumber', 'unit', 'unitPrice', 'surplusLossNum', 'surplusLossMoney', 'remark', 'productionDate', 'materialExpirationDate', 'expiryDate', 'actualQuantity', 'depositQuantity'],
      isIndeterminate: false, //默认全选
      dialogFormVisible: false,
      ruleForm: {
        documentType: '盘盈入库', //单据类型
        documentNo: '', //单据编号
        currency: '', //币别
        allow: 'false', //审核标志
        manager: '', //主管
        examineEmployee: '', //审核人
        documentCreator: '', //制单人
        handingEmployee: '', //经办人
        materialsDetail: [
          {
            materialName: '',
            materialSap: '',
            warehouseName: '',
            batchNumber: '',
            unit: '',
            unitPrice: '',
            surplusLossNum: '',
            surplusLossMoney: '',
            remark: '',
            productionDate: '',
            materialExpirationDate: '',
            expiryDate: '',
            actualQuantity: '',
            depositQuantity: '',
            show: false
          },
        ],
      },
      rules: {
        documentType: [
          {required: true, message: '请选择单据类型', trigger: 'change'}
        ],
        documentNo: [
          {required: true, message: '请输入单号', trigger: 'change'}
        ],
        documentDate: [
          {type: 'string', required: true, message: '请选择日期', trigger: 'change'}
        ],
      },
      columns: [{
        prop: 'materialName',
        label: '物料名称'
      }, {
        prop: 'materialSap',
        label: '规格型号'
      }, {
        prop: 'warehouseName',
        label: '收料仓库'
      }, {
        prop: 'batchNumber',
        label: '批号'
      }, {
        prop: 'unit',
        label: '单位'
      }, {
        prop: 'unitPrice',
        label: '单价'
      }, {
        prop: 'surplusLossNum',
        label: '盘盈数量(*)'
      }, {
        prop: 'surplusLossMoney',
        label: '盘盈金额'
      }, {
        prop: 'remark',
        label: '备注'
      }, {
        prop: 'productionDate',
        label: '生产/采购日期'
      }, {
        prop: 'materialExpirationDate',
        label: '保质期（天）'
      }, {
        prop: 'expiryDate',
        label: '有效期到'
      }, {
        prop: 'actualQuantity',
        label: '实存数量'
      }, {
        prop: 'depositQuantity',
        label: '账存数量'
      },
      ],
      tempCols: [{
        prop: 'materialName',
        label: '物料名称'
      }, {
        prop: 'materialSap',
        label: '规格型号'
      }, {
        prop: 'warehouseName',
        label: '收料仓库'
      }, {
        prop: 'batchNumber',
        label: '批号'
      }, {
        prop: 'unit',
        label: '单位'
      }, {
        prop: 'unitPrice',
        label: '单价'
      }, {
        prop: 'surplusLossNum',
        label: '盘盈数量(*)'
      }, {
        prop: 'surplusLossMoney',
        label: '盘盈金额'
      }, {
        prop: 'remark',
        label: '备注'
      }, {
        prop: 'productionDate',
        label: '生产/采购日期'
      }, {
        prop: 'materialExpirationDate',
        label: '保质期（天）'
      }, {
        prop: 'expiryDate',
        label: '有效期到'
      }, {
        prop: 'actualQuantity',
        label: '实存数量'
      }, {
        prop: 'depositQuantity',
        label: '账存数量'
      },
      ],

    }
  },
  computed: {},
  created() {
  },
  destroyed() {
  },
  watch: {},
  methods: {
    toggle(documentNo) {
      this.getHead(documentNo)
    },
    async getHead(documentNo) {
      http(apiPath.examine.billsDetail(documentNo), {}, "GET").then(res => {
        for (var item in this.ruleForm) {
          this.ruleForm[item] = res.result.table.bodies[0][item]
        }
        this.ruleForm.materialsDetail = res.result.table.bodies[0].details.table.bodies
        this.dialogVisible = !this.dialogVisible
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //全选按钮
    handleCheckAllChange(val) {
      this.checkedCities = val ? cityOptions : [];
      this.isIndeterminate = false;
    },
    //单个选中
    handleCheckedCitiesChange(value) {
      let checkedCount = value.length;
      this.checkAll = checkedCount === this.columns.length;
      this.isIndeterminate = checkedCount > 0 && checkedCount < this.columns.length;
    },
    showData(row) {
      this.dialogFormVisible = true;
    },
    confirmShow() {
      this.tempCols = [];
      if (this.checkedCities.length < 1) {
        this.$message.warning("请至少选择一项数据");
        this.dialogFormVisible = true;
      } else {
        for (var i = 0; i < this.columns.length; i++) {
          for (var j = 0; j < this.checkedCities.length; j++) {
            if (this.columns[i].prop == this.checkedCities[j]) {
              this.tempCols.push(this.columns[i]);
            }
          }
        }
        console.log(this.tempCols);
        //this.cols = this.tempCols;
        this.dialogFormVisible = false;
      }
    },
    //回填物料规格型号
    handleSelect(item, index) {
      console.log(item);
      index.materialSap = item.materialSap;
    },
    //删行
    deleteRow(index, rows) {
      rows.splice(index, 1);
    },
    //添行
    addRow() {
      const newRow = {
        materialName: '',
        materialSap: '',
        warehouseName: '',
        batchNumber: '',
        unit: '',
        unitPrice: '',
        surplusLossNum: '',
        surplusLossMoney: '',
        remark: '',
        productionDate: '',
        materialExpirationDate: '',
        expiryDate: '',
        actualQuantity: '',
        depositQuantity: '',
        show: false
      };
      this.ruleForm.materialsDetail.push(newRow);
    },
    //提交数据
    async onSumit(ref) {
      this.$refs[ref].validate(async (valid) => {
        if (valid) {
          const data = await this.$axios.post(this.$interface.BILLS.createBills, this.ruleForm);
          this.$message.success(data.message);
        } else
          this.$message({showClose: true, message: '提交失败', type: 'error', offset: 40});
      });

    },
    //获取单号
    async getDate() {
      let params = Object.assign({billsType: '盘盈入库'})
      http(apiPath.bills.documentNo, params, "GET").then(res => {
        const documentNo = res.result;
        this.ruleForm.documentNo = documentNo;
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //删除单据
    async deleteBill() {
      const data = await this.$axios.delete(this.$interface.BILLS.deleteBill(this.ruleForm.documentNo));
      this.$message.success(data.message);
      this.$emit('func'); //重新刷新分页内容
    },
    //模糊查询员工
    async querySearchAsync(queryString, cb) {
      let params = queryString ? Object.assign({size: 50}, {keyWord: queryString}) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then(res => {
        const {result} = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询物币别
    async searchCurrencyAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.currency, params, "GET").then(res => {
        const {result} = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询物料名
    async searchMaterialAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.material, params, "GET").then(res => {
        const {result} = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询仓库名
    async searchWarehouseNameAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.warehouse, params, "GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询计量单位
    async searchUnitAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.unit, params, "GET").then(res => {
        const {result} = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
  }
})
