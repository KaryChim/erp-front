/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"

const cityOptions = ['materialName', 'materialSap', 'warehouseName', 'batchNumber', 'unit', 'unitPrice', 'receivedNum', 'amountMoney', 'remark', 'productionDate', 'purchaseDate', 'materialExpirationDate', 'expiryDate', 'singleContainerNumber', 'containerNumber'];
export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data() {
    return {
      allow: [{
        value: true,
        label: "已审核"
      }, {
        value: false,
        label: "未审核"
      },],
      redSheet: [{
        value: true,
        label: "红单"
      }, {
        value: false,
        label: "蓝单"
      },],
      dialogVisible: false,
      loading: false,
      timeout: null,
      checkAll: true, //默认全选
      checkedCities: ['materialName', 'materialSap', 'warehouseName', 'batchNumber', 'unit', 'unitPrice', 'receivedNum', 'amountMoney', 'remark', 'productionDate', 'purchaseDate', 'materialExpirationDate', 'expiryDate', 'singleContainerNumber', 'containerNumber'],
      isIndeterminate: false, //默认全选
      dialogFormVisible: false,
      ruleForm: {
        documentType: '采购入库', //单据类型
        supplier: '', //供应商
        documentDate: '', //日期
        documentNo: '', //单据编号
        redSheet: "false", //红蓝单
        procureMethod: '赊购', //采购方式
        allow: "false", //审核标志
        digest: '', //摘要
        manager: '', //主管
        department: '', //部门
        checkEmployee: '', //验收人
        examineEmployee: '', //审核人
        documentCreator: '', //制单人
        handingEmployee: '', //经办
        safekeepEmployee: '', //保管人
        qualityEmployee: '', //品质
        approvalEmployee: '', //批准
        deliveryDriver: '', //送货司机
        salesman: '', //业务员
        materialsDetail: [
          {
            materialName: '',
            materialSap: '',
            warehouseName: '',
            batchNumber: '',
            unit: '',
            unitPrice: '',
            receivedNum: '',
            amountMoney: '',
            remark: '',
            productionDate: '',
            purchaseDate: '',
            materialExpirationDatealityDate: '',
            expiryDate: '',
            singleContainerNumber: '',
            containerNumber: '',
            show: false
          },
        ]
      },
      rules: {
        documentType: [
          {required: true, message: '请选择单据类型', trigger: 'change'}
        ],
        documentNo: [
          {required: true, message: '请输入单号', trigger: 'change'}
        ],
        documentDate: [
          {type: 'string', required: true, message: '请选择日期', trigger: 'change'}
        ],
      },
      columns: [{
        prop: 'materialName',
        label: '物料名称'
      },
      {
        prop: 'materialSap',
        label: '规格型号'
      },
      {
        prop: 'warehouseName',
        label: '仓库名称'
      },
      {
        prop: 'batchNumber',
        label: '批号'
      },
      {
        prop: 'unit',
        label: '单位'
      },
      {
        prop: 'unitPrice',
        label: '单价'
      },
      {
        prop: 'receivedNum',
        label: '实收数量'
      },

      {
        prop: 'amountMoney',
        label: '金额'
      },
      {
        prop: 'remark',
        label: '备注'
      },
      {
        prop: 'productionDate',
        label: '生产日期'
      },
      {
        prop: 'purchaseDate',
        label: '采购日期'
      },
      {
        prop: 'materialExpirationDate',
        label: '保质期（天）'
      },
      {
        prop: 'expiryDate',
        label: '有效期到'
      },
      {
        prop: 'singleContainerNumber',
        label: '单箱数'
      },
      {
        prop: 'containerNumber',
        label: '箱数'
      },
      ],
      tempCols: [{
        prop: 'materialName',
        label: '物料名称'
      },
      {
        prop: 'materialSap',
        label: '规格型号'
      },
      {
        prop: 'warehouseName',
        label: '仓库名称'
      },
      {
        prop: 'batchNumber',
        label: '批号'
      },
      {
        prop: 'unit',
        label: '单位'
      },
      {
        prop: 'unitPrice',
        label: '单价'
      },
      {
        prop: 'receivedNum',
        label: '实收数量'
      },

      {
        prop: 'amountMoney',
        label: '金额'
      },
      {
        prop: 'remark',
        label: '备注'
      },
      {
        prop: 'productionDate',
        label: '生产日期'
      },
      {
        prop: 'purchaseDate',
        label: '采购日期'
      },
      {
        prop: 'materialExpirationDate',
        label: '保质期（天）'
      },
      {
        prop: 'expiryDate',
        label: '有效期到'
      },
      {
        prop: 'singleContainerNumber',
        label: '单箱数'
      },
      {
        prop: 'containerNumber',
        label: '箱数'
      },
      ],
    }
  },
  computed: {},
  created() {
  },
  destroyed() {
  },
  watch: {},
  methods: {
    toggle(documentNo) {
      this.getHead(documentNo)
    },
    async getHead(documentNo) {
      http(apiPath.examine.billsDetail(documentNo), {}, "GET").then(res => {
        for (var item in this.ruleForm) {
          this.ruleForm[item] = res.result.table.bodies[0][item]
        }
        this.ruleForm.materialsDetail = res.result.table.bodies[0].details.table.bodies
        this.dialogVisible = !this.dialogVisible
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //全选按钮
    handleCheckAllChange(val) {
      this.checkedCities = val ? cityOptions : [];
      this.isIndeterminate = false;
    },
    //单个选中
    handleCheckedCitiesChange(value) {
      let checkedCount = value.length;
      this.checkAll = checkedCount === this.columns.length;
      this.isIndeterminate = checkedCount > 0 && checkedCount < this.columns.length;
    },
    showData(row) {
      this.dialogFormVisible = true;
    },
    confirmShow() {
      this.tempCols = [];
      if (this.checkedCities.length < 1) {
        this.$message.warning("请至少选择一项数据");
        this.dialogFormVisible = true;
      } else {
        for (var i = 0; i < this.columns.length; i++) {
          for (var j = 0; j < this.checkedCities.length; j++) {
            if (this.columns[i].prop == this.checkedCities[j]) {
              this.tempCols.push(this.columns[i]);
            }
          }
        }
        console.log(this.tempCols);
        //this.cols = this.tempCols;
        this.dialogFormVisible = false;
      }
    },
    //删行
    deleteRow(index, rows) {
      rows.splice(index, 1);
    },
    //添行
    addRow() {
      const newRow = {
        materialName: '',
        materialSap: '',
        warehouseName: '',
        batchNumber: '',
        unit: '',
        unitPrice: '',
        receivedNum: '',
        amountMoney: '',
        remark: '',
        productionDate: '',
        materialExpirationDatealityDate: '',
        expiryDate: '',
        singleContainerNumber: '',
        containerNumber: '',
        show: false
      };
      this.ruleForm.materialsDetail.push(newRow);
    },
    //回填物料规格型号
    handleSelect(item, index) {
      console.log(item);
      index.materialSap = item.materialSap;
    },
    //提交数据
    async onSumit(ref) {
      this.$refs[ref].validate(async (valid) => {
        if (valid) {
          const data = await this.$axios.post(this.$interface.BILLS.createBills, this.ruleForm);
          this.$message.success(data.message);
        } else
          this.$message({showClose: true, message: '提交失败', type: 'error', offset: 40});
      });

    },
    //获取数据
    async getDate() {
      let params = Object.assign({billsType: '采购入库'})
      http(apiPath.bills.documentNo, params, "GET").then(res => {
        const documentNo = res.result;
        this.ruleForm.documentNo = documentNo;
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //删除单据
    async deleteBill() {
      const data = await this.$axios.delete(this.$interface.BILLS.deleteBill(this.ruleForm.documentNo));
      this.$message.success(data.message);
      this.$emit('func'); //重新刷新分页内容
    },
    //queryString 为在框中输入的值
    //callback 回调函数,将处理好的数据推回
    //实现模糊查询供应商
    async searchSupplierAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.supplier, params, "GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询送货司机
    async searchCourierAsync(queryString, callback) {
      var params = Object.assign({keyword: queryString})
      console.log(apiPath.fuzzy)
      http(apiPath.fuzzy.courier, params, "GET").then(res => {
        console.log(res.result);
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询部门
    async searchDepartmentAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.department, params, "GET").then(res => {
        const {result} = res;
        console.log(result);
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询员工
    async querySearchAsync(queryString, cb) {
      let params = queryString ? Object.assign({size: 50}, {keyWord: queryString}) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then(res => {
        const {result} = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询物料名
    async searchMaterialAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.material, params, "GET").then(res => {
        const {result} = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询仓库名
    async searchWarehouseNameAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.warehouse, params, "GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询计量单位
    async searchUnitAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.unit, params, "GET").then(res => {
        const {result} = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
  }
})
