import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"


export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data() {
    return {
      degree: [ //紧急程度
        {value: '正常', label: '正常'},
        {value: '加急', label: '加急'},
        {value: '特急', label: '特急'},
      ],
      status: [ //状态
        {value: '正常', label: '正常'},
        {value: '延后', label: '延后'},
        {value: '中止', label: '中止'},
        {value: '暂停', label: '暂停'},
      ],
      timeout: null, //可选框展示的时间
      createForm: { //新建计划填入的相关信息
        principalEmployeeNo: '',
        approvedEmployeeNo: '',
        closingDate: '',
        status: '',
        source: '',
        linkedOrder: '',
        emergencyLevel: '',
        customerName: '',
        remarks: '',
        taskMaterialVos: [],
      }, //创建信息，
      createFormRule: {
        linkedOrder: [
          {required: true, message: '请输入关联订单号', trigger: 'change'},
        ],
        status: [
          {required: true, message: '请输入状态', trigger: 'change'},
        ],
        emergencyLevel: [
          {required: true, message: '请输入紧急程度', trigger: 'change'},
        ],
        closingDate: [
          {required: true, message: '请输入完工日期', trigger: 'change'},
        ],
        customerName: [
          {required: true, message: '请输入客户简称', trigger: 'change'},
        ],
        principalEmployeeNo: [
          {required: true, message: '请输入负责人', trigger: 'change'},
        ],
        approvedEmployeeNo: [
          {required: true, message: '请输入审核人', trigger: 'change'},
        ]
      },
      formLabelWidth: '200px',
      dialogFormVisible: false,
      createFormRef: 'createForm',
      //成品表头
      productionTabHeader: [
        {key: 'productionName', label: '成品名称'},
        {key: 'productionNumber', label: '成品数量'}
      ],
      //物料表头
      materialTabHeader: [
        {key: 'materialCode', label: '物料代码'},
        {key: 'materialName', label: '物料名称'},
        {key: 'materialNumber', label: '物料数量'},
        {key: 'materialSap', label: '规格型号'},
        {key: 'materialType', label: '物料类型'},
        {key: 'materialUnit', label: '单位'},
        {key: 'remark', label: '备注'},
      ],
      taskProduction: [],
      taskMaterial: [],
      selectedMaterial: [],
      selectedProduction: [],
      selectPlanRow: []
    }
  },
  computed: {},
  created() {
  },
  destroyed() {
  },
  watch: {},
  methods: {
    // //预处理
    // pretreatment(ref){
    // this.$refs[ref].validate(async valid => {})
    // },
    //返回相关信息
    async sendForm(ref) {
      if (this.taskProduction.length == 0 || this.taskMaterial.length == 0) {
        this.$message.error('原料和成品不能为空');
      } else {
        this.$refs[ref].validate(async (valid) => {
          if (valid) {
            this.createForm.productionName = this.taskProduction[0].materialName;
            this.createForm.productionNumber = this.taskProduction[0].materialNumber;
            this.createForm.taskMaterialVos = this.createForm.taskMaterialVos.concat(this.taskMaterial);
            this.createForm.taskMaterialVos.push(this.taskProduction[0]);
            http(apiPath.plan.create, this.createForm, {}, "POST").then(res => {
              this.$message.success(res.message);
              this.toggle();
              return;
            }).catch(err => {
              // console.log(err)
              this.$message({
                message: err.message.content,
                type: 'error'
              });
            })
          } else
            this.$message({showClose: true, message: '提交失败', type: 'error', offset: 40});
        });

      }
    },
    //模糊查询客户
    async searchCustomerAsync(queryString, callback) {
      let params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.customer, params, "GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //负责人的名字选取
    async querySearchAsync(queryString, cb) {
      let params = queryString ? Object.assign({size: 50}, {keyWord: queryString}) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then(res => {
        const {result} = res; //res是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    toggle(selectPlanRow) {
      console.log(selectPlanRow)
      this.selectPlanRow = selectPlanRow
      this.getTaskInfo()
      this.dialogFormVisible = !this.dialogFormVisible
    },
    reset(ref) {
      console.log(ref);
      this.$refs[ref].resetFields();
    },
    //选择物料
    selectMaterial(rows) {
      // 用一个变量来存放被选中的index
      this.selectedMaterial = rows;
      //console.log(this.badCodeSelection);
    },
    //选择成品
    selectProduction(rows) {
      // 用一个变量来存放被选中的index
      this.selectedProduction = rows;
      //console.log(this.badCodeSelection);
    },
    //打开对话框，选择物料
    openDialog(name) {
      console.log(this.$refs[name]);
      this.$refs[name].toggle();
    },
    //接收原料子组件数据
    getSelectedMaterial(data) {
      for (var element in data) {

        if (this.taskMaterial.length == 0) {

          this.taskMaterial.push(this.pushMaterial(data[element], "原料"));
          continue;
        }
        for (var item in this.taskMaterial) {
          if (data[element].materialCode == this.taskMaterial[item].materialCode) {
            this.taskMaterial[item].materialNumber = parseInt(this.taskMaterial[item].materialNumber) + 1;
            break;
          } else {
            if (item == this.taskMaterial.length - 1) {
              this.taskMaterial.push(this.pushMaterial(data[element], "原料"));
            }
          }
        }
      }
      this.$message.success('添加成功');
      console.log("表格数据");
      console.log(this.taskMaterial);
    },
    getProduction(data) {
      for (var element in data) {
        if (this.taskProduction.length == 1) {
          this.$message.error("暂时只允许输入一件成品，");
          return;
        }
        if (this.taskProduction.length == 0) {
          this.taskProduction.push(this.pushMaterial(data[element], "成品"));
          continue;
        }
        for (var item in this.taskProduction) {
          if (data[element].materialCode == this.taskProduction[item].materialCode) {
            this.taskProduction[item].materialNumber = parseInt(this.taskProduction[item].materialNumber) + 1;
            break;
          } else {
            if (item == this.taskProduction.length - 1) {
              this.taskProduction.push(this.pushMaterial(data[element], "成品"));
            }
          }
        }
      }
      this.$message.success('添加成功');
      console.log("表格数据");
      console.log(this.taskProduction);
    },
    //接收物料子组件数据，添加物料到表格
    pushMaterial(data, type) {
      var temp = new Object;
      temp.materialCode = data.materialCode;
      temp.materialName = data.materialName;
      temp.materialNumber = 1;
      temp.materialSap = data.materialSap;
      temp.materialType = type;
      temp.materialUnit = data.materialUnit;
      temp.remark = '';
      return temp;
    },
    async analyzeBOM() {
      if (!this.taskProduction[0]) {
        this.$message.error('填入成品后才能进行分析');
        return;
      }
      var temp = new Object;
      temp.materialCode = this.taskProduction[0].materialCode;
      temp.materialNumber = this.taskProduction[0].materialNumber;
      var productionArray = [];
      productionArray.push(temp);
      http(apiPath.product.materialListForBomByCode, productionArray, "POST").then(res => {
        console.log(res);
        this.taskMaterial = [];
        this.taskMaterial = this.taskMaterial.concat(res.result);
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    deleteMaterialRows() {
      this.selectedMaterial.forEach((item) => {
        for (let i = 0; i < this.taskMaterial.length; i++) {
          if (this.taskMaterial[i].materialCode === item.materialCode) {
            this.taskMaterial.splice(i, 1);
            break;
          }
        }
      });
      this.selectedMaterial = [];
    },
    deleteProductionRows() {
      this.selectedProduction.forEach((item) => {
        for (let i = 0; i < this.taskProduction.length; i++) {
          if (this.taskProduction[i].materialCode === item.materialCode) {
            this.taskProduction.splice(i, 1);
            break;
          }
        }
      });
      this.selectedProduction = [];
    },
    //按照比例改变原料数量
    changeNumber(number) {
      console.log(number)
      for (var item in this.taskMaterialNumber) {
        console.log(this.taskProductionNumber[0].materialNumber)
        this.taskMaterial[item].materialNumber = Number(number) / Number(
          this.taskProductionNumber[0].materialNumber) * Number(this.taskMaterialNumber[item].materialNumber)
      }
    },
    //初始化回填
    async getTaskInfo() {
      console.log(this.selectPlanRow)
      let params = Object.assign({productionNo: this.selectPlanRow.productionNo})
      http(apiPath.examine.taskDetail, params, "GET").then(res => {
        this.taskProduction = [];
        this.taskMaterial = [];
        this.createForm.taskMaterialVos = [];
        for (var item in res.result) {
          this.createForm[item] = res.result[item]
        }
        for (var item1 in res.result.taskMaterialVos) {
          if (res.result.taskMaterialVos[item1].materialType == "成品")
            this.taskProduction.push(res.result.taskMaterialVos[item1]);
          else
            this.taskMaterial.push(res.result.taskMaterialVos[item1]);
        }
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
      // this.form.state=data.result.state;

      this.taskProductionNumber = JSON.parse(JSON.stringify(this.taskProduction))
      this.taskMaterialNumber = JSON.parse(JSON.stringify(this.taskMaterial))
    },
  }
})
