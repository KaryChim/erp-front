import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
import showbom from './showbom/index.vue'

export default created({
  props: {},
  components: {
    HelloWorld,
    showbom
  },
  mixins: [],
  data() {
    return {
      state: '',
      page: {
        currentPage: 1,
        pageSize: 10,
        total: 10
      },
      tableBodies: [],
      tableHeaders: [],
      select: [],
      tableHeight: 200,
      activeIndex: '未审核',
      loading: false,
    }
  },
  computed: {},
  created() {},
  destroyed() {},
  watch: {},
  methods: {
    async getList(key) {
      this.loading = true
      this.state = key
      let params = Object.assign({ page: this.page.currentPage, size: this.page.pageSize, auditStatus: this.state })
      http(apiPath.examine.bomList, params, "GET").then(res => {
        this.tableHeaders = res.result.table.headers;
        this.tableBodies = res.result.table.bodies;
        this.tableHeight = document.documentElement.clientHeight - this.$refs['table'].$el.getBoundingClientRect().top;
        this.page.total = res.result.total
        this.loading = false
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    getSelect(val) {
      this.select = []
      val.forEach(element => {
        this.select.push(element.bomId)
      });
    },
    async pass() {
      if (this.select.length === 0) { this.$message.error('请选择bom单后再进行审核'); return; }
      http(apiPath.examine.examineBomList, {}, "PUT").then(res => {
        this.$refs.ResultDialog.results = res.result;
        this.$refs.ResultDialog.toggle();
        this.select = [];
        this.getList(this.state)
        return;

      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    handleSizeChange(val) {
      this.page.pageSize = val;
      this.page.currentPage = 1;
      this.getList(this.state);
    },
    handleCurrentChange(val) {
      this.page.currentPage = val;
      this.getList(this.state);
    },
    // eslint-disable-next-line no-unused-vars
    rowClick(row, column, cell, event) {
      //console.log(row);
      this.$refs["showbom"].toggle(row.bomId);
    }
  },
  mounted() {
    this.state = '未审核'
    this.getList(this.state)
    this.tableHeight = document.documentElement.clientHeight - this.$refs['table'].$el.getBoundingClientRect().top - 220;
    console.log(this.tableHeight)
  }
})