import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"

export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data() {
    return {
      dialogFormVisible: false,
      BOMFormListLoading: true,
      BOMTableListLoading: true,
      BOMTableList: [], //BOM单表格内容表格数据
      BOMFormList: [], //BOM单表单内容表格数据
      BOMTableFiled: [], //BOM单表格内容表格头
      BOMFormFiled: [], //BOM单表单内容表格头
    }
  },
  computed: {},
  created() {},
  destroyed() {},
  watch: {},
  methods: {
    toggle(bomId) {
      console.log(bomId);
      this.getData(bomId);
      this.dialogFormVisible = true;
    },
    //获取两个表格内的数据
    async getData(bomId) {
      this.BOMTableListLoading = true;
      this.BOMFormListLoading = true;
      let params = Object.assign({ bomId: bomId })
      http(apiPath.examine.bomDetail, params, "GET").then(res => {
        const { result } = res;
        const bomTable = result.bom.table;
        const bomDetailTable = result.bomDetail.table;
        this.BOMFormFiled = bomTable.headers;
        this.BOMFormList = bomTable.bodies;
        this.BOMTableFiled = bomDetailTable.headers;
        this.BOMTableList = bomDetailTable.bodies;
        this.BOMTableListLoading = false;
        this.BOMFormListLoading = false;

      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
      return;
    }
  }
})