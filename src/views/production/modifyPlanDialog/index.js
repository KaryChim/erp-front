import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"
import {eventBus} from "common/socket/event";
import SearchProductionDialog from "./searchProductionDialog/index.vue"
import SearchMaterialDialog from "./searchMaterialDialog/index.vue"

export default created({
  props: {},
  components: {
    SearchProductionDialog,
    SearchMaterialDialog
  },
  mixins: [],
  data() {
    return {
      degree: [//紧急程度
        {value: '正常', label: '正常'},
        {value: '加急', label: '加急'},
        {value: '特急', label: '特急'},
      ],
      status: [//状态
        {value: '正常', label: '正常'},
        {value: '延后', label: '延后'},
        {value: '中止', label: '中止'},
        {value: '暂停', label: '暂停'},
      ],
      timeout: null,//可选框展示的时间
      createForm: {//新建计划填入的相关信息
        principalEmployeeNo: '',
        approvedEmployeeNo: '',
        productionNo: '',
        closingDate: '',
        status: '',
        source: '',
        linkedOrder: '',
        emergencyLevel: '',
        customerName: '',
        remarks: '',
        planMaterialDtoList: [],
      }, //创建信息，
      createFormRule: {
        linkedOrder: [
          {required: true, message: '请输入关联订单号', trigger: 'change'},
        ],
        status: [
          {required: true, message: '请输入状态', trigger: 'change'},
        ],
        emergencyLevel: [
          {required: true, message: '请输入紧急程度', trigger: 'change'},
        ],
        closingDate: [
          {required: true, message: '请输入完工日期', trigger: 'change'},
        ],
        customerName: [
          {required: true, message: '请输入客户简称', trigger: 'change'},
        ],
        principalEmployeeNo: [
          {required: true, message: '请输入负责人', trigger: 'change'},
        ],
        approvedEmployeeNo: [
          {required: true, message: '请输入审核人', trigger: 'change'},
        ]
      },
      formLabelWidth: '200px',
      dialogFormVisible: false,
      createFormRef: 'createForm',
      //成品表头
      productionTabHeader: [
        {key: 'productionName', label: '成品名称'},
        {key: 'productionNumber', label: '成品数量'}
      ],
      //物料表头
      materialTabHeader: [
        {key: 'materialCode', label: '物料代码'},
        {key: 'materialName', label: '物料名称'},
        {key: 'materialNumber', label: '物料数量'},
        {key: 'materialSap', label: '规格型号'},
        {key: 'materialType', label: '物料类型'},
        {key: 'materialUnit', label: '单位'},
        {key: 'remark', label: '备注'},
      ],
      planProduction: [],
      planMaterial: [],
      selectedMaterial: [],
      selectedProduction: [],
      selectPlanRow: []
    }
  },
  computed: {},
  created() {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate() {
      console.log("执行template具体业务操作")
    },
    toggle(selectPlanRow) {
      this.selectPlanRow = selectPlanRow
      this.createForm.productionNo = this.selectPlanRow.productionNo
      this.getTaskInfo()
      this.dialogFormVisible = !this.dialogFormVisible
    },
    //初始化回填
    getTaskInfo() {
      console.log(this.selectPlanRow);
      const params = {productionNo: this.selectPlanRow.productionNo}
      http(apiPath.plan.detail, params, "GET").then(res => {
        //工序回填信息
        this.planProduction = [];
        this.planMaterial = [];
        this.createForm.planMaterialDtoList = [];
        for (var item in res.result) {
          this.createForm[item] = res.result[item]
        }

        //this.form.state=data.result.state;
        for (item in res.result.planMaterialDtoList) {
          if (res.result.planMaterialDtoList[item].materialType == "成品")
            this.planProduction.push(res.result.planMaterialDtoList[item]);
          else
            this.planMaterial.push(res.result.planMaterialDtoList[item]);
        }
        this.planProductionNumber = JSON.parse(JSON.stringify(this.planProduction))
        this.planMaterialNumber = JSON.parse(JSON.stringify(this.planMaterial))
      })
    },
    //模糊查询客户
    searchCustomerAsync(keyword, callback) {
      let params = Object.assign({keyword});
      http(apiPath.fuzzy.customer, params, "GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());

      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询员工
    querySearchAsync(keyWord, callback) {
      let params = keyWord ? Object.assign({size: 50}, {keyWord}) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then(res => {
        const result = res.result.list;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    reset(ref) {
      console.log(ref);
      this.$refs[ref].resetFields();
    },
    //选择成品
    selectProduction(rows) {
      // 用一个变量来存放被选中的index
      this.selectedProduction = rows;
      //console.log(this.badCodeSelection);
    },
    //选择物料
    selectMaterial(rows) {
      // 用一个变量来存放被选中的index
      this.selectedMaterial = rows;
      //console.log(this.badCodeSelection);
    },
    //打开对话框，选择物料
    openDialog(name) {
      console.log(this.$refs[name]);
      this.$refs[name].toggle();
    },
    getProduction(data) {
      for (var element in data) {
        if (this.planProduction.length == 1) {
          this.$message.error("暂时只允许输入一件成品，");
          return;
        }
        if (this.planProduction.length == 0) {
          this.planProduction.push(this.pushMaterial(data[element], "成品"));
          continue;
        }
        for (var item in this.planProduction) {
          if (data[element].materialCode == this.planProduction[item].materialCode) {
            this.planProduction[item].materialNumber = parseInt(this.planProduction[item].materialNumber) + 1;
            break;
          } else {
            if (item == this.planProduction.length - 1) {
              this.planProduction.push(this.pushMaterial(data[element], "成品"));
            }
          }
        }
      }
      this.planProductionNumber = JSON.parse(JSON.stringify(this.planProduction))
      this.$message.success('添加成功');
      //console.log("表格数据");
      //console.log(this.planProduction);
    },
    //接收原料子组件数据
    getSelectedMaterial(data) {
      for (var element in data) {

        if (this.planMaterial.length == 0) {

          this.planMaterial.push(this.pushMaterial(data[element], "原料"));
          continue;
        }
        for (var item in this.planMaterial) {
          if (data[element].materialCode == this.planMaterial[item].materialCode) {
            this.planMaterial[item].materialNumber = parseInt(this.planMaterial[item].materialNumber) + 1;
            break;
          } else {
            if (item == this.planMaterial.length - 1) {
              this.planMaterial.push(this.pushMaterial(data[element], "原料"));
            }
          }
        }
      }
      this.$message.success('添加成功');
      console.log("表格数据");
      console.log(this.planMaterial);
    },
    //接收物料子组件数据，添加物料到表格
    pushMaterial(data, type) {
      var temp = new Object;
      temp.materialCode = data.materialCode;
      temp.materialName = data.materialName;
      temp.materialNumber = 1;
      temp.materialSap = data.materialSap;
      temp.materialType = type;
      temp.materialUnit = data.materialUnit;
      temp.remark = '';
      return temp;
    },
    deleteMaterialRows() {
      this.selectedMaterial.forEach((item) => {
        for (let i = 0; i < this.planMaterial.length; i++) {
          if (this.planMaterial[i].materialCode === item.materialCode) {
            this.planMaterial.splice(i, 1);
            break;
          }
        }
      });
      this.selectedMaterial = [];
    },
    deleteProductionRows() {
      this.selectedProduction.forEach((item) => {
        for (let i = 0; i < this.planProduction.length; i++) {
          if (this.planProduction[i].materialCode === item.materialCode) {
            this.planProduction.splice(i, 1);
            break;
          }
        }
      });
      this.selectedProduction = [];
    },
    //BOM单分析
    analyzeBOM() {
      if (!this.planProduction[0]) {
        this.$message.error('填入成品后才能进行分析');
        return;
      }
      let params = Object.assign({materialCode: this.planProduction[0].materialCode, materialNumber: this.planProduction[0].materialNumber});
      http(apiPath.product.materialBom, params, "POST").then(res => {
        this.planMaterial = [];
        this.planMaterial = this.planMaterial.concat(res.result);
        this.planMaterialNumber = [...this.planMaterial]
      })
    },
    //按照比例改变原料数量
    changeNumber(number) {
      console.log(number)
      setTimeout(() => {
        for (var item in this.planMaterialNumber) {
          console.log(this.planProductionNumber[0].materialNumber)
          this.planMaterial[item].materialNumber
            = Number(number) / Number(this.planProductionNumber[0].materialNumber) * Number(this.planMaterialNumber[item].materialNumber)
        }
      },200)
    },
    //提交表单数据
    sendForm(ref) {
      if (this.planProduction.length == 0 || this.planMaterial.length == 0) {
        this.$message.error('原料和成品不能为空');
      } else {
        this.$refs[ref].validate(async (valid) => {
          if (valid) {
            //console.log(this.createForm);
            this.createForm.planMaterialDtoList = [];
            this.createForm.planMaterialDtoList = this.createForm.planMaterialDtoList.concat(this.planMaterial);//把原料信息存入
            this.createForm.planMaterialDtoList.push(this.planProduction[0]);//成品只能有一件，所以只添加第一件
            //console.log(this.createForm.planMaterialDtoList);
            const params = this.createForm;
            http(apiPath.plan.update, params, "POST").then(res => {
              this.$message.success(res.message);
              this.dialogFormVisible = !this.dialogFormVisible
            })
          } else
            this.$message({showClose: true, message: '提交失败', type: 'error', offset: 40});
        });
      }
    },

  }
})
