import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
import {eventBus} from "common/socket/event";
import SearchProductionDialog from "./searchProductionDialog/index.vue"
import SearchMaterialDialog from "./searchMaterialDialog/index.vue"

export default created({
  props: {},
  components: {
    HelloWorld,
    SearchProductionDialog,
    SearchMaterialDialog
  },
  mixins: [],
  data () {
    return {
      dialogFormVisible:false,
      formRef:'form',
      selectedMaterial:[],//勾选要删除的物料
      selectedProduction:[],
      taskProduction:[],
      taskMaterial:[],
      taskProductionNumber:[],
      taskMaterialNumber:[],
      //新建工序表单
      form:{
        productionName:'',
        productionNumber:'',
        productionPlanNo:'',
        taskName:'',
        operatorEmployeeNo:'',
        forecastStartDate:'',
        forecastEndDate:'',
        remark:'',
        taskMaterialVos:[],
        //publicTaskDetail
      },
      formRules:{
        productionPlanNo: [
          {required: true, message: '请输入任务单号', trigger: 'change'},
        ],
        taskName: [
          {required: true, message: '请输入工序名', trigger: 'change'},
        ],
        operatorEmployeeNo: [
          {required: true, message: '请输入负责人', trigger: 'change'},
        ],
        forecastStartDate:[
          { type: 'string',required: true, message: '请输入开始时间', trigger: 'change'},
        ],
        forecastEndDate:[
          { type: 'string',required: true, message: '请输入结束时间', trigger: 'change'},
        ],
      },
      //成品表头
      productionTabHeader:[
        {key:'productionName',label:'成品名称'},
        {key:'productionNumber',label:'成品数量'}
      ],
      //物料表头
      materialTabHeader:[
        {key:'materialCode',label:'物料代码'},
        {key:'materialName',label:'物料名称'},
        {key:'materialNumber',label:'物料数量'},
        {key:'materialSap',label:'规格型号'},
        {key:'materialType',label:'物料类型'},
        {key:'materialUnit',label:'单位'},
        {key:'remark',label:'备注'},
      ],
    }
  },
  computed: {},
  created () {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate(){
      console.log("执行template具体业务操作")
    },
    toggle(planNum){
      this.dialogFormVisible=!this.dialogFormVisible
      console.log(planNum);
      this.taskProduction=[];
      this.taskMaterial=[];
      this.form.productionPlanNo=planNum;//从父组件中接收计划单号
      this.reset(this.formRef);
    },
    //重置表单数据
    reset(ref){
      console.log(ref);
      this.taskProduction=[],
      this.taskMaterial=[],
      this.$refs[ref].resetFields();
    },
    //提交表单数据
    sendForm(ref){
      if(this.taskProduction.length==0||this.taskMaterial.length==0){
        this.$message.error('原料和成品不能为空');
      }
      else{
        this.$refs[ref].validate(async (valid) => {
          if(valid){
            this.form.productionName=this.taskProduction[0].materialName;
            this.form.productionNumber=this.taskProduction[0].materialNumber;
            this.form.taskMaterialVos=this.form.taskMaterialVos.concat(this.taskMaterial);
            this.form.taskMaterialVos.push(this.taskProduction[0]);
            //console.log(this.taskMaterial);
            const params=this.form
            http(apiPath.task.createTask,params,"POST").then(res => {
              this.$emit('func',1,10,this.form.productionPlanNo);
              this.$message.success(res.message);
              this.dialogFormVisible=false;
            })
          }
          else
            this.$message({showClose: true, message: '提交失败',type: 'error',offset:40});
        });
      }
    },
    //模糊查询工序名称
    searchTaskName(queryString, cb){
      //taskName
      const params={keyword:queryString};
      http(apiPath.fuzzy.taskName,params,"GET").then(res => {
        let {result} = res;//data是后端返回的数据
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      })
    },
    //选择工序后自动回填publicTaskDetail
    selectTaskName(){
      this.taskProduction=[];
      this.taskMaterial=[];
      const params={taskName:this.form.taskName};
      http(apiPath.task.publicTaskDetail,params,"GET").then(res => {
        for(var item in res.result.taskMaterialVos){
          console.log(res)
          if(res.result.taskMaterialVos[item].materialType=="成品")
            this.taskProduction.push(res.result.taskMaterialVos[item]);
          else
            this.taskMaterial.push(res.result.taskMaterialVos[item]);
        }
        this.taskProductionNumber=JSON.parse(JSON.stringify(this.taskProduction))
        this.taskMaterialNumber=JSON.parse(JSON.stringify(this.taskMaterial))
      })
    },
    //模糊查询员工
    querySearchAsync(keyWord, callback) {
      let params = Object.assign({size: 50},{keyWord});
      http(apiPath.employee.fuzzy,params,"GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //接收原料子组件数据
    getSelectedMaterial(data){
      for(var element in data){

        if(this.taskMaterial.length==0)
        {

          this.taskMaterial.push(this.pushMaterial(data[element],"原料"));
          continue;
        }
        for(var item in this.taskMaterial)
        {
          if(data[element].materialCode==this.taskMaterial[item].materialCode){
            this.taskMaterial[item].materialNumber=parseInt(this.taskMaterial[item].materialNumber)+1;
            break;
          }
          else {
            if(item==this.taskMaterial.length-1)
            {
              this.taskMaterial.push(this.pushMaterial(data[element],"原料"));
            }
          }
        }
      }
      this.$message.success('添加成功');
      console.log("表格数据");
      console.log(this.taskMaterial);
    },
    getProduction(data)
    {
      for(var element in data){
        if(this.taskProduction.length==1)
        {
          this.$message.error("暂时只允许输入一件成品，");
          return;
        }
        if(this.taskProduction.length==0)
        {
          this.taskProduction.push(this.pushMaterial(data[element],"成品"));
          continue;
        }
        for(var item in this.taskProduction)
        {
          if(data[element].materialCode==this.taskProduction[item].materialCode){
            this.taskProduction[item].materialNumber=parseInt(this.taskProduction[item].materialNumber)+1;
            break;
          }
          else {
            if(item==this.taskProduction.length-1)
            {
              this.taskProduction.push(this.pushMaterial(data[element],"成品"));
            }
          }
        }
      }
      this.taskProductionNumber=JSON.parse(JSON.stringify(this.taskProduction))
      this.$message.success('添加成功');
      //console.log("表格数据");
      //console.log(this.taskProduction);
    },
    //按照比例改变原料数量
    changeNumber(number){
      console.log(number)
      for(var item in this.taskMaterialNumber){
        console.log(this.taskProductionNumber[0].materialNumber)
        this.taskMaterial[item].materialNumber
        =Number(number)/Number(this.taskProductionNumber[0].materialNumber)*Number(this.taskMaterialNumber[item].materialNumber)
      }
    }
  }
})
