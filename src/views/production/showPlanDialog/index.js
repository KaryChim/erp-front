import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
import {eventBus} from "common/socket/event";

export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data () {
    return {
      degree: [//紧急程度
        {value: '正常', label: '正常'},
        {value: '加急', label: '加急'},
        {value: '特急', label: '特急'},
      ],
      status: [//状态
        {value: '正常', label: '正常'},
        {value: '延后', label: '延后'},
        {value: '中止', label: '中止'},
        {value: '暂停', label: '暂停'},
      ],
      timeout: null,//可选框展示的时间
      createForm: {//新建计划填入的相关信息
        principalEmployeeNo: '',
        approvedEmployeeNo: '',
        closingDate: '',
        status: '',
        source: '',
        linkedOrder: '',
        emergencyLevel: '',
        customerName: '',
        remarks: '',
        planMaterialDtoList:[],
      }, //创建信息，
      createFormRule: {
        linkedOrder: [
          {required: true, message: '请输入关联订单号', trigger: 'change'},
        ],
        status: [
          {required: true, message: '请输入状态', trigger: 'change'},
        ],
        emergencyLevel: [
          {required: true, message: '请输入紧急程度', trigger: 'change'},
        ],
        closingDate: [
          {required: true, message: '请输入完工日期', trigger: 'change'},
        ],
        customerName: [
          {required: true, message: '请输入客户简称', trigger: 'change'},
        ],
        principalEmployeeNo: [
          {required: true, message: '请输入负责人', trigger: 'change'},
        ],
        approvedEmployeeNo: [
          {required: true, message: '请输入审核人', trigger: 'change'},
        ]
      },
      formLabelWidth: '200px',
      dialogFormVisible: false,
      createFormRef: 'createForm',
      //成品表头
      productionTabHeader:[
        {key:'productionName',label:'成品名称'},
        {key:'productionNumber',label:'成品数量'}
      ],
      //物料表头
      materialTabHeader:[
        {key:'materialCode',label:'物料代码'},
        {key:'materialName',label:'物料名称'},
        {key:'materialNumber',label:'物料数量'},
        {key:'materialSap',label:'规格型号'},
        {key:'materialType',label:'物料类型'},
        {key:'materialUnit',label:'单位'},
        {key:'remark',label:'备注'},
      ],
      planProduction:[],
      planMaterial:[],
      selectedMaterial:[],
      selectedProduction:[],
      selectPlanRow:[]
    }
    
  },
  computed: {},
  created () {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate(){
      console.log("执行template具体业务操作")
    },
    toggle(selectPlanRow) {
      this.selectPlanRow=selectPlanRow
      this.getTaskInfo()
      this.dialogFormVisible = !this.dialogFormVisible
    },
    //初始化回填
    getTaskInfo(){
      console.log(this.selectPlanRow);
      const params ={productionNo:this.selectPlanRow.productionNo}
      http(apiPath.plan.detail,params,"GET").then(res => {
        //工序回填信息
        this.planProduction=[];
        this.planMaterial=[];
        this.createForm.planMaterialDtoList=[];
        for(var item in res.result){
          this.createForm[item]=res.result[item]
        }

        //this.form.state=data.result.state;
        for(item in res.result.planMaterialDtoList)
        {
          if(res.result.planMaterialDtoList[item].materialType=="成品")
            this.planProduction.push(res.result.planMaterialDtoList[item]);
          else
            this.planMaterial.push(res.result.planMaterialDtoList[item]);
        } 
        this.planProductionNumber=JSON.parse(JSON.stringify(this.planProduction))
        this.planMaterialNumber=JSON.parse(JSON.stringify(this.planMaterial))
      })
    },
  
  }
})
