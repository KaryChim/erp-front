import {
  http,
  apiPath
} from "@/api"

import created from "mixins/created.js"
import {eventBus} from "common/socket/event";
import SearchProductionDialog from "./searchProductionDialog/index.vue"
import SearchMaterialDialog from "./searchMaterialDialog/index.vue"

export default created({
  props: {},
  components: {
    SearchProductionDialog,
    SearchMaterialDialog
  },
  mixins: [],
  data () {
    return {
      dialogFormVisible:false,
      formRef:'form',
      selectedMaterial:[],//勾选要删除的物料
      selectedProduction:[],
      taskProduction:[],
      taskMaterial:[],
      //新建工序表单
      form:{
        productionName:'',
        productionNumber:'',
        passPercent:'',
        // productionPlanNo:'',
        taskName:'',
        // operatorEmployeeNo:'',
        //forecastStartDate:'',
        //forecastEndDate:'',
        remark:'',
        taskMaterialVos:[],
      },
      formRules:{
        // productionPlanNo: [
        //   {required: true, message: '请输入任务单号', trigger: 'change'},
        // ],
        passPercent:[
          { required: true, message: '请输入合格率', trigger: 'change'},
        ],
        taskName: [
          {required: true, message: '请输入工序名', trigger: 'change'},
        ],
        // operatorEmployeeNo: [
        //   {required: true, message: '请输入负责人', trigger: 'change'},
        // ],
        // forecastStartDate:[
        //   { type: 'string',required: true, message: '请输入开始时间', trigger: 'change'},
        // ],
        // forecastEndDate:[
        //   { type: 'string',required: true, message: '请输入结束时间', trigger: 'change'},
        // ]
      },
      //成品表头
      productionTabHeader:[
        {key:'productionName',label:'成品名称'},
        {key:'productionNumber',label:'成品数量'}
      ],
      //物料表头
      materialTabHeader:[
        {key:'materialCode',label:'物料代码'},
        {key:'materialName',label:'物料名称'},
        {key:'materialNumber',label:'物料数量'},
        {key:'materialSap',label:'规格型号'},
        {key:'materialType',label:'物料类型'},
        {key:'materialUnit',label:'单位'},
        {key:'remark',label:'备注'},
      ],
    }
  },
  computed: {},
  created () {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate(){
      console.log("执行template具体业务操作")
    },
    //重置表单数据
    reset(ref){
      console.log(ref);
      this.taskProduction=[],
      this.taskMaterial=[],   
      this.$refs[ref].resetFields();
    },
    toggle(planNum){
      this.dialogFormVisible=!this.dialogFormVisible
      console.log(planNum);
      this.taskProduction=[];
      this.taskMaterial=[];
      this.form.productionPlanNo=planNum;//从父组件中接收计划单号
    },
    selectMaterial(rows) {
      // 用一个变量来存放被选中的index
      this.selectedMaterial = rows;
      //console.log(this.badCodeSelection);
    },
    selectProduction(rows) {
      // 用一个变量来存放被选中的index
      this.selectedProduction = rows;
      //console.log(this.badCodeSelection);
    },
    deleteMaterialRows() {
      this.selectedMaterial.forEach((item) => {
        for (let i = 0; i < this.taskMaterial.length; i++) {
          if (this.taskMaterial[i].materialCode === item.materialCode) {
            this.taskMaterial.splice(i, 1);
            break;
          }
        }
      });
      this.selectedMaterial = [];
    },
    deleteProductionRows() {
      this.selectedProduction.forEach((item) => {
        for (let i = 0; i < this.taskProduction.length; i++) {
          if (this.taskProduction[i].materialCode === item.materialCode) {
            this.taskProduction.splice(i, 1);
            break;
          }
        }
      });
      this.selectedProduction = [];
    },
    //接收原料子组件数据
    getSelectedMaterial(data){
      for(var element in data){
        
        if(this.taskMaterial.length==0)
        {
          
          this.taskMaterial.push(this.pushMaterial(data[element],"原料"));
          continue;
        }
        for(var item in this.taskMaterial)
        {
          if(data[element].materialCode==this.taskMaterial[item].materialCode){
            this.taskMaterial[item].materialNumber=parseInt(this.taskMaterial[item].materialNumber)+1;
            break;
          }
          else {
            if(item==this.taskMaterial.length-1)
            {
              this.taskMaterial.push(this.pushMaterial(data[element],"原料"));
            }
          } 
        }
      }
      this.$emit('func');
      this.$message.success('添加成功');
      console.log("表格数据");
      console.log(this.taskMaterial);
    },
    getProduction(data)
    {
      for(var element in data){
        if(this.taskProduction.length==1)
        { 
          this.$message.error("暂时只允许输入一件成品");
          return;
        }
        if(this.taskProduction.length==0)
        {
          this.taskProduction.push(this.pushMaterial(data[element],"成品"));
          continue;
        }
        for(var item in this.taskProduction)
        {
          if(data[element].materialCode==this.taskProduction[item].materialCode){
            this.taskProduction[item].materialNumber=parseInt(this.taskProduction[item].materialNumber)+1;
            break;
          }
          else {
            if(item==this.taskProduction.length-1)
            {
              this.taskProduction.push(this.pushMaterial(data[element],"成品"));
            }
          } 
        }
      }
      this.taskProductionNumber=JSON.parse(JSON.stringify(this.taskProduction))
      this.$message.success('添加成功');
      console.log("表格数据");
      console.log(this.taskProduction);
    },
    //打开对话框，选择物料
    openDialog(name){
      console.log(this.$refs[name]);
      this.$refs[name].toggle();
    },
    //接收物料子组件数据，添加物料到表格
    pushMaterial(data,type){
      var temp = new Object; 
      temp.materialCode =data.materialCode; 
      temp.materialName =data.materialName;
      temp.materialNumber=1;
      temp.materialSap=data.materialSap;
      temp.materialType=type;
      temp.materialUnit=data.materialUnit;
      temp.remark='';
      return temp;
    },
    analyzeBOM(){
      if(!this.taskProduction[0]){this.$message.error('填入成品后才能进行分析');return;}
      var temp = new Object; 
      temp.materialCode=this.taskProduction[0].materialCode;
      temp.materialNumber=this.taskProduction[0].materialNumber;
      var productionArray=[];
      productionArray.push(temp);
      const params = productionArray;
      http(apiPath.product.materialListForBomByCode,params,"POST").then(res => {
        //console.log(res);
        this.taskMaterial=[];
        this.taskMaterial=this.taskMaterial.concat(res.result);
        this.taskMaterialNumber=[...this.taskMaterial]
      })
    },
    //按照比例改变原料数量
    changeNumber(number){
      // console.log(this.taskProductionNumber);
      // console.log(this.taskMaterialNumber);
      // console.log(this.taskMaterial);
      for(var item in this.taskMaterialNumber){
        this.taskMaterial[item].materialNumber
        =Number(number)/Number(this.taskProductionNumber[0].materialNumber)*Number(this.taskMaterialNumber[item].materialNumber)
      }
      //console.log(this.taskMaterial)
    },
    //提交表单数据
    sendForm(ref){ 
      if(this.taskProduction.length==0||this.taskMaterial.length==0){
        this.$message.error('原料和成品不能为空');
      }
      else{
        this.$refs[ref].validate(async (valid) => {
          if(valid){
            this.form.productionName=this.taskProduction[0].materialName;
            this.form.productionNumber=this.taskProduction[0].materialNumber;
            this.form.taskMaterialVos=this.form.taskMaterialVos.concat(this.taskMaterial);
            this.form.taskMaterialVos.push(this.taskProduction[0]);
            //console.log(this.taskMaterial);
            const params = this.form;
            http(apiPath.task.createPublicTask,params,"POST").then(res => {
              this.$emit('func',1,10,this.form.productionPlanNo);
              this.$message.success(res.message);
              this.dialogFormVisible=false;
            })            
          }
          else
            this.$message({showClose: true, message: '提交失败',type: 'error',offset:40});
        });
      }
    },
  }
})
