import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
import {eventBus} from "common/socket/event";

export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data () {
    return {
      dialogFormVisible:false,
      formRef:'form',
      selectedMaterial:[],//勾选要删除的物料
      selectedProduction:[],
      productionName:'',
      productionNumber:'',
      //新建工序表单
      form:{
        productionPlanNo:'',
        taskName:'',
        state:'',
        operatorEmployeeNo:'',
        forecastStartDate:'',
        forecastEndDate:'',
        passPercent:'',
        remark:'',
        taskProductionVos:[],
        taskMaterialVos:[],
      },
      formRules:{
        productionPlanNo: [
          {required: true, message: '请输入任务单号', trigger: 'change'},
        ],
        taskName: [
          {required: true, message: '请输入工序名', trigger: 'change'},
        ],
        operatorEmployeeNo: [
          {required: true, message: '请输入负责人', trigger: 'change'},
        ],
        forecastStartDate:[
          { type: 'string',required: true, message: '请输入开始时间', trigger: 'change'},
        ],
        forecastEndDate:[
          { type: 'string',required: true, message: '请输入结束时间', trigger: 'change'},
        ],
        passPercent:[
          { required: true, message: '请输入合格率', trigger: 'change'},
        ]
      },
      //成品表头
      productionTabHeader:[
        {key:'productionName',label:'成品名称'},
        {key:'productionNumber',label:'成品数量'}
      ],
      //物料表头
      materialTabHeader:[
        {key:'materialCode',label:'物料代码'},
        {key:'materialName',label:'物料名称'},
        {key:'materialNumber',label:'物料数量'},
        {key:'materialSap',label:'规格型号'},
        {key:'materialType',label:'物料类型'},
        {key:'materialUnit',label:'单位'},
        {key:'remark',label:'备注'},
      ],
    }
  },
  computed: {},
  created () {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate(){
      console.log("执行template具体业务操作")
    },
    toggle(planNum,taskNo){
      this.dialogFormVisible=!this.dialogFormVisible
      console.log(planNum);
      this.form.productionPlanNo=planNum;//从父组件中接收计划单号
      this.form.taskNo=taskNo;//从父组件中接收工序号
      this.getTaskInfo(taskNo);
    },
    getTaskInfo(){
      const params ={taskNo:this.form.taskNo}
      http(apiPath.task.taskDetail,params,"GET").then(res => {
        //工序回填信息
        this.form.taskName=res.result.taskName;
        this.form.operatorEmployeeNo=res.result.operatorEmployeeNo;
        this.form.forecastStartDate=res.result.forecastStartDate;
        this.form.forecastEndDate=res.result.forecastEndDate;
        this.form.passPercent=res.result.passPercent;
        this.form.remark=res.result.remark;
        this.form.state=res.result.state;
        this.form.passPercent=res.result.passPercent
        this.form.taskProductionVos=[];
        this.form.taskMaterialVos=[];
        for(var item in res.result.taskMaterialVos)
        {
          if(res.result.taskMaterialVos[item].materialType=="成品")
            this.form.taskProductionVos.push(res.result.taskMaterialVos[item]);
          else
            this.form.taskMaterialVos.push(res.result.taskMaterialVos[item]);
        }  
      })
    },
    //按照比例改变原料数量
    changeNumber(number){
      console.log(number)
      for(var item in this.taskMaterialNumber){
        console.log(this.taskProductionNumber[0].materialNumber)
        this.taskMaterial[item].materialNumber
        =Number(number)/Number(this.taskProductionNumber[0].materialNumber)*Number(this.taskMaterialNumber[item].materialNumber)
      }
    },
  }
})
