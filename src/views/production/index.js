import HelloWorld from 'components/HelloWorld';
import created from "mixins/created.js";
import CreatePlanDialog from './createPlanDialog/index.vue';
import DeletePlans from'./deleteDialog/index.vue';
import ModifyPlanDialog from'./modifyPlanDialog/index.vue';
import ShowPlanDialog from './showPlanDialog/index.vue';
import CreateTaskDialog from'./createTaskDialog/index.vue';
import ModifyTaskDialog from './modifyTaskDialog/index.vue';
import ShowTaskDialgo from './showTaskDialog/index.vue';
import BatchResultDialgo from'./batchResultDialog/index.vue';
import CreatePublicTaskDialog from './createPublishTaskDialgo/index.vue'
import ModifyPublicTaskDialog from './modifyPublishTaskDialog/index.vue'
import {
  http,
  apiPath
} from "@/api"

export default created({
  props: {
    addTab:Function
  },
  components: {
    HelloWorld,
    CreatePlanDialog,
    DeletePlans,
    ModifyPlanDialog,
    ShowPlanDialog,
    CreateTaskDialog,
    ModifyTaskDialog,
    ShowTaskDialgo,
    BatchResultDialgo,
    CreatePublicTaskDialog,
    ModifyPublicTaskDialog
  },
  mixins: [],
  data () {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js "+ name +" App",
      planNavArr:[
        '所有任务',
        '新任务',
        '生产中',
        '已完成'
      ],
      selectPlanType:'所有任务',//侧栏菜单选择计划类型,默认选中所有任务
      planButtonInfo: [
        { label: '新增任务', type: 'success', event: this.openDialog.bind(this, 'createPlanDialog') },
        { label: '复制任务', type: 'primary', event: this.copyPlans.bind(this, 'selectPlanRows') },
        { label: '删除任务', type: 'danger', event: this.deletePlans.bind(this, 'selectPlanRows') },
        // { label: '缺货分析', type: 'danger', event: this.openDialog.bind(this, 'analysisDialog') },
        // { label: '生产入库', type: 'warning', event: this.openDialog.bind(this, 'storageDialog') },
        // { label: '帮助' , event: this.openDialog.bind(this, 'helpDialog') }
      ],
      searchPlanNo:'',//查找计划，右上角搜索栏用
      planList:[],//请求获取计划列表
      planPageInfo:{total:0,current:1,pages:0,pageSize:10},//请求获取计划列表的分页信息
      taskPageInfo:{total:0,current:1,pages:0,pageSize:10},//请求获取工序列表的分页信息
      publicTasknPageInfo:{total:0,current:1,pages:0,pageSize:10},//请求获取计划列表的分页信息
      planListLoading:false,//模糊查询订单号
      planTableFiled:[
        {key:'id', label: '标识'}
        ,{key:'createEmployeeNo', label: '创建人'}
        ,{key:'principalEmployeeNo', label: '负责人'}
        ,{key:'productionNo', label: '任务单号'}
        ,{key:'createDate', label: '创建时间'}
        ,{key:'closingDate', label: '关闭时间'}
        ,{key:'auditStatus', label: '审核状态'}
        ,{key:'state', label: '状态'}
        ,{key:'status', label: '状况'}
        ,{key:'source', label: '来源'}
        ,{key:'linkedOrder', label: '关联单号'}
        ,{key:'remarks', label: '备注'}
      ],//映射任务表格label与后台字段
      taskList:[],//模糊查询订单号存放位置
      taskListLoading:false,
      finishProduct:{headers:[],bodies:[]},//成品表格
      semiFinished:{headers:[],bodies:[]},//成品表格
      rawMaterial:{headers:[],bodies:[]},//成品表格
      taskTableFiled:[
        {key:'id',label:'标识'},
        {key:'taskName',label:'名称'},
        {key:'state',label:'状态'},
        {key:'operatorEmployeeNo',label:'操作人'},
        {key:'forecastStartDate',label:'预计开始时间'},
        {key:'stateChangeDate',label:'最新状态变化时间'},
        {key:'forecastEndDate',label:'预计结束时间'},
        {key:'productionName',label:'成品名称'},
        {key:'productionNumber',label:'成品数量'},
        {key:'documentNo',label:'关联单据编号'},
        {key:'passPercent',label:'合格率'},
      ],//映射公休表格label与后台字段
      taskManageTable:{headers:[
        {key:'taskNo',label:'工序编号'},
        {key:'taskName',label:'工序名称'},
        {key:'productionName',label:'成品名称'},
        {key:'productionNumber',label:'成品数量'},
        {key:'passPercent',label:'合格率'},
        {key:'remark',label:'备注'},
        {key:'createDate',label:'创建日期'},
        {key:'updateDate',label:'修改日期'},

      ],bodies:[]},//工序管理表格


    }

  },
  computed: {},
  created () {
    this.getPlans(1,this.planPageInfo.pageSize,this.selectPlanType);
  },
  watch: {},

  methods: {
    planHandleSelect(key) {
      console.log(this.selectPlanType);
      this.selectPlanType=key;
      if(this.planNavArr.includes(key)){
        this.selectPlanType = key;
        this.getPlans(1,this.planPageInfo.pageSize,key);
        this.taskPageInfo = {
          pageSize: 10,
          total:0,
          pages:0,
          current: 1
        };
      }else{
        this.showlistTask();
        console.log('非计划');
      }
    },
    //改变页面大小
    taskPageSizeChange(val){
      this.taskPageInfo.pageSize = val;
      this.getTask(1,val,this.selectPlanType);
    },
    //改变页码
    taskPageCurrentChange(val){
      this.taskPageInfo.current = val;
      this.getTask(val,this.planPageInfo.pageSize,this.selectPlanType);
    },

    selectPlan(val){
      this.selectPlanRow = val;
      this.taskList=[];
      console.log(val)
      if(val!==null) {
        this.getTask(1,10,this.selectPlanRow.productionNo);
        this.finishProductMessage()
        //this.semiFinishedProductMessage()
        this.rawMaterialMessage()
      }
    },
    //选择计划(也就是任务)
    selectPlans(val){
      this.selectPlanRows = val;
    },
    //改变页面大小
    planPageSizeChange(val){
      this.planPageInfo.pageSize = val;
      this.getPlans(1,val,this.selectPlanType);
    },
    //改变页码
    planPageCurrentChange(val){
      this.planPageInfo.current = val;
      this.getPlans(val,this.planPageInfo.pageSize,this.selectPlanType);
    },
    getPlans(pageNum,pageSize,state){
      this.planListLoading = true;
      state === this.planNavArr[0]?state=null:state;
      console.log(state);
      let params = Object.assign({state});
      http(apiPath.plan.list(pageNum,pageSize),params,"GET").then(res => {
        console.log("production", res);
        const data = res.result
        const result = data;
        this.planList = result.records;
        if(this.planList.length===0){
          this.$message.info("没有该状态下的生产计划");
        }
        this.planPageInfo = {
          pageSize: this.planPageInfo.pageSize,
          total:result.total,
          pages:result.pages,
          current: result.current
        };
        this.planListLoading = false;
      })
    },

    openDialog(name,row){
      console.log(name)
      if(name==='createPlanDialog'){this.$refs[name].toggle(); return;}
      if(name==='CreatePublicTaskDialog'){this.$refs[name].toggle();return;}
      if(name==='showPlanDialog'){this.$refs[name].toggle(row);return;}
      if(name==='modifyPlanDialog'){this.$refs[name].toggle(row);return;}
      console.log(this.selectPlanRow);
      if(this.selectPlanRow!=undefined){
        this.$refs[name].toggle(this.selectPlanRow.productionNo);
      }
      else {if(name!=='createPlanDialog')this.$message.error("请先选择生产计划");}

    },
    copyPlans(rowsName){
      const productionNoList = this[rowsName].map(plan=>plan.productionNo);
      const params =productionNoList;
      http(apiPath.plan.copy,params,"POST").then(res => {
        this.$message.success(res.message);
        this.$refs.batchResultDialog.results=res.result;
        this.$refs.batchResultDialog.toggle();
        let computed=0;
        res.result.forEach(element => {
          if(element['结果']=='成功'){computed++;}
        });
        this.getPlans(Math.ceil((this.planPageInfo.total+computed)/this.planPageInfo.pageSize),this.planPageInfo.pageSize,this.selectPlanType);
      })
    },
    deletePlans(rowsName){
      console.log(rowsName);
      const productionNoList = this[rowsName].map(plan=>plan.productionNo);
      this.$refs['dialog'].toggle(productionNoList);//打开对话框进一步确认
    },
    //订单号实现模糊查询
    async searchPlanAsync(queryString,callback) {
      const params = {productionNo:this.searchPlanNo}
      http(apiPath.fuzzy.planNameFuzzyQuery,params,"GET").then(res => {
        const data = res.result;
        callback(data.map(item => ({'value': item})));
      })
    },
    searchPlanAsyncSelect(item){
      this.searchPlan(item.value);
    },
    //对输入的订单进行筛选过滤，输出正确的订单详情信息
    searchPlan(productionNo){
      this.planListLoading = true;
      http(apiPath.plan.plan(productionNo),{},"GET").then(res => {
        const data =res.result;
        this.planList = [data];
        this.planListLoading = false;
      })

    },
    getTask(pageNum,pageSize,planNo){
      //console.log(pageNum,pageSize,planNo);
      this.taskListLoading = true;
      let params = Object.assign({planNo});
      http(apiPath.task.list(pageNum,pageSize),params,"GET").then(res => {
        const data = res.result.records;
        this.taskList = data;
        this.taskPageInfo = {
          pageSize: this.planPageInfo.pageSize,
          total:res.result.total,
          pages:res.result.pages,
          current: res.result.current
        };
        if(this.taskList.length===0){
          this.$message.info('该计划下没有生产任务');
        }
        this.taskListLoading = false;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //打开修改工序和查看工序详情对话框
    openModify(name,row){
      console.log(row);
      this.$refs[name].toggle(row.productionPlanNo,row.taskNo);
    },
    //启动工序
    startTask(){
      var taskNoGroups=''
      for(var index in this.selectTaskRows){
        taskNoGroups=taskNoGroups+this.selectTaskRows[index].taskNo+','
      }
      const params ={taskNo:taskNoGroups.substring(0,taskNoGroups.length-1)};
      http(apiPath.task.start,params,"POST").then(res => {
        this.getTask(1,10,this.selectPlanRow.productionNo);
        this.$message.success(res.message);
        this.$refs.batchResultDialog.results=res.result;
        this.$refs.batchResultDialog.toggle();
      })
    },
    //撤销工序
    cancelTask(){
      var taskNoGroups='';
      for(var index in this.selectTaskRows){
        taskNoGroups=taskNoGroups+this.selectTaskRows[index].taskNo+','
      }
      const params ={taskNo:taskNoGroups.substring(0,taskNoGroups.length-1)};
      http(apiPath.task.revocation,params,"POST").then(res => {
        this.getTask(1,10,this.selectPlanRow.productionNo);
        this.$message.success(res.message);
        this.$refs.batchResultDialog.results=res.result;
        this.$refs.batchResultDialog.toggle();
      })
    },
    //删除工序
    deleteTask(){
      var taskNoGroups='';
      for(var index in this.selectTaskRows){
        taskNoGroups=taskNoGroups+this.selectTaskRows[index].taskNo+','
      }
      const params ={taskNo:taskNoGroups.substring(0,taskNoGroups.length-1)};
      http(apiPath.task.delete,params,"POST").then(res => {
        this.getTask(1,10,this.selectPlanRow.productionNo);
        this.$message.success(res.message);
        this.$refs.batchResultDialog.results=res.result;
        this.$refs.batchResultDialog.toggle();
      })
    },
    //暂停工序
    pauseTask(){
      var taskNoGroups='';
      for(var index in this.selectTaskRows){
        taskNoGroups=taskNoGroups+this.selectTaskRows[index].taskNo+','
      }
      const params ={taskNo:taskNoGroups.substring(0,taskNoGroups.length-1)};
      http(apiPath.task.pause,params,"POST").then(res => {
        this.getTask(1,10,this.selectPlanRow.productionNo);
        this.$message.success(res.message);
        this.$refs.batchResultDialog.results=res.result;
        this.$refs.batchResultDialog.toggle();
      })
    },
    //完成工序
    accomplishTask(){
      var taskNoGroups='';
      for(var index in this.selectTaskRows){
        taskNoGroups=taskNoGroups+this.selectTaskRows[index].taskNo+','
      }
      const params ={taskNo:taskNoGroups.substring(0,taskNoGroups.length-1)};
      http(apiPath.task.accomplish,params,"POST").then(res => {
        this.getTask(1,10,this.selectPlanRow.productionNo);
        this.$message.success(res.message);
        this.$refs.batchResultDialog.results=res.result;
        this.$refs.batchResultDialog.toggle();
      })
    },
    //快速领料
    outStock(){
      var taskNoGroups='';
      for(var index in this.selectTaskRows){
        taskNoGroups=taskNoGroups+this.selectTaskRows[index].taskNo+','
      }
      if(taskNoGroups===''){this.$message.info("请选择工序后再进行领料");return;}
      localStorage.selectTaskRows=JSON.stringify(this.selectTaskRows)
      console.log(this.selectTaskRows)
      this.addTab('out-stockroom-draw')
    },
    selectTask(val){
      this.selectTaskRows=val
      console.log(this.selectTaskRows)

    },
    //成品信息
    finishProductMessage(){
      let params= Object.assign({planNo:this.selectPlanRow.productionNo});
      http(apiPath.product.rawMaterialByPlanNo,params,"GET").then(res => {
        this.finishProduct.bodies=res.result;
        http(apiPath.product.finishedProductField,{},"GET").then(res => {
          this.finishProduct.headers=res.result;
        }).catch(err => {
          // console.log(err)
          this.$message({
            message: err.message.content,
            type: 'error'
          });
        })
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })

    },
    //原料信息
    rawMaterialMessage(){
      let params = Object.assign({planNo:this.selectPlanRow.productionNo});
      http(apiPath.product.rawMaterialByPlanNo,params,"GET").then(res => {
        this.rawMaterial.bodies=res.result;
        http(apiPath.product.rawMaterialField,{},"GET").then(res => {
          this.rawMaterial.headers=res.result;
        }).catch(err => {
          // console.log(err)
          this.$message({
            message: err.message.content,
            type: 'error'
          });
        })

      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })

    },
    //标准工序部分
    //工序列表listTask
    showlistTask(){
      this.publicTaskListLoading=true
      http(apiPath.task.listTask(this.publicTasknPageInfo.current,this.publicTasknPageInfo.pageSize),{},"GET").then(res => {
        this.taskManageTable.bodies=res.result.records
        this.publicTasknPageInfo.total=res.result.total
        this.publicTaskListLoading=false
      })
    },
    //修改pagesize大小
    publicTasknPageSizeChange(val){
      this.publicTasknPageInfo.pageSize=val;
      this.showlistTask();
    },
    //修改current
    publicTasknPageCurrentChange(val){
      this.publicTasknPageInfo.current=val
      this.showlistTask();
    },
    //删除标准工序
    deletePublicTask(row){
      const params = {taskName:row.taskName}
      http(apiPath.task.deletePublicTask,params,"POST").then(res => {
        this.$message.success(res.message);
        this.showlistTask();
      })
    },
  }
})
