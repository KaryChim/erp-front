import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
import {eventBus} from "common/socket/event";

export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data () {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js "+ name +" App",
      wsTemplateToken: '',
      state:'',
      dialogFormVisible:false,
      materialListLoading:false,
      materialListBodies:[],
      materialListHeaders:[
        {key:'materialCode',label:'物料代码'},
        {key:'materialName',label:'物料名称'},
        {key:'materialSap',label:'规格型号'},
        {key:'materialUnit',label:'单位'},
        {key:'warehouseName',label:'仓库名称'},
        {key:'inventoryQuantity',label:'库存总量'},
        {key:'availableQuantity',label:'可用库存量'},
      ],
      selected:[],
      materialInfo:'',
      page:{
        currentPage:1,
        pageSize:10,
        total:100
      }
    }
  },
  computed: {},
  created () {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate(){
      console.log("执行template具体业务操作")
    },
    toggle(){
      this.getMaterial();
      this.dialogFormVisible=!this.dialogFormVisible;
      console.log("打开");
    },
    getMaterial(){
      this.materialListLoading =true;
      let params = Object.assign({page:this.page.currentPage,size:this.page.pageSize,materialCode:this.state});
      http(apiPath.product.productList,params,"GET").then(res => {
        this.materialListBodies=res.result.records; 
        this.materialListLoading =false;
        this.page.total=res.result.total
      })
      
    },
    //改变每页大小
    handleSizeChange(val){
      this.page.pageSize=val
      this.getMaterial();
    },
    //跳转页数
    handleCurrentChange(val){
      this.page.currentPage=val
      this.getMaterial();
    },
    selectMaterial(val){
      this.selected=[];
      val.forEach(element => {  
        this.selected.push(element)         
      });
      //console.log(this.selected);
    },
    //模糊查询物料
    querySearchAsync(queryString, cb) {
      this.queryWarehouseName = queryString;
      this.page.currentPage = 1;
      this.page.pageSize = 50;
      let params = Object.assign({page:this.page.currentPage,size:this.page.pageSize,keyword:queryString});
      http(apiPath.materialPage.byKeyword,params,"GET").then(res => {
        let result = res.result.table.bodies; //data是后端返回的数据
        //console.log(result);
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      }) 
    },
    sentSelected(){
      this.$emit('func',this.selected);
      this.$refs.materialListTable.clearSelection();
    },
  }
})
