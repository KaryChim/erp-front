import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"
import {eventBus} from "common/socket/event";

export default created({
  props: {},
  components: {},
  mixins: [],
  data() {
    return {
      dialogFormVisible: false,
      formRef: 'form',
      selectedMaterial: [],//勾选要删除的物料
      selectedProduction: [],
      taskProduction: [],
      taskMaterial: [],
      //新建工序表单
      form: {
        productionName: '',
        productionNumber: '',
        productionPlanNo: '',
        taskName: '',
        operatorEmployeeNo: '',
        forecastStartDate: '',
        forecastEndDate: '',
        remark: '',
        taskNo: '',
        taskMaterialVos: [],
        passPercent: ''
      },
      formRules: {
        productionPlanNo: [
          {required: true, message: '请输入任务单号', trigger: 'change'},
        ],
        taskName: [
          {required: true, message: '请输入工序名', trigger: 'change'},
        ],
        operatorEmployeeNo: [
          {required: true, message: '请输入负责人', trigger: 'change'},
        ],
        forecastStartDate: [
          {type: 'string', required: true, message: '请输入开始时间', trigger: 'change'},
        ],
        forecastEndDate: [
          {type: 'string', required: true, message: '请输入结束时间', trigger: 'change'},
        ],
        passPercent: [
          {required: true, message: '请输入合格率', trigger: 'change'},
        ]
      },
      //成品表头
      productionTabHeader: [
        {key: 'productionName', label: '成品名称'},
        {key: 'productionNumber', label: '成品数量'}
      ],
      //物料表头
      materialTabHeader: [
        {key: 'materialCode', label: '物料代码'},
        {key: 'materialName', label: '物料名称'},
        {key: 'materialNumber', label: '物料数量'},
        {key: 'materialSap', label: '规格型号'},
        {key: 'materialType', label: '物料类型'},
        {key: 'materialUnit', label: '单位'},
        {key: 'remark', label: '备注'},
      ],
      taskProductionNumber: [],
      taskMaterialNumber: []
    }
  },
  computed: {},
  created() {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate() {
      console.log("执行template具体业务操作")
    },
    toggle(planNum, taskNo) {
      this.dialogFormVisible = !this.dialogFormVisible
      console.log(taskNo);
      console.log("确认是否清空");
      console.log(this.taskProduction);
      this.form.productionPlanNo = planNum;//从父组件中接收计划单号
      this.form.taskNo = taskNo;//从父组件中接收工序号
      this.getTaskInfo();
    },
    //模糊查询员工
    querySearchAsync(keyWord, callback) {
      let params = keyWord ? Object.assign({size: 50}, {keyWord}) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    selectMaterial(rows) {
      // 用一个变量来存放被选中的index
      this.selectedMaterial = rows;
      //console.log(this.badCodeSelection);
    },
    selectProduction(rows) {
      // 用一个变量来存放被选中的index
      this.selectedProduction = rows;
      //console.log(this.badCodeSelection);
    },
    deleteMaterialRows() {
      this.selectedMaterial.forEach((item) => {
        for (let i = 0; i < this.taskMaterial.length; i++) {
          if (this.taskMaterial[i].materialCode === item.materialCode) {
            this.taskMaterial.splice(i, 1);
            break;
          }
        }
      });
      this.selectedMaterial = [];
    },
    deleteProductionRows() {
      this.selectedProduction.forEach((item) => {
        for (let i = 0; i < this.taskProduction.length; i++) {
          if (this.taskProduction[i].materialCode === item.materialCode) {
            this.taskProduction.splice(i, 1);
            break;
          }
        }
      });
      this.selectedProduction = [];
    },
    getTaskInfo() {
      const params = {taskNo: this.form.taskNo}
      http(apiPath.task.taskDetail, params, "GET").then(res => {
        //工序回填信息
        this.taskProduction = [];
        this.taskMaterial = [];
        this.form.taskMaterialVos = [];
        this.form.taskName = res.result.taskName;
        this.form.operatorEmployeeNo = res.result.operatorEmployeeNo;
        this.form.passPercent = res.result.passPercent;
        this.form.forecastStartDate = res.result.forecastStartDate;
        this.form.forecastEndDate = res.result.forecastEndDate;
        this.form.remark = res.result.remark;
        this.form.passPercent = res.result.passPercent
        // this.form.state=data.result.state;
        for (var item in res.result.taskMaterialVos) {
          if (res.result.taskMaterialVos[item].materialType == "成品")
            this.taskProduction.push(res.result.taskMaterialVos[item]);
          else
            this.taskMaterial.push(res.result.taskMaterialVos[item]);
        }
        this.taskProductionNumber = JSON.parse(JSON.stringify(this.taskProduction))
        this.taskMaterialNumber = JSON.parse(JSON.stringify(this.taskMaterial))
      })
    },
    //按照比例改变原料数量
    changeNumber(number) {
      console.log(number)
      setTimeout(() => {
        for (var item in this.taskMaterialNumber) {
          console.log(this.taskProductionNumber[0].materialNumber)
          this.taskMaterial[item].materialNumber
            = Number(number) / Number(this.taskProductionNumber[0].materialNumber) * Number(this.taskMaterialNumber[item].materialNumber)
        }
      },200)
    },
    //重置表单数据
    reset(ref) {
      console.log(ref);
      this.taskProduction = []
      this.taskMaterial = []
      this.$refs[ref].resetFields();
    },
    //模糊查询工序名称
    searchTaskName(queryString, cb) {
      //taskName
      const params = {keyword: queryString};
      http(apiPath.fuzzy.taskName, params, "GET").then(res => {
        let {result} = res;//data是后端返回的数据
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      })
    },
    //选择工序后自动回填publicTaskDetail
    selectTaskName() {
      this.taskProduction = [];
      this.taskMaterial = [];
      const params = {taskName: this.form.taskName};
      http(apiPath.task.publicTaskDetail, params, "GET").then(res => {
        this.form.passPercent = res.result.passPercent;
        for (var item in res.result.taskMaterialVos) {
          console.log(res)
          if (res.result.taskMaterialVos[item].materialType == "成品")
            this.taskProduction.push(res.result.taskMaterialVos[item]);
          else
            this.taskMaterial.push(res.result.taskMaterialVos[item]);
        }
        this.taskProductionNumber = JSON.parse(JSON.stringify(this.taskProduction))
        this.taskMaterialNumber = JSON.parse(JSON.stringify(this.taskMaterial))
      })
    },
  }
})
