// import {
//   http,
//   apiPath
// } from "@/api"

import created from "mixins/created.js"
import {eventBus} from "common/socket/event";

export default created({
  props: {},
  components: {
  },
  mixins: [],
  data () {
    return {
      dialogFormVisible:false,
      results:[]
    }
  },
  computed: {},
  created () {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate(){
      console.log("执行template具体业务操作")
    },
    toggle(){
      this.dialogFormVisible=!this.dialogFormVisible
    }
  }
})
