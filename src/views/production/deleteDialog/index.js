// import {
//   http,
//   apiPath
// } from "@/api"
import created from "mixins/created.js"
import {eventBus} from "common/socket/event";
import http from "@/api/http";
import { apiPath } from "@/api/index";

export default created({
  props:['refresh','selectPlanType','planPageInfo'],
  components: {

  },
  mixins: [],
  data () {
    return {
      dialogFormVisible:false,
      productionNoList:[],
      result: false
    }
  },
  computed: {},
  created () {
    // 新建接受订阅器
    this.wsTemplateToken = eventBus.wsTemplate.sub(this.dealTemplate)
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    this.wsTemplateToken && eventBus.wsTemplate.unsub(this.wsTemplateToken)
  },
  watch: {},
  methods: {
    dealTemplate(){
      console.log("执行template具体业务操作")
    },
    async deletePlans(production){
      console.log(this.$parent.$parent.$parent)
      const params =production;
      http(apiPath.plan.delete,params,"POST").then(res => {
        this.$message.success(res.message);
        let computed=0;
        res.result.forEach(element => {
          if(element['结果']=='成功'){computed++;}
        });
        this.refresh(Math.ceil((this.planPageInfo.total-computed)/this.planPageInfo.pageSize),this.planPageInfo.pageSize,this.selectPlanType)
        this.dialogFormVisible=!this.dialogFormVisible
      })      
    },
    toggle(rowName){
      this.productionNoList=rowName
      this.dialogFormVisible=!this.dialogFormVisible
    }
  }
  
  
})
