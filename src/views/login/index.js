// import router from "../../router";
import vuexHome from "mixins/vuex/home";
import created from "mixins/created.js"
import {
  http,
  apiPath
} from "@/api"
import {listToTree} from "common/utils/listUtil"

export default created({
  props: {},
  components: {},
  mixins: [vuexHome], // eslint-disable-next-line
  data() {
    return {
      form: {
        username: "admin",
        password: "000000",
      },
      rules: {
        username: [
          {required: true, message: '请输入用户名', trigger: 'blur'},
          {min: 3, max: 15, message: '用户名长度在 3 到 15 个字符', trigger: 'blur'}
        ],
        password: [
          {required: true, message: '请输入密码', trigger: 'blur'},
          {min: 6, max: 15, message: '密码长度在 6 到 15 个字符', trigger: 'blur'}
        ]
      },
    }
  },
  created() {
    let getStorage = localStorage.getItem("user-loginInfo");
    const localStroage = getStorage && JSON.parse(getStorage) || {};
    let {access_token = "", expired_time = 0} = localStroage;
    if (access_token && expired_time > new Date().getTime()) {
      console.log("access_token存在且未过期")
      this.$router.push('/production');
    }
  },
  methods: {
    resetRuleFormRef() {
      console.log(this)
      this.$refs.ruleForm.resetFields();
    },
    submitForm(formName) {
      this.$refs[formName].validate((valid) => {
        if (valid) {
          let {username, password} = this.form;
          let params = Object.assign({username, password})
          // 登录请求
          http(apiPath.user.user.login, params, "POST").then(res => {
            let {username, token: access_token} = res.result;
            console.log(username, access_token)
            this.loginInfoSet(Object.assign({username, access_token}));
            res.result.menu = [
              {
                "id":1,
                "name":"生产管理",
                "pid":0,
                "path":"/production",
                "nature":0,
                "index":"/production"
              },
              {
                "id":2,
                "pid":0,
                "name":"物料管理",
                "path":"/materiel",
                "nature":0,
                "index":"/materiel"
              },
              {
                "id":3,
                "pid":0,
                "name":"财务管理",
                "path":"/finance",
                "nature":0,
                "index":"/finance"
              },
              {
                "id":4,
                "pid":0,
                "name":"审核页面",
                "path":"/examine",
                "nature":0,
                "index":"/examine"
              },
              {
                "id":5,
                "pid":0,
                "name":"账号管理",
                "path":"/account",
                "nature":0,
                "index":"/account"
              },
              {
                "id":6,
                "pid":5,
                "nature":0,
                "name":"部门管理",
                "path":"/account/departmentManage",
                "index":"/account/departmentManage"
              },
              {
                "id":7,
                "pid":5,
                "nature":0,
                "name":"员工管理",
                "path":"/account/employeeManage",
                "index":"/account/employeeManage"
              },
              {
                "id":8,
                "pid":5,
                "nature":0,
                "name":"角色管理",
                "path":"/account/roleManage",
                "index":"/account/roleManage"
              },
              {
                "id":9,
                "pid":5,
                "nature":0,
                "name":"菜单管理",
                "path":"/account/menuManage",
                "index":"/account/menuManage"
              },
              {
                "id":10,
                "pid":5,
                "nature":0,
                "name":"权限管理",
                "path":"/account/powerManage",
                "index":"/account/powerManage"
              }
            ]
            // this.permissionStateSet(res.power);
            this.permissionStateSet(res.result.menu);
            let menu = listToTree(res.result.menu)
            this.menuStateSet(menu);

            let curTime = new Date().getTime();
            // 增加过期时间 30分钟
            let expired_time = curTime + 30 * 60 * 1000;
            let info = JSON.stringify({
              username: username,
              access_token: access_token,
              time: curTime,
              expired_time: expired_time
            });
            localStorage.setItem('user-loginInfo', info);
            this.$router.push('/production');
          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content || "网络不给力",
              type: 'error'
            });
          })
        } else {
          return false;
        }
      });
    },
  }
})
