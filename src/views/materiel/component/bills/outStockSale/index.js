import {
  http,
  apiPath
} from "@/api"

const cityOptions = ['linenumber', 'materialName', 'materialSap', 'batchNumber', 'warehouseName', 'unit', 'unitPrice',
  'issuedQuantity', 'remark', 'productionDate', 'materialExpirationDate', 'expiryDate'
];
const disableOptions = ['linenumber', 'materialName', 'materialSap'];
export default {
  data() {
    return {
      checkAll: true, //默认全选
      checkedCities: ['linenumber', 'materialName', 'materialSap', 'batchNumber', 'warehouseName', 'unit', 'unitPrice',
        'issuedQuantity', 'remark', 'productionDate', 'materialExpirationDate', 'expiryDate'
      ],
      isIndeterminate: false, //默认全选
      dialogFormVisible: false, //对话框
      ruleForm: {
        documentType: '销售出库', //单据类型
        documentDate: '', //日期
        documentNo: '', //单据编号
        redSheet: "false", //红蓝单
        allow: "false", //审核标志
        digest: '', //摘要
        manager: '', //主管
        salesWay: '', //销售方式
        department: '', //部门
        examineEmployee: '', //审核人
        documentCreator: '', //制单人
        handingEmployee: '', //经办人
        delivery: '', //送货人
        consignor: '', //发货人
        qualityEmployee: '', //品质检查人
        deliveryPlace: '', //交货地点
        safekeepEmployee: '', //保管人
        approvalEmployee: '', //批准人
        salesman: '', //业务员
        materialsDetail: [{
          linenumber: '',
          materialName: '',
          materialSap: '',
          batchNumber: '',
          warehouseName: '',
          unit: '',
          unitPrice: '',
          issuedQuantity: '',
          remark: '',
          productionDate: '',
          materialExpirationDate: '',
          expiryDate: ''
        },]
      },
      dialogDetailForm: {
        linenumber: '',
        materialName: '',
        materialSap: '',
        warehouseName: '',
        batchNumber: '',
        unit: '',
        unitPrice: '',
        issuedQuantity: '',
        amountMoney: '',
        remark: '',
        productionDate: '',
        materialExpirationDate: '',
        expiryDate: ''
      },
      dialogDetailFormIndex: -1,
      rules: {
        documentType: [
          {required: true, message: '请选择单据类型', trigger: 'change'}
        ],
        documentNo: [
          {required: true, message: '请输入单号', trigger: 'change'}
        ],
        documentDate: [
          {type: 'string', required: true, message: '请选择日期', trigger: 'change'}
        ],
        documentCreator: [
          {required: true, message: '请输入制单人', trigger: 'change'}
        ],
        salesWay: [
          {required: true, message: '请选择销售方式', trigger: 'change'}
        ],
        handingEmployee: [
          {required: true, message: '请选择经办人', trigger: 'change'}
        ],
        approvalEmployee: [
          {required: true, message: '请选择批准人', trigger: 'change'}
        ],
        delivery: [
          {required: true, message: '请选择送货人', trigger: 'change'}
        ],
        qualityEmployee: [
          {required: true, message: '请选择品质检查人', trigger: 'change'}
        ],
        materialName: [
          {required: true, message: '请输入物料名称', trigger: 'change'}
        ],
        warehouseName: [
          {required: true, message: '请输入仓库名称', trigger: 'change'}
        ],
        unit: [
          {required: true, message: '请输入单位', trigger: 'change'}
        ],
        issuedQuantity: [
          {required: true, message: '请输入实发数量', trigger: 'change'}
        ],
      },
      //全部表格字段
      // 不可选字段
      disabledColumns: [{
        prop: 'linenumber',
        label: '行号',
        fixed: true,
        type: "index",
        width: '50px'
      }, {
        prop: 'materialName',
        label: '物料名称',
        fixed: true
      }, {
        prop: 'materialSap',
        label: '规格型号',
        fixed: true
      }],
      columns: [{
        prop: 'batchNumber',
        label: '批号'
      },
      {
        prop: 'warehouseName',
        label: '发料仓库'
      },
      {
        prop: 'unit',
        label: '单位'
      },
      {
        prop: 'unitPrice',
        label: '单价'
      },
      {
        prop: 'issuedQuantity',
        label: '实发数量'
      },
      {
        prop: 'remark',
        label: '备注'
      },
      {
        prop: 'productionDate',
        label: '生产/采购日期'
      },
      {
        prop: 'materialExpirationDate',
        label: '保质期（天）'
      },

      {
        prop: 'expiryDate',
        label: '有效期到'
      },
      ],
      //已选表格字段
      tempCols: [],
      DialogDetailFormVisible: false,
    }
  },
  created() {
    this.tempCols = Array.from(this.disabledColumns)
    this.tempCols.push(...this.columns)
  },
  methods: {
    //全选按钮
    handleCheckAllChange(val) {
      this.checkedCities = val ? cityOptions : disableOptions;
      this.isIndeterminate = false;
    },
    //单个选中
    handleCheckedCitiesChange(value) {
      let checkedCount = value.length;
      this.checkAll = checkedCount === this.columns.length + this.disabledColumns.length;
      this.isIndeterminate = checkedCount > 0 && checkedCount < this.columns.length + this.disabledColumns.length;
    },
    //设置表格列字段
    confirmShow() {
      this.tempCols = Array.from(this.disabledColumns);
      console.log("checkedCities:", this.checkedCities)
      console.log("columns:", this.columns)
      if (this.checkedCities.length < 1) {
        this.$message.warning("请至少选择一项数据");
        this.dialogFormVisible = true;
      } else {
        for (var i = 0; i < this.columns.length; i++) {
          for (var j = this.disabledColumns.length; j < this.checkedCities.length; j++) {
            if (this.columns[i].prop == this.checkedCities[j]) {
              this.tempCols.push(this.columns[i]);
            }
          }
        }
        console.log("tempCols:", this.tempCols);
        //this.cols = this.tempCols;
        this.dialogFormVisible = false;
      }
    },
    //删行
    deleteRow(index, rows) {
      rows.splice(index, 1);
    },
    //添行
    addRow() {
      const newRow = {
        materialName: '',
        materialSap: '',
        batchNumber: '',
        warehouseName: '',
        unit: '',
        unitPrice: '',
        issuedQuantity: '',
        remark: '',
        productionDate: '',
        materialExpirationDate: '',
        expiryDate: ''
      };
      this.ruleForm.materialsDetail.push(newRow);
    },
    //回填物料规格型号
    handleSelect(item) {
      this.dialogDetailForm.materialSap = item.materialSap;
    },
    closeDetailFormDialog() {
      this.DialogDetailFormVisible = false
      this.dialogDetailFormIndex = -1
      this.dialogDetailForm = Object.assign({}, this.$options.data().dialogDetailForm)
    },
    openDetailFormDialog(item) {
      console.log("handleSelect item:", item)
      if (item) {
        this.dialogDetailFormIndex = item.$index;
        this.dialogDetailForm = Object.assign({}, item.row);
      }
      this.DialogDetailFormVisible = true;
    },
    // 保存物料detail
    handleSaveDetailForm() {
      this.$refs['dialogDetailForm'].validate((valid) => {
        if (valid) {
          // 校验
          if (this.dialogDetailFormIndex >= 0) {
            // 确认编辑
            this.ruleForm.materialsDetail[this.dialogDetailFormIndex] = this.dialogDetailForm
          } else {
            // 确认添加
            this.ruleForm.materialsDetail.push(this.dialogDetailForm)
          }
          this.closeDetailFormDialog()
        }
      })
    },
    //检测表格数据是否合格
    checkTableData() {
      if (this.ruleForm.materialsDetail.length == 0) {
        this.$message.error("至少添加一种物料");
        return false;
      }
      if (this.ruleForm.materialsDetail.every(item => item.materialName !== '')) {
        if (this.ruleForm.materialsDetail.every(item => item.unit !== '')) {
          if (this.ruleForm.materialsDetail.every(item => item.receivedNum !== '')) {
            if (this.ruleForm.materialsDetail.every(item => item.warehouseName !== ''))
              return true;
            else {
              this.$message.error("物料收料仓库不能为空");
              return false;
            }
          } else {
            this.$message.error("物料实收数量不能为空");
            return false;
          }
        } else {
          this.$message.error("物料单位不能为空");
          return false;
        }
      } else {
        this.$message.error("物料名称不能为空");
        return false;
      }
    },
    //提交数据
    async onSumit(ref) {
      this.$refs[ref].validate(async (valid) => {
        if (valid) {
          if (this.checkTableData()) {
            http(apiPath.material.billCreateBills, this.ruleForm, "POST").then(res => {
              this.$message.success(res.message);
              this.$parent.$parent.$parent.removeTab(this.$parent.$parent.$parent.tabActiveName);
            }).catch(err => {
              // console.log(err)
              this.$message({
                message: err.message.content,
                type: 'error'
              });
            })
          }
        } else
          this.$message({showClose: true, message: '提交失败', type: 'error', offset: 40});
      });

    },
    //获取数据
    async getDate() {
      var params = Object.assign({billsType: '销售出库'})
      http(apiPath.material.billDocumentNo, params, "GET").then(res => {
        const documentNo = res.result;
        this.ruleForm.documentNo = documentNo;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询员工
    async querySearchAsync(queryString, cb) {
      var params = queryString ? Object.assign({size: 50}, {keyWord: queryString}) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询仓库名
    async searchWarehouseNameAsync(queryString, callback) {
      var params = Object.assign({keyWord: queryString})
      http(apiPath.fuzzy.listWarehouseByKeyWord, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询送货人
    async searchCourierAsync(queryString, callback) {
      var params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.courier, params, "GET").then(res => {
        console.log(res.result);
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询部门
    async searchDepartmentAsync(queryString, callback) {
      var params = queryString ? Object.assign({size: 50},{keyword: queryString}) : Object.assign({size: 50})
      http(apiPath.fuzzy.department, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询物料名
    async searchMaterialAsync(queryString, callback) {
      var params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.fuzzyMaterial, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询计量单位
    async searchUnitAsync(queryString, callback) {
      var params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.unit, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
  },
  mounted() {
    this.getDate();
  },
}
