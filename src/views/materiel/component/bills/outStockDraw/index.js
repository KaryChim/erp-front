/* eslint-disable max-len */
import {
  http,
  apiPath
} from "@/api"

const cityOptions = ['linenumber', 'materialName', 'materialSap', 'batchNumber', 'warehouseName', 'unit', 'remark', 'issuedQuantity', 'productProject'];
const disableOptions = ['linenumber', 'materialName', 'materialSap'];
export default {
  data() {
    return {
      loading: false,
      checkAll: true, //默认全选
      checkedCities: ['linenumber', 'materialName', 'materialSap', 'batchNumber', 'warehouseName', 'unit', 'remark', 'issuedQuantity', 'productProject'],
      isIndeterminate: false, //默认全选
      dialogFormVisible: false,
      warehouseDialogVisible: false,
      selectedMaterialName: '',
      selectedMaterialLeastQuantity: '',
      selectedWarehouse: [],
      materialInfo: [],
      warehouseHeader: [],
      PageInfo: {total: 0, current: 1, pages: 0, pageSize: 10},
      ruleForm: {
        documentType: '领料出库', //单据类型
        documentDate: '', //日期
        documentNo: '', //单据编号
        redSheet: 'false', //红蓝单
        allow: 'false', //审核标志
        digest: '', //摘要
        manager: '', //主管
        salesWay: '', //销售方式
        department: '', //部门
        examineEmployee: '', //审核人
        documentCreator: '', //制单人
        consignor: '', //发货人
        safekeepEmployee: '', //保管人
        approvalEmployee: '', //批准
        delivery: '', //送货人
        salesman: '', //业务员
        materialsDetail: [{
          linenumber: '',
          materialName: '',
          materialSap: '',
          warehouseName: '',
          batchNumber: '',
          unit: '',
          remark: '',
          issuedQuantity: '',
          productProject: ''
        }],
      },
      dialogDetailForm: {
        linenumber: '',
        materialName: '',
        materialSap: '',
        warehouseName: '',
        batchNumber: '',
        unit: '',
        remark: '',
        issuedQuantity: '',
        productProject: ''
      },
      dialogDetailFormIndex: -1,
      rules: {
        documentType: [
          {required: true, message: '请选择单据类型', trigger: 'change'}
        ],
        documentNo: [
          {required: true, message: '请输入单号', trigger: 'change'}
        ],
        documentDate: [
          {type: 'string', required: true, message: '请选择日期', trigger: 'change'}
        ],
        department: [
          {required: true, message: '请输入部门', trigger: 'change'}
        ],
        documentCreator: [
          {required: true, message: '请输入制单人', trigger: 'change'}
        ],
        currency: [
          {required: true, message: '请输入币别', trigger: 'change'}
        ],
        materialName: [
          {required: true, message: '请输入物料名称', trigger: 'change'}
        ],
        warehouseName: [
          {required: true, message: '请输入仓库名称', trigger: 'change'}
        ],
        unit: [
          {required: true, message: '请输入单位', trigger: 'change'}
        ],
        issuedQuantity: [{
          required: true,
          message: '请输入实发数量',
          trigger: 'change'
        }]
      },
      //全部表格字段
      // 不可选字段
      disabledColumns: [{
        prop: 'linenumber',
        label: '行号',
        fixed: true,
        type: "index",
        width: '50px'
      }, {
        prop: 'materialName',
        label: '物料名称',
        fixed: true
      }, {
        prop: 'materialSap',
        label: '规格型号',
        fixed: true
      }],
      columns: [{
        prop: 'batchNumber',
        label: '批号'
      },
      {
        prop: 'warehouseName',
        label: '发料仓库'
      },
      {
        prop: 'unit',
        label: '单位'
      },
      {
        prop: 'remark',
        label: '备注'
      },
      {
        prop: 'issuedQuantity',
        label: '实发数量'
      },
      {
        prop: 'productProject',
        label: '产品项目'
      },
      ],
      //已选表格字段
      tempCols: [],
      form: {
        productionPlanNo: '',
        taskName: '',
        state: '',
        operatorEmployeeNo: '',
        forecastStartDate: '',
        forecastEndDate: '',
        remark: '',
        taskNo: '',
        taskNos: ''
      },
      selectTaskRows: [],
      DialogDetailFormVisible: false,
    }

  },
  created() {
    this.tempCols = Array.from(this.disabledColumns)
    this.tempCols.push(...this.columns)
  },
  methods: {
    //全选按钮
    handleCheckAllChange(val) {
      this.checkedCities = val ? cityOptions : disableOptions;
      this.isIndeterminate = false;
    },
    //单个选中
    handleCheckedCitiesChange(value) {
      let checkedCount = value.length;
      this.checkAll = checkedCount === this.columns.length + this.disabledColumns.length;
      this.isIndeterminate = checkedCount > 0 && checkedCount < this.columns.length + this.disabledColumns.length;
    },
    //设置表格列字段
    confirmShow() {
      this.tempCols = Array.from(this.disabledColumns);
      console.log("checkedCities:", this.checkedCities)
      console.log("columns:", this.columns)
      if (this.checkedCities.length < 1) {
        this.$message.warning("请至少选择一项数据");
        this.dialogFormVisible = true;
      } else {
        for (var i = 0; i < this.columns.length; i++) {
          for (var j = this.disabledColumns.length; j < this.checkedCities.length; j++) {
            if (this.columns[i].prop == this.checkedCities[j]) {
              this.tempCols.push(this.columns[i]);
            }
          }
        }
        console.log("tempCols:", this.tempCols);
        //this.cols = this.tempCols;
        this.dialogFormVisible = false;
      }
    },
    //删行
    deleteRow(index, rows) {
      rows.splice(index, 1);
    },
    //添行
    addRow() {
      const newRow = {
        materialName: '',
        materialSap: '',
        warehouseName: '',
        batchNumber: '',
        unit: '',
        remark: '',
        issuedQuantity: '',
        productProject: '',
        leastQuantity: ''
      };
      this.ruleForm.materialsDetail.push(newRow);
      console.log(this.ruleForm.materialsDetail)
    },
    //回填物料规格型号
    handleSelect(item) {
      this.dialogDetailForm.materialSap = item.materialSap;
    },
    closeDetailFormDialog() {
      this.DialogDetailFormVisible = false
      this.dialogDetailFormIndex = -1
      this.dialogDetailForm = Object.assign({}, this.$options.data().dialogDetailForm)
    },
    openDetailFormDialog(item) {
      console.log("handleSelect item:", item)
      if (item) {
        this.dialogDetailFormIndex = item.$index;
        this.dialogDetailForm = Object.assign({}, item.row);
      }
      this.DialogDetailFormVisible = true;
    },
    // 保存物料detail
    handleSaveDetailForm() {
      this.$refs['dialogDetailForm'].validate((valid) => {
        if (valid) {
          // 校验
          if (this.dialogDetailFormIndex >= 0) {
            // 确认编辑
            this.ruleForm.materialsDetail[this.dialogDetailFormIndex] = this.dialogDetailForm
          } else {
            // 确认添加
            this.ruleForm.materialsDetail.push(this.dialogDetailForm)
          }
          this.closeDetailFormDialog()
        }
      })
    },
    //检测表格数据是否合格
    checkTableData() {
      if (this.ruleForm.materialsDetail.length == 0) {
        this.$message.error("至少添加一种物料");
        return false;
      }
      if (this.ruleForm.materialsDetail.every(item => item.materialName !== '')) {
        if (this.ruleForm.materialsDetail.every(item => item.unit !== '')) {
          if (this.ruleForm.materialsDetail.every(item => item.receivedNum !== '')) {
            if (this.ruleForm.materialsDetail.every(item => item.warehouseName !== ''))
              return true;
            else {
              this.$message.error("物料收料仓库不能为空");
              return false;
            }
          } else {
            this.$message.error("物料实收数量不能为空");
            return false;
          }
        } else {
          this.$message.error("物料单位不能为空");
          return false;
        }
      } else {
        this.$message.error("物料名称不能为空");
        return false;
      }
    },
    //提交数据
    async onSumit(ref) {
      this.$refs[ref].validate(async (valid) => {
        if (valid) {
          if (this.checkTableData()) {
            http(apiPath.material.billCreateBills, this.ruleForm, "POST").then(res => {
              this.$message.success(res.message);
            }).catch(err => {
              // console.log(err)
              this.$message({
                message: err.message.content,
                type: 'error'
              });
            })
            if (this.form.taskNo !== '') {
              //setDocumentNo
              //  await this.$axios.put(this.$interface.Task.setDocumentNo,{},{params:
              //{taskNo:this.form.taskNos.substring(0,this.form.taskNos.length-1),documentNo:this.ruleForm.documentNo}});
              this.$message.success("关联成功！");
            }
            this.$parent.$parent.$parent.removeTab(this.$parent.$parent.$parent.tabActiveName);
          }
        } else
          this.$message({showClose: true, message: '提交失败', type: 'error', offset: 40});
      });
    },
    //获取单号
    async getDate() {
      var params = Object.assign({billsType: '领料出库'})
      http(apiPath.material.billDocumentNo, params, "GET").then(res => {
        const documentNo = res.result;
        this.ruleForm.documentNo = documentNo;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询员工
    async querySearchAsync(queryString, cb) {
      var params = queryString ? Object.assign({size: 50}, {keyWord: queryString}) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询领料用途
    async searchPickingPurposeAsync(queryString, callback) {
      //pickingPurpose
      var params = Object.assign({params: {keyword: queryString}})
      http(apiPath.fuzzy.pickingPurpose, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询部门
    async searchDepartmentAsync(queryString, callback) {
      var params = queryString ? Object.assign({size: 50},{keyword: queryString}) : Object.assign({size: 50})
      http(apiPath.fuzzy.department, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询物料名
    async searchMaterialAsync(queryString, callback) {
      var params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.fuzzyMaterial, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询仓库名
    async searchWarehouseNameAsync(queryString, callback) {
      var params = Object.assign({keyWord: queryString})
      http(apiPath.fuzzy.listWarehouseByKeyWord, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询计量单位
    async searchUnitAsync(queryString, callback) {
      var params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.unit, params, "GET").then(res => {
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //拉取领料出库
    async getTaskInfo() {
      this.loading = true; //getPickingCreateInfo
      var params = Object.assign({taskNo: this.form.taskNo})
      http(apiPath.material.getPickingCreateInfo, params, "GET").then(res => {
        //回填信息
        this.ruleForm.documentDate = res.result.documentDate;
        for (var item = 0; item < res.result.bodyData.length; item++) {
          const newRow = {
            materialName: '',
            materialSap: '',
            warehouseName: '',
            batchNumber: '',
            unit: '',
            remark: '',
            issuedQuantity: '',
            productProject: '',
            minIssuedQuantity: '',
            minIssuedQuantityUnit: ''
          };
          this.ruleForm.materialsDetail.push(newRow);
          for (var index in newRow)
            this.ruleForm.materialsDetail[this.ruleForm.materialsDetail.length - 1][index] = res.result.bodyData[item][index];
        }
        this.loading = false;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    // //填写仓库时回填单据信息
    // async getMaterialDetial(){
    //   const materialQuantityInfoList=[];
    //   const warehouseList=[];
    //   for(var item in this.ruleForm.materialsDetail){
    //     const newOjb={};
    //     newOjb.materialName=this.ruleForm.materialsDetail[item].materialName;//传物料名称
    //     newOjb.quantity=this.ruleForm.materialsDetail[item].issuedQuantity;//传物料数量
    //     newOjb.unitName=this.ruleForm.materialsDetail[item].unit;//传物料单位
    //     materialQuantityInfoList.push(newOjb);
    //     if(this.ruleForm.materialsDetail[item].warehouseName!=undefined)
    //       warehouseList.push(this.ruleForm.materialsDetail[item].warehouseName);
    //   }
    //   console.log(materialQuantityInfoList);
    //   const data = await this.$axios.post(this.$interface.BILLS.getPickingCreateInfo,{materialQuantityInfoList,warehouseList});
    // },
    //打开选择物料仓库对话框
    async selectWarehouse(current, Size, selectRow) {
      this.selectedMaterialName = selectRow.materialName;
      this.selectedMaterialLeastQuantity = selectRow.minIssuedQuantity + selectRow.minIssuedQuantityUnit;
      var params = Object.assign({size: this.PageInfo.pageSize, page: this.PageInfo.current, materialName: this.selectedMaterialName})
      http(apiPath.material.stockInfo, params, "GET").then(res => {
        console.log(res.result.total);
        this.PageInfo.pages = res.result.pages;
        this.PageInfo.total = res.result.total;
        this.warehouseHeader = res.result.table.headers;
        this.materialInfo = res.result.table.bodies;
        this.warehouseDialogVisible = true;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    SelectionWarehouseChange(val) {
      this.selectedWarehouse = val;
      console.log(this.selectedWarehouse);
    },
    //页面改变
    PageSizeChange(val) {
      this.PageInfo.pageSize = val;
      this.selectWarehouse(1, val, this.selectedMaterialName, this.selectedMaterialLeastQuantity);
    },
    PageCurrentChange(val) {
      this.planPageInfo.current = val;
      this.selectWarehouse(val, this.PageInfo.pageSize, this.selectedMaterialName, this.selectedMaterialLeastQuantity);

    },
    //选择仓库后，修改物料信息
    modifyMaterialDetail() {
      if (this.selectedWarehouse.length == 0) {
        this.$message({showClose: true, message: '请至少选择一个仓库', type: 'error'});
        return 0;
      }

      for (var item1 = 0; item1 < this.ruleForm.materialsDetail.length; item1++) {
        if (this.ruleForm.materialsDetail[item1].materialName == this.selectedMaterialName) {
          for (var item2 in this.selectedWarehouse) {
            var newRow = JSON.parse(JSON.stringify(this.ruleForm.materialsDetail[item1]));
            newRow.warehouseName = this.selectedWarehouse[item2].warehouseName;
            newRow.unit = this.selectedWarehouse[item2].unitName;
            newRow.issuedQuantity = this.selectedWarehouse[item2].number;
            this.ruleForm.materialsDetail.splice(item1, 0, newRow);
            item1++;
          }
          break;
        }
      }
      var length = 0;
      //console.log(item1);
      for (; item1 < this.ruleForm.materialsDetail.length; item1++) {
        if (this.ruleForm.materialsDetail[item1].materialName == this.selectedMaterialName) {
          length++;
          if (item1 == this.ruleForm.materialsDetail.length - 1)
            this.ruleForm.materialsDetail.splice(item1 - length + 1, length);
        } else {
          console.log(item1 - length);
          this.ruleForm.materialsDetail.splice(item1 - length, length);
          break;
        }
      }
      this.warehouseDialogVisible = false;
    }
  },
  mounted() {
    if (localStorage.selectTaskRows != '' && localStorage.selectTaskRows != null) {
      this.form.taskNo = ''
      this.form.taskNos = ''
      this.ruleForm.materialsDetail = []
      this.selectTaskRows = JSON.parse(localStorage.selectTaskRows)
      var index = 0;
      for (; index < this.selectTaskRows.length; index++) {
        this.form.taskNo = this.selectTaskRows[index].taskNo
        this.form.taskNos = this.form.taskNos + this.selectTaskRows[index].taskNo + ','
        this.getTaskInfo()
      }
      localStorage.selectTaskRows = ''
    }
    console.log(this.form.taskNos)
    this.getDate();
  },
}
