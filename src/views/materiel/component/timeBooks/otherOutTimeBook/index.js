/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"

export default created({
  props: {},
  components: {},
  mixins: [],
  data() {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js " + name + " App",
      mergeField: [],
      mergeIndex: {},
      headers: [],
      bodies: [],
      mergeNum: {},
      loading: false,
      tableHeight: 10000,
      page: {
        currentPage: 1,
        pageSize: 10,
        total: 100
      }
    }
  },
  computed: {},
  created() {

  },
  destroyed() {

  },
  watch: {},
  methods: {
    //表头样式
    headerStyle() {
      return 'tableStyle'
    },
    objectSpanMethod({ column, rowIndex }) {
      /**
       * 获取字段名
       * 查看merge字段中是否包含该字段名
       * 字段中要是包含了需要合并的行，则返回[0行,0列]
       * 若不是被合并的行，必有[1行,1列]
       */
      const { property } = column;
      if (this.mergeField.includes(property)) {
        if (this.mergeIndex[property] && this.mergeIndex[property].includes(rowIndex)) {
          return { rowspan: 0, colspan: 0 }
        }
        let index = rowIndex;
        let mergeRow = 1;
        /**
         * 遍历该字段数据，直到字段值不相等或最后一个为止。
         * 在其中每次相等都将{mergeRow,index}+1
         * 将该字段需要何合并的下标记下
         **/
        while (this.tableData.length > index + 1 &&
          this.tableData[index][property] === this.tableData[index + 1][property]) {
          mergeRow = mergeRow + 1;
          index = index + 1;
          if (this.mergeIndex[property]) {
            this.mergeIndex[property].push(index);
          } else {
            this.mergeIndex[property] = [index];
          }
        }
        return { rowspan: mergeRow, colspan: 1 };
      }
    },
    objectSpanMethod2({ row, column, rowIndex, columnIndex }) {
      const { property } = column;
      if (this.mergeField.includes(property)) {
        if (this.mergeIndex[rowIndex] !== undefined) {
          return { rowspan: 0, colspan: 0 };
        }
        if (property === this.mergeField[0]) {
          let index = rowIndex;
          let mergeRow = 1;
          while (this.bodies.length > index + 1 && this.bodies[index + 1][property] === null) {
            mergeRow = mergeRow + 1;
            index = index + 1;
            this.mergeIndex[index] = true;
          }
          this.mergeNum[rowIndex] = mergeRow;
          return { rowspan: mergeRow, colspan: 1 };
        } else {
          return { rowspan: this.mergeNum[rowIndex], colspan: 1 };
        }
      }
    },
    async getDate() {
      this.loading = true;
      let params = Object.assign({ billsType: '其他出库' })
      http(apiPath.bills.allBills(this.page.currentPage, this.page.pageSize), params, "GET").then(res => {
        const { headers } = res.result.table;
        const { bodies } = res.result.table;
        //转换红蓝单和审核标志
        for (var i = 0; i < bodies.length; i++) {
          if (bodies[i].documentNo != null) {
            if (bodies[i].allow == false)
              bodies[i].allow = "未审核";
            else bodies[i].allow = "已审核";
            if (bodies[i].redSheet == false)
              bodies[i].redSheet = "蓝单";
            else bodies[i].redSheet = "红单";
          }
        }
        this.headers = headers;
        this.bodies = bodies;
        this.page.total = res.result.total;
        const mergeItem = headers.filter(e => e.merge);
        this.mergeField = mergeItem.map(e => e.key);
        this.loading = false;
        this.tableHeight = document.documentElement.clientHeight - this.$refs['table'].$el.getBoundingClientRect().top;
        return;
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    tableRowClassName({ row, rowIndex }) {
      if (row['documentNo'] !== null) {
        return 'success-row';
      }
    },
    //改变每页大小
    handleSizeChange(val) {
      this.page.pageSize = val
      this.getDate();
    },
    //跳转页数
    handleCurrentChange(val) {
      this.page.currentPage = val
      this.getDate();
    }
  },
  mounted() {
    this.getDate();
    //使table自适应窗口大小
    window.onresize = () => {
      return (() => {
        this.tableHeight = document.documentElement.clientHeight - this.$refs['table'].$el.getBoundingClientRect().top;
      })();
    };
  },
})