import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogFormVisible: false,
      //修改bom组别
      updBomForm: {
        bomTypeCode: '',
        bomTypeId: '',
        bomTypeName: '',
      },
      updBomFormRules: {
        bomTypeId: [
          {required: true, message: "请输入bom组别id", trigger: "blur"}
        ]
      },
      updBomLoading: false
    };
    
  },
  methods: {
    async updBomSubmit() {
      this.updBomLoading = true;
      //console.log(this.updBomForm["bomTypeCode"])
      try {
        //bomUpdateTypeInfo
        http(apiPath.material.bomUpdateTypeInfo, this.updBomForm , "PUT").then(res => {
          this.dialogFormVisible = false;
          this.$message.success(res.message);
          console.log(this.updBomForm)
          this.getTreeData();
        }).catch(err => {
          // console.log(err)
          this.$message({
            message: err.message.content,
            type: 'error'
          });
        })
        // const data = await this.$axios.put(this.$interface.BOM.updateTypeInfo, this.updBomForm);
        // this.dialogFormVisible = false;
        // this.$message.success(data.message);
        // console.log(this.updBomForm)
        // this.getTreeData();
      } finally {
        this.updBomLoading = false;
      }
    },
    toggle(selectType) {
      for(var item in this.updBomForm){
        this.updBomForm[item]=selectType[item]
      }
      this.dialogFormVisible = !this.dialogFormVisible;
    }
  },
  props:{
    getTreeData:Function
  }
};