import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogFormVisible: false,
      //新增bom组别
      addBomForm: {
        bomTypeCode: '',
        bomTypeName: '',
      },
      addBomFormRules: {
        bomTypeName: [
          {required: true, message: "请输入bom组别名称", trigger: "blur"},
          {min: 1, message: "名称不能为空，最少1个字符", trigger: "blur"}
        ],
      },
      addBomLoading: false,
    };
  },
  methods: {
    async addBomSubmit() {
      this.addBomLoading = true;
      //console.log(this.addBomForm["bomTypeCode"])
      try {
        http(apiPath.material.bomAddType, this.addBomForm , "POST").then(res => {
          this.dialogFormVisible = false;
          this.$message.success(res.message);
          this.getTreeData()
        }).catch(err => {
        // console.log(err)
          this.$message({
            message: err.message.content,
            type: 'error'
          });
        })
      } finally {
        this.addBomLoading = false;
      }
        
    },
    toggle() {
      this.dialogFormVisible = !this.dialogFormVisible;
    },},
  props:{
    getTreeData:Function
  }
}