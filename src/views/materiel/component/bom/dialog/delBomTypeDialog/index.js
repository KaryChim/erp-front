import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      selectType:[],
      dialogFormVisible: false,
      delBomLoading: false,
      dialogCheckVisible:false,
    };
  },
  methods: {
    async delBomSubmit(select) {
      console.log(this.selectType.bomTypeName)
      this.delBomLoading = true;
      //bomExistByTypeId
      var params = Object.assign({bomTypeId:this.selectType.bomTypeId})
      console.log(this.selectType.bomTypeId)
      http(apiPath.material.bomExistByTypeId, params , "GET").then(res => {
        if(res.result&&select===false){this.dialogCheckVisible=true;return;}
      }).catch(err => {
      // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
      try {
        http(apiPath.material.bomDeleteType, params , "DELETE").then(res => {
          this.dialogFormVisible = false;
          this.dialogCheckVisible=false;
          this.selectType.bomTypeId
          this.$message.success(res.message);
          this.getTreeData()
        }).catch(err => {
        // console.log(err)
          this.$message({
            message: err.message.content,
            type: 'error'
          });
        })
      } finally {
        this.delBomLoading = false;
      }
        
    },
    toggle(selectType) {
      this.selectType=selectType
      this.dialogFormVisible = !this.dialogFormVisible;
    }
  },
  props:{
    getTreeData:Function
  }
};