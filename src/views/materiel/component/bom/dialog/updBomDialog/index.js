import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
    //选择是与否
      dialogFormVisible: false,
      ruleForm: {
        bomId: '',
        materialName: '',
        bomTypeName: '',
        formNumber: '',
        auditStatus: '',
        status: '',
        formSource: '',
        number: '',
        cost: '',
        unitName: '',
        remark: '',
        bomDetailShines: [],
      },
      rules: {
        materialName: [
          { required: true, message: '请选择物料名', trigger: 'change' }
        ],
        materialCode: [
          { required: true, message: '请输入物料代码', trigger: 'change' }
        ],
        unitName: [
          { required: true, message: '请输入单位', trigger: 'change' }
        ],
        number: [
          { required: true, message: '请输入合成物料数量', trigger: 'change' }
        ],
        cost: [
          { required: true, message: '请输入费用', trigger: 'change' }
        ],
      },
      tempCols: [
        {
          prop: 'materialName',
          label: '物料名字(*)'
        },
        {
          prop: 'materialCode',
          label: '物料代码(*)'
        },
        {
          prop: 'status',
          label: '物料使用状态'
        },
        {
          prop: 'dosage',
          label: '用量(*)'
        },
        {
          prop: 'lossRate',
          label: '损耗率(*)'
        },
        // {
        //   prop: 'warehouseName',
        //   label: '发料仓库'
        // },
        {
          prop: 'unitName',
          label: '单位(*)'
        },
        {
          prop: 'remark',
          label: '备注'
        },
      ],
      sumitRuleForm: {
        bomId: '',
        materialName: '',
        bomTypeName: '',
        formNumber: '',
        auditStatus: '',
        status: '',
        formSource: '',
        number: '',
        cost: '',
        unitName: '',
        remark: '',
        bomDetailShines: [
        ],
      },
      BomField: [],
      id: -1000,
    }
  },
  methods: {
    toggle(row) {
      this.dialogFormVisible = true;
      console.log(row);
      this.getData(row.bomId);
    },
    //提交数据
    async onSumit(ref) {
      let pass = true;
      //检验数据是否已经全部填好
      this.ruleForm.bomDetailShines.forEach(item => {
        // const map = data => {
        //   index=0
        console.log(item.unitName)
        if (item.materialName === '' || item.materialCode === '' || item.dosage === '' || item.unitName === '' || item.lossRate === '') {
          this.$message.error('请将表格的必填项全部填入后才能创建bom单'); pass = false; return;
        }
        //   data.children && data.children.forEach(child => map(child));
        // }
        // map(item);
      })
      if (pass === false) return;
      //     //转化数据的格式才能提交（删除id，并且返回父结点）
      for (var element in this.ruleForm) {
        this.sumitRuleForm[element] = this.ruleForm[element]
      }
      console.log(this.ruleForm)
      this.$refs[ref].validate(async (valid) => {
        if (valid) {
          http(apiPath.material.bomUpdate, this.sumitRuleForm , "POST").then(res => {
            this.$message.success(res.message);
            this.dialogVisible = false
            this.$emit('func', this.ruleForm);
            this.dialogFormVisible = false
          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content,
              type: 'error'
            });
          })
        }
        else
          this.$message({ showClose: true, message: '提交失败', type: 'error', offset: 40 });
      });
    },
    //选择物料后，回填物料代码
    handleSelect(item, index) {
      console.log(item);
      index.materialCode = item.materialCode;
    },
    //BOM单表单
    async getBOMFormFiled() {
      this.BOMFormListLoading = true;
      const data = await this.$axios.get(this.$interface.BOM.BOMFormFiled);
      //打印console.log(data);
      this.BOMFormFiled = data.result;
      this.BOMFormListLoading = false;
    },
    //回填表格表格内的数据
    async getData(bomId) {
      console.log(bomId)
      var params = Object.assign({bomId: bomId})
      http(apiPath.material.bomById, params , "GET").then(res => {
        console.log(res)
        const bomTable = res.result.bom.table;
        const bomDetailTable = res.result.bomDetail.table;
        for(var item in this.ruleForm){
          this.ruleForm[item]=bomTable.bodies[0][item];
        }
        this.ruleForm.bomDetailShines=bomDetailTable.bodies
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询员工
    async querySearchAsync(queryString, cb) {
      var params = Object.assign({keyWord: queryString})
      http(apiPath.employee.employeeList, params , "GET").then(res => {
        const [result] = res.result;//data是后端返回的数据
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result.list);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询物料名
    async searchMaterialAsync(queryString, callback) {
      var params = Object.assign({keyword: queryString})
      http(apiPath.fuzzy.fuzzyMaterial, params , "GET").then(res => {
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(res.result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询仓库名
    async searchWarehouseNameAsync(queryString, callback) {
      let params = Object.assign({ keyWord:queryString })
      http(apiPath.fuzzy.listWarehouseByKeyWord, params , "GET").then(res => {
        const {result} = res;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //模糊查询计量单位
    async searchUnitAsync(queryString, callback) {
      let params = Object.assign({ keyword:queryString })
      http(apiPath.fuzzy.unit, params , "GET").then(res => {
        const {result} = res;//data是后端返回的数据
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          callback(result);
        }, 500 * Math.random());
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    //删行
    deleteRow(index) {
      this.ruleForm.bomDetailShines.splice(index, 1);
    },
    //添行
    addRow() {
      const newRow = { materialName: '', materialCode: '', status: '', dosage: '', lossRate: '', unitName: '', remark: '', show: false };
      this.ruleForm.bomDetailShines.push(newRow);
    },
  },
}