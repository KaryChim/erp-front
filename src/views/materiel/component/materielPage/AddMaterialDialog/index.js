import {
  http,
  apiPath
} from "@/api";
export default {
  data() {
    return {
      //选择是与否
      options: [
        {
          value: "是",
          label: "是",
        },
        {
          value: "否",
          label: "否",
        },
      ],
      autoListName: [
        "employeeNo",
        "warehouseName",
        "materialTypeName",
        "procurementEmployee",
        "baseUnit",
        "purchaseUnit",
        "saleUnit",
        "stockUnit",
        "unitGroup",
      ],
      //列表
      createLogisticsBaseDto: [
        {
          name: "单价显示精度",
          describe: "accuracyUnitPrice",
          accuracyUnitPrice: "",
          type: "el-input",
        },
        {
          name: "是否自动产生批号",
          describe: "autoGenerateNumber",
          autoGenerateNumber: "",
          type: "el-select",
        },
        {
          name: "是否进行保质期管理",
          describe: "expirationManagement",
          expirationManagement: "",
          type: "el-select",
        },
        {
          name: "毛利率（%）",
          describe: "grossMargin",
          grossMargin: "",
          type: "el-input",
        },
        {
          name: "存货科目代码",
          describe: "inventoryAccountCode",
          inventoryAccountCode: "",
          type: "el-input",
        },
        {
          name: "是否采用业务批次管理",
          describe: "management",
          management: "",
          type: "el-select",
        },
        {
          name: "保质期（天）",
          describe: "materialAge",
          materialAge: "",
          type: "el-input",
        },
        {
          name: "计划单价",
          describe: "planUnitPrice",
          planUnitPrice: "",
          type: "el-input",
        },
        {
          name: "计价方法",
          describe: "pricingMethod",
          pricingMethod: "",
          type: "el-input",
        },
        {
          name: "采购负责人",
          describe: "procurementEmployee",
          procurementEmployee: "",
          type: "el-autocomplete",
          event: this.name,
        },
        {
          name: "采购单价",
          describe: "purchaseUnitPrice",
          purchaseUnitPrice: "",
          type: "el-input",
        },
        {
          name: "销售成本科目代码",
          describe: "saleCostCode",
          saleCostCode: "",
          type: "el-input",
        },
        {
          name: "销售收入科目代码",
          describe: "saleRevenueCode",
          saleRevenueCode: "",
          type: "el-input",
        },
        {
          name: "销售单价",
          describe: "saleUnitPrice",
          saleUnitPrice: "",
          type: "el-input",
        },
        {
          name: "税率（%）",
          describe: "taxRate",
          taxRate: "",
          type: "el-input",
        },
      ],
      createMaterialBaseDto: [
        {
          name: "数量显示精度",
          describe: "accuracy",
          accuracy: "",
          type: "el-input",
        },
        {
          name: "基本计量单位(*)",
          describe: "baseUnit",
          baseUnit: "",
          type: "el-autocomplete",
          event: this.unitName,
        },
        // {
        //   name: '审核日期',
        //   describe: 'examineDate',
        //   examineDate: '',
        //   date: 'datetime',
        //   type: 'el-date-picker',
        // }, {
        //   name: '审核人员',
        //   describe: 'employeeNo',
        //   employeeNo: '',
        //   type: 'el-autocomplete',
        //   event: this.employeeNo
        // },
        //  {
        //   name: '审核标志',
        //   describe: 'exmineSign',
        //   exmineSign: '666',
        //   type: 'el-input',
        // },
        {
          name: "物料代码(*)",
          describe: "materialCode",
          materialCode: "",
          type: "el-input",
        },
        {
          name: "物料名(*)",
          describe: "materialName",
          materialName: "",
          type: "el-input",
        },
        {
          name: "客户编码",
          describe: "customerCode",
          customerCode: "",
          type: "el-input",
        },
        {
          name: "物料属性",
          describe: "materialProperties",
          materialProperties: "",
          type: "el-input",
        },
        {
          name: "物料备注",
          describe: "materialRemark",
          materialRemark: "",
          type: "el-input",
        },
        {
          name: "规格型号",
          describe: "materialSap",
          materialSap: "",
          type: "el-input",
        },
        {
          name: "物料组别名称(*)",
          describe: "materialTypeName",
          materialTypeName: "",
          type: "el-autocomplete",
          event: this.materialTypeName,
        },
        {
          name: "最高存量",
          describe: "maxSave",
          maxSave: "",
          type: "el-input",
        },
        {
          name: "最少存量",
          describe: "minSave",
          minSave: "",
          type: "el-input",
        },
        {
          name: "采购计量单位(*)",
          describe: "purchaseUnit",
          purchaseUnit: "",
          type: "el-autocomplete",
          event: this.unitName,
        },
        {
          name: "安全库存数量",
          describe: "safeStockQuantity",
          safeStockQuantity: "",
          type: "el-input",
        },
        {
          name: "销售计量单位(*)",
          describe: "saleUnit",
          saleUnit: "",
          type: "el-autocomplete",
          event: this.unitName,
        },
        {
          name: "单箱数",
          describe: "singleContainerNumber",
          singleContainerNumber: "",
          type: "el-input",
        },
        {
          name: "库存记录单位(*)",
          describe: "stockUnit",
          stockUnit: "",
          type: "el-autocomplete",
          event: this.unitName,
        },
        {
          name: "计量单位组(*)",
          describe: "unitGroup",
          unitGroup: "",
          type: "el-autocomplete",
          event: this.unitTypeName,
        },
        {
          name: "默认仓库",
          describe: "warehouseName",
          warehouseName: "",
          type: "el-autocomplete",
          event: this.warehouseName,
        },
      ],
      //用于添加的数组
      add: {
        createLogisticsBaseDto: {
          accuracyUnitPrice: 1,
          autoGenerateNumber: 1,
          expirationManagement: 1,
          grossMargin: 1,
          inventoryAccountCode: "存货科目代码",
          management: 1,
          materialAge: 30,
          planUnitPrice: 1,
          pricingMethod: "计价方法",
          procurementEmployee: "张三",
          purchaseUnitPrice: 1,
          saleCostCode: "销售成本科目代码",
          saleRevenueCode: "销售收入科目代码",
          saleUnitPrice: 1,
          taxRate: 1,
        },
        createMaterialBaseDto: {
          accuracy: 1,
          baseUnit: "台",
          // examineDate: "2020-2-2 11:11:11",
          // examineEmployee: "张三",
          // exmineSign: 1,
          materialCode: "物料代码",
          customerCode: "",
          materialName: "物料名称",
          materialProperties: "物料属性",
          materialRemark: "备注",
          materialSap: "规格型号",
          materialTypeName: "物料组别名称",
          maxSave: 1,
          minSave: 1,
          purchaseUnit: "台",
          safeStockQuantity: 1,
          saleUnit: "台",
          singleContainerNumber: 1,
          stockUnit: "台",
          unitGroup: "台",
          warehouseName: "01",
        },
      },
      dialogVisible: false,
    };
  },
  methods: {
    //调用toggle打开窗口
    toggle(selectType) {
      console.log(selectType)
      this.dialogVisible = true;
    },
    //添加物料
    async addMaterial() {
      this.createLogisticsBaseDto.forEach((item) => {
        this.add["createLogisticsBaseDto"][item.describe] = item[item.describe];
      });
      this.createMaterialBaseDto.forEach((item) => {
        this.add["createMaterialBaseDto"][item.describe] = item[item.describe];
      });

      let params = Object.assign(this.add);
      http(apiPath.materialPage.add, params, "POST").then((res) => {
        console.log(res);
        if (res.message=== "正常响应") {
          this.$message.success("物料添加成功");
          this.$parent.showMaterialGroupList();
          this.dialogVisible = false;
        } else {
          this.$message.error(res.result);
          this.$parent.showMaterialGroupList();
          this.dialogVisible = false;
        }
      });
      // this.$message({ message: "物料添加成功", type: "success" });
      // this.dialogVisible = false;
    },
    //模糊查询工人
    async name(queryString, cb) {
      let params = queryString ? Object.assign({size: 50},{ keyWord: queryString }): Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then((res) => {
        console.log(res);
        const {result} = res; //data是后端返回的数据
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      });
    },
    //模糊查询物料组别groupGetByName
    async materialTypeName(queryString, cb) {
      let params = Object.assign({ materialTypeName: queryString });
      http(apiPath.user.fuzzy.listMaterialByKeyWord, params, "GET").then(
        (res) => {
          console.log(res);
          let {result} = res;
          clearTimeout(this.timeout); //限定展示出来的时间
          this.timeout = setTimeout(() => {
            cb(result);
          }, 1000 * Math.random());
        }
      );
    },
    //模糊查询仓库
    async warehouseName(queryString, cb) {
      let params = Object.assign({ materialTypeName: queryString });
      http(apiPath.user.fuzzy.listWarehouseByKeyWord, params, "GET").then(
        (res) => {
          console.log(res);
          let {result} = res;
          clearTimeout(this.timeout); //限定展示出来的时间
          this.timeout = setTimeout(() => {
            cb(result);
          }, 300 * Math.random());
        }
      );
    },
    //模糊查询计量单位
    async unitName(queryString, cb) {
      let params = Object.assign({ materialTypeName: queryString });
      http(apiPath.materialPage.unit, params, "GET").then((res) => {
        console.log(res);
        let {result} = res;
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 300 * Math.random());
      });
    },
    //模糊查询计量单位组
    async unitTypeName(queryString, cb) {
      let params = Object.assign({ materialTypeName: queryString });
      http(apiPath.materialPage.unitType, params, "GET").then((res) => {
        console.log(res);
        let {result} = res;
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 300 * Math.random());
      });
    },
  },
  mounted() { },
};
