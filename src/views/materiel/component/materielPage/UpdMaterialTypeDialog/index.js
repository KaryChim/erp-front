import { http, apiPath } from "@/api";
export default {
  data() {
    return {
      warehouseMaterialTypeDto: {},
      warehouseMaterialTypeDtoRef: {
        materialTypeCode: "",
        materialTypeName: "",
        materialTypeRemark: "",
        materialTypeId: "",
      },
      dialogVisible: false,
    };
  },
  methods: {
    //回填物料种类信息
    async toggle(selectType) {
      this.dialogVisible = true;
      let params = Object.assign({ materialTypeName: selectType.label });
      http(apiPath.materialPage.groupGetByName, params, "GET").then(
        (res) => {
          console.log(res);
          this.warehouseMaterialTypeDto = res.result;
        }
      );
      // let data = await this.$axios.get(this.$interface.MaterialPage.groupGetByName,{params:{materialTypeName:selectType.label}})
      // this.warehouseMaterialTypeDto=data.result
    },
    //更新物料种类信息
    async updateMaterialType() {
      let params = Object.assign(this.warehouseMaterialTypeDto);
      http(apiPath.materialPage.groupModify, params, "PUT").then((res) => {
        console.log(res);
        if (res.result == true) {
          this.$message.success("信息更改成功");
          this.$parent.showMaterialGroupList();
          this.dialogVisible = false;
        } else {
          this.$message.error(res.result);
        }
      });
      //   let data = await this.$axios.put(this.$interface.MaterialPage.groupModify,this.warehouseMaterialTypeDto)
      //   this.$message({ message: "信息更改成功", type: "success" });
      //   this.$parent.showMaterialGroupList();
      //   this.dialogVisible = false;
    },
  },
};