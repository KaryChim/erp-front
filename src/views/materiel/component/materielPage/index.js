import addMaterialTypeDialog from "./AddMaterialTypeDialog/index.vue";
import delMaterialTypeDialog from "./DelMaterialTypeDialog/index.vue";
import updMaterialTypeDialog from "./UpdMaterialTypeDialog/index.vue";
import addMaterialDialog from "./AddMaterialDialog/index.vue";
import updDelMaterialDialog from "./UpdDelMaterialDialog/index.vue";
import addBatchMaterialDialog from "./AddBatchMaterialDialog/index.vue";
import { 
  http,
  apiPath 
} from "@/api";
export default {
  data() {
    return {
      tableHeight: 500,
      page: {
        currentPage: 1,
        pageSize: 10,
        total: 100,
      },
      menuVisible: false,
      materialTypeName: "",
      data: [
        {
          materialTypeId: 0,
          label: "物料组别",
          materialTypeCode: "materialTypeCode",
          materialTypeName: "materialTypeName",
          children: [],
        },
      ],
      defaultProps: {
        children: "children",
        label: "label",
        materialTypeId: "id",
        materialTypeCode: "materialTypeCode",
        materialTypeName: "materialTypeName",
      },
      basicMessageHeaders: [],
      basicMessageBodies: [],
      selectType: "",
      materialType: "",
      state: "",
    };
  },
  created() {
    this.showMaterialList();
  },
  methods: {
    //拉动div
    dragControllerDiv: function () {
      let resize = document.getElementById("resize");
      let left = document.getElementById("left");
      let right = document.getElementById("right");
      let MaterielPage = document.getElementById("MaterielPage");
      resize.onmousedown = function (e) {
        let startX = e.clientX;
        resize.left = resize.offsetLeft;
        document.onmousemove = function (e) {
          let endX = e.clientX;
          let moveLen = resize.left + (endX - startX);
          let maxT = MaterielPage.clientWidth - resize.offsetWidth;
          if (moveLen < 150) moveLen = 360;
          if (moveLen > maxT - 800) moveLen = maxT - 800;
          resize.style.left = moveLen;
          left.style.width = moveLen + "px";

          right.style.width = MaterielPage.clientWidth - moveLen - 5 + "px";
        };
        document.onmouseup = function () {
          document.onmousemove = null;
          document.onmouseup = null;
          resize.releaseCapture && resize.releaseCapture();
        };
        resize.setCapture && resize.setCapture();
        return false;
      };
    },
    //点击物料管理的结点树
    handleNodeClick(array) {
      this.materialType = array;
      this.page.currentPage = 1;
      this.page.pageSize = 10;
      this.showMaterialList();
      
    },
    //展示物料列表
    async showMaterialList() {
      let params = Object.assign({
        page: this.page.currentPage,
        size: this.page.pageSize,
        materialTypeName: this.materialType.materialTypeName,
      });
      http(apiPath.materialPage.list, params, "GET").then((res) => {
        console.log("展示物料列表");
        this.basicMessageHeaders = res.result.table.headers;
        this.basicMessageBodies = res.result.table.bodies;
        this.page.total = res.result.total;
      });
      // let data = await this.$axios.get(this.$interface.MaterialPage.list, {
      //   params: {
      //     page: this.page.currentPage,
      //     size: this.page.pageSize,
      //     materialTypeName: this.materialType.materialTypeName,
      //   },
      // });
      // this.basicMessageHeaders = data.result.table.headers;
      // this.basicMessageBodies = data.result.table.bodies;
      // this.page.total = data.result.total;
      window.onresize = () => {
        return (() => {
          this.tableHeight =
                        document.documentElement.clientHeight -
                        this.$refs["materialTable"].$el.getBoundingClientRect().top;
        })();
      };
      
    },
    //改变每页大小
    handleSizeChange(val) {
      this.page.pageSize = val;
      this.showMaterialList();
    },
    //跳转页数
    handleCurrentChange(val) {
      this.page.currentPage = val;
      this.showMaterialList();
    },
    //模糊查询物料
    async querySearchAsync(queryString, cb) {
      this.queryWarehouseName = queryString;
      this.page.currentPage = 1;
      this.page.pageSize = 50;
      let params = Object.assign({
        page: this.page.currentPage,
        size: this.page.pageSize,
        keyword: queryString,
      });
      http(apiPath.materialPage.byKeyword, params, "GET").then((res) => {
        console.log(res);
        let result = res.result.table.bodies;
        console.log(result);
        clearTimeout(this.timeout); //限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      });
      // let data = await this.$axios.get(this.$interface.MaterialPage.byKeyword, {
      //   params: {
      //     page: this.page.currentPage,
      //     size: this.page.pageSize,
      //     keyword: queryString,
      //   },
      // });
      // let result = data.result.table.bodies; //data是后端返回的数据
      // console.log(result);
      // clearTimeout(this.timeout); //限定展示出来的时间
      // this.timeout = setTimeout(() => {
      //   cb(result);
      // }, 500 * Math.random());
    },
    //点击模糊查询后
    async handleSelect() {
      this.page.currentPage = 1;
      this.page.pageSize = 10;
      let params = Object.assign({
        page: this.page.currentPage,
        size: this.page.pageSize,
        keyword: this.state,
      });
      http(apiPath.materialPage.byKeyword, params, "GET").then((res) => {
        console.log(res);
        this.basicMessageHeaders = res.result.table.headers;
        this.basicMessageBodies = res.result.table.bodies;
        this.page.total = res.result.total;
      });
      // let data = await this.$axios.get(this.$interface.MaterialPage.byKeyword, {
      //   params: {
      //     page: this.page.currentPage,
      //     size: this.page.pageSize,
      //     keyword: this.state,
      //   },
      // });
      // this.basicMessageHeaders = data.result.table.headers;
      // this.basicMessageBodies = data.result.table.bodies;
      // this.page.total = data.result.total;
    },
    //物料种类列表
    showMaterialGroupList() {
      this.data[0].children = [];
      http(apiPath.materialPage.groupList, {}, "GET").then((res) => {
        console.log(res);
        for (const item in res.result) {
          const newChild = {
            label: res.result[item].materialTypeName,
            materialTypeId: res.result[item].materialTypeId,
            materialTypeCode: res.result[item].materialTypeCode,
            materialTypeName: res.result[item].materialTypeName,
            children: [],
          };
          this.data[0].children.push(newChild);
        }
      });
      // let data = await this.$axios.get(this.$interface.MaterialPage.groupList);
      // for (const item in data.result) {
      //   const newChild = {
      //     label: data.result[item].materialTypeName,
      //     materialTypeId: data.result[item].materialTypeId,
      //     materialTypeCode: data.result[item].materialTypeCode,
      //     materialTypeName: data.result[item].materialTypeName,
      //     children: [],
      //   };
      //   this.data[0].children.push(newChild);
      // }
    },
    //BOM树右击增删改
    rightHandleNodeContextMenu(MouseEvent, data) {
      this.selectType = data;
      //鼠标右击触发事件
      this.menuVisible = false;
      //先把模态框关死，目的是第二次或者第N次右键鼠标的时候它的默认值是true
      this.menuVisible = true;
      var scrollTop =
                document.documentElement.scrollTop || document.body.scrollTop;
      //显示模态窗口，跳出自定义菜单栏
      var menu = document.querySelector("#menu");
      document.addEventListener("click", this.foo);
      //给整个document添加监听鼠标事件，点击任何位置执行foo方法
      menu.style.display = "block";
      menu.style.left = MouseEvent.clientX - 0 + "px";
      menu.style.top = MouseEvent.clientY + scrollTop - 200 + "px";
      console.log(menu.style.left);
    },
    //取消鼠标监听事件菜单栏
    foo() {
      this.menuVisible = false;
      document.removeEventListener("click", this.foo);
      //及时关掉监听！
    },
    //打开dialog
    openDialog(name) {
      this.$refs[name].toggle(this.selectType);
    },
    //点击某行打开窗口
    rowClick(row) {
      this.$refs["updDelMaterialDialog"].toggle(row);
    },
  },
  mounted() {
    this.dragControllerDiv();
    this.showMaterialGroupList();
    window.onresize = () => {
      return (() => {
        this.tableHeight =
                    document.documentElement.clientHeight -
                    this.$refs["materialTable"].$el.getBoundingClientRect().top;
      })();
    };
  },
  components: {
    "add-material-type-dialog": addMaterialTypeDialog,
    "del-material-type-dialog": delMaterialTypeDialog,
    "upd-material-type-dialog": updMaterialTypeDialog,
    "add-material-dialog": addMaterialDialog,
    "upd-del-material-dialog": updDelMaterialDialog,
    "add-batch-material-dialog": addBatchMaterialDialog,
  },
};