import { http, apiPath } from "@/api";
export default {
  data() {
    return {
      dialogVisible: false,
      selectType: "",
    };
  },
  methods: {
    toggle(selectType) {
      this.selectType = selectType;
      this.dialogVisible = true;
    },
    async delMaterialType() {
      let params = Object.assign({ materialTypeName: this.selectType.label });
      http(apiPath.materialPage.groupDelete, params, "DELETE").then(
        (res) => {
          console.log(res);
          if (res.result == true) {
            this.$message.success("种类删除成功");
            this.$parent.showMaterialGroupList();
            this.dialogVisible = false;
          } else {
            this.$message.error(res.result);
            this.$parent.showMaterialGroupList();
            this.dialogVisible = false;
          }
        }
      );
    },
  },
};