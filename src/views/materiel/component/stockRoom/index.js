import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"
import AddStockRoom from './AddStockRoom/index.vue'
import EditStockRoom from './EditStockRoom/index.vue'
export default created({
  props: {
    addTab: Function,
  },
  components: {
    "add-stock-room": AddStockRoom,
    "edit-stock-room": EditStockRoom,
  },
  mixins: [],
  data() {
    return {
      timeout: null,
      headers: [],
      bodies: [],
      currentPage: 1,
      pageSize: 10,
      total: 0,
      deletedWarehouseName: '',
      queryWarehouseName: '',
    }
  },
  computed: {},
  created() {
    console.log("stockRoom");
    this.showStockroom()
    // console.log(this.addTab('materiel-page'))
  },
  watch: {},
  methods: {
    //双击打开仓存
    edit(row) {
      localStorage.warehouseName = JSON.stringify(row.warehouseName)
      this.addTab('storage')
    },
    openDialog(name) {
      //console.log(this.$refs)
      this.$refs[name].toggle();
    },
    //仓库表
    async showStockroom() {
      //console.log(apiPath.user.material.list);
      http(apiPath.user.material.list(this.currentPage, this.pageSize), {}, "GET").then(res => {
        this.headers = res.result.table.headers;
        this.bodies = res.result.table.bodies;
        this.total = res.result.total;
        //console.log(res)      
      })
      // let data = await this.$axios.get(this.$interface.Warehouse.list(this.currentPage, this.pageSize));
    },
    //点击处理页码大小
    handleSizeChange(val) {
      this.pageSize = val;
      this.showStockroom();

    },
    //点击当前页变动时候触发
    handleCurrentChange(val) {
      this.currentPage = val;
      this.showStockroom();
    },
    //删除仓库
    deleteStockroom(deletedWarehouseName) {
      this.$confirm('此操作将永久删除该文件, 是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      }).then(async () => {
        let params = Object.assign({ warehouseName: this.deletedWarehouseName })
        http(apiPath.user.material.delete, params, "DELETE").then(res => {
          console.log(res)
          if (res.result) {
            this.$message.success(res.result);
          }
        })
      })
      this.deletedWarehouseName = deletedWarehouseName;
      //let data = await this.$axios.delete(this.$interface.Warehouse.delete, { params: { warehouseName: this.deletedWarehouseName } });
      //   if (data.result) {
      //     this.$message.success("删除成功");
      //   }
      //   this.showStockroom();
      // }).catch(() => {
      //   this.$message({
      //     type: 'info',
      //     message: '已取消删除'
      //   });
      // });
    },
    //编辑仓库信息
    openDialogEdit(name, sotckroomName) {
      console.log(name, sotckroomName)
      this.$refs[name].toggle(sotckroomName);
    },
    //模糊查询仓库
    async querySearchAsync(queryString, cb) {
      this.queryWarehouseName = queryString;
      let params = Object.assign({ keyWord: this.queryWarehouseName })
      http(apiPath.user.material.list(this.currentPage, this.pageSize), params, "GET").then(res => {
        //console.log(res)
        let result = res.result.table.bodies;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      })
    },
    async downLoad() {
      //exportWarehouseData
      // let params = Object.assign({ keyWord: this.queryWarehouseName })
      http(apiPath.user.material.exportWarehouseData, { responseType: 'blob' }, "GET").then(res => {
        console.log(res)
        if (!res) { this.$Message.error('下载内容为空'); return; }
        let url = window.URL.createObjectURL(new Blob([res]))
        let link = document.createElement('a')
        link.style.display = 'none'
        link.href = url
        link.setAttribute('download', '仓库资料' + '.xlsx')
        document.body.appendChild(link)
        link.click()
        //释放URL对象所占资源
        window.URL.revokeObjectURL(url)
        //用完即删
        document.body.removeChild(link)
      })
      //let data = await this.$axios.get(this.$interface.Warehouse.exportWarehouseData, { responseType: 'blob' });
      // console.log(data)
      // if (!data) { this.$Message.error('下载内容为空'); return; }
      // let url = window.URL.createObjectURL(new Blob([data.data]))
      // let link = document.createElement('a')
      // link.style.display = 'none'
      // link.href = url
      // link.setAttribute('download', '仓库资料' + '.xlsx')
      // document.body.appendChild(link)
      // link.click()
      // //释放URL对象所占资源
      // window.URL.revokeObjectURL(url)
      // //用完即删
      // document.body.removeChild(link)
    },
    header({row }) {
      console.log(row)
      return 'font-weight: 900'
    }
  }
})
