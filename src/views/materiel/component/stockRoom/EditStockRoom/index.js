import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
//import {eventBus} from "common/socket/event";

export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data() {
    this.templateData = [
      {
        label: '仓库是否还接受新物料',
        model: 'acceptNew',
        components: {
          name: 'el-select',
          op: true,
          option: [{
            value: true,
            label: '是'
          }, {
            value: false,
            label: '否'
          }],
        },
      },
      {
        label: '仓库地址',
        model: 'warehouseAddress',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库代码',
        model: 'warehouseCode',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库助记码',
        model: 'warehouseMnemonic',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库名',
        model: 'warehouseName',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库座机',
        model: 'warehouseOperator',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '备注',
        model: 'warehouseRemarks',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库类型',
        model: 'warehouseTypeCode',
        components: {
          name: 'el-select',
          op: true,
          option: [{
            value: 'ST01',
            label: '普通仓'
          },
          {
            value: 'ST06',
            label: '其他仓'
          },
          {
            value: 'ST03',
            label: '虚仓'
          }],

        },
      },
    ];
    return {
      timeout: null,
      dialogVisible: false,
      stockRoomForm: {
        acceptNew: "",
        administratorEmployeeNo: "",
        warehouseAddress: "",
        warehouseCode: "",
        warehouseMnemonic: "",
        warehouseId: "",
        warehouseName: "",
        warehouseOperator: "",
        warehouseRemarks: "",
        warehouseTypeCode: "",
      },
      stockRoomFormRules: {
        warehouseName: [
          { required: true, message: '请输入仓库名', trigger: 'blur' },
        ],
        warehouseCode: [
          { required: true, message: '请输入仓库代码', trigger: 'blur' },
        ],
        warehouseTypeCode: [
          { required: true, message: '请输入仓库类型', trigger: 'blur' },
        ],
      },
    }
  },
  computed: {},
  created() {
    //
  },
  destroyed() {
    // 关闭页面时，关闭订阅器

  },
  watch: {},
  methods: {
    dealTemplate() {
      console.log("editstockroom")
    },
    async toggle(stockroomName) {

      let params = Object.assign({keyWord: stockroomName})
      // this.dialogVisible = !this.dialogVisible;
      http(apiPath.user.material.list(0, 0), params, "GET").then(res => {
        console.log(res)
        this.bodies = res.result.table.bodies;
        if (res.result.table.bodies[0].acceptNew) { this.stockRoomForm['acceptNew'] = "是"; }
        else { this.stockRoomForm['acceptNew'] = "否"; }
        this.stockRoomForm.warehouseId = res.result.table.bodies[0].warehouseId;
        this.stockRoomForm.administratorEmployeeNo = res.result.table.bodies[0].administratorEmployeeNo
        this.stockRoomForm.warehouseAddress = res.result.table.bodies[0].warehouseAddress
        this.stockRoomForm.warehouseCode = res.result.table.bodies[0].warehouseCode
        this.stockRoomForm.warehouseMnemonic = res.result.table.bodies[0].warehouseMnemonic
        this.stockRoomForm.warehouseName = res.result.table.bodies[0].warehouseName
        this.stockRoomForm.warehouseOperator = res.result.table.bodies[0].warehouseOperator
        this.stockRoomForm.warehouseRemarks = res.result.table.bodies[0].warehouseRemarks
        this.stockRoomForm.warehouseTypeCode = res.result.table.bodies[0].warehouseTypeCode
        this.dialogVisible = !this.dialogVisible;
      })
    },
    //修改仓库信息
    async editStockroom() {
      this.$refs.stockRoomFormRef.validate(async valid => {
        if (!valid) {
          this.$message({ showClose: true, message: '请输入必填项', type: 'error' });
          return;
        }

        if (this.stockRoomForm.acceptNew == '是') { this.stockRoomForm.acceptNew = true; }
        else if (this.stockRoomForm.acceptNew == '否') { this.stockRoomForm.acceptNew = false; }
        let params = Object.assign(this.stockRoomForm)
        console.log(params)
        http(apiPath.user.material.modify, params, "PUT", {}).then(res => {
          console.log(res)
          this.$message.success("修改仓库成功");
        })
        // let data = await this.$axios.put(this.$interface.Warehouse.modify, this.stockRoomForm);
        // this.$message.success("修改仓库成功");
        this.dialogVisible = !this.dialogVisible;
      })
    },
    //负责人的名字选取
    // async querySearchAsync(queryString, cb) {
    //   const data = await this.$axios.get(
    //     this.$interface.Employee.list(1, 50), { params: { keyWord: queryString } });
    //   const result = data.result;//data是后端返回的数据
    //   clearTimeout(this.timeout);//限定展示出来的时间
    //   this.timeout = setTimeout(() => {
    //     cb(result.list);
    //   }, 500 * Math.random());
    // },
    async querySearchAsync(queryString, cb) {
      this.queryWarehouseName = queryString;
      let params = queryString ? Object.assign({size: 50},{ keyWord: this.queryWarehouseName }) : Object.assign({size: 50})
      http(apiPath.employee.fuzzy, params, "GET").then(res => {
        // console.log(res)
        let {result} = res;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      })
    },
  }
})
