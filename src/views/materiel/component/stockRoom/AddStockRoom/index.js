import {
  http,
  apiPath
} from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"
//import addBomDialog from '../../bom/dialog/addBomDialog/index'

export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data () {
    this.templateData = [
      {
        label: '仓库是否还接受新物料',
        model: 'acceptNew',
        components: {
          name: 'el-select',
          op: true,
          option: [{
            value: 'true',
            label: '是'
          }, {
            value: 'false',
            label: '否'
          }],
        },
      },
      {
        label: '仓库地址',
        model: 'warehouseAddress',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库代码',
        model: 'warehouseCode',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库助记码',
        model: 'warehouseMnemonic',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库名',
        model: 'warehouseName',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库座机',
        model: 'warehouseOperator',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '备注',
        model: 'warehouseRemarks',
        components: {
          name: 'el-input',
          op: false,
          option: '',
        },
      },
      {
        label: '仓库类型',
        model: 'warehouseTypeCode',
        components: {
          name: 'el-select',
          op: true,
          option: [{
            value: 'ST01',
            label: '普通仓'
          },
          {
            value: 'ST06',
            label: '其他仓'
          },
          {
            value: 'ST03',
            label: '虚仓'
          }],
        },
      },
    ];
    return {
      labelPosition: 'right',
      stockRoomFormRules: {
        warehouseName: [
          { required: true, message: '请输入仓库名', trigger: 'blur' },
        ],
        warehouseCode: [
          { required: true, message: '请输入仓库代码', trigger: 'blur' },
        ],
        warehouseTypeCode: [
          { required: true, message: '请输入仓库类型', trigger: 'blur' },
        ],
      },
      dialogVisible: false,
      stockRoomForm: {
        acceptNew: "",
        administratorEmployeeNo: "",
        warehouseAddress: "",
        warehouseCode: "",
        warehouseMnemonic: "",
        warehouseName: "",
        warehouseOperator: "",
        warehouseRemarks: "",
        warehouseTypeCode: "",
        //用于模糊查询的仓库管理员编号    
      },

    }
  },
  computed: {},
  created () {
    // 新建接受订阅器
    console.log("addstockroom")
  },
  destroyed() {
    // 关闭页面时，关闭订阅器
    console.log("close addstockroom")
  },
  watch: {},
  methods: {
    toggle() {
      this.dialogVisible = !this.dialogVisible;
    },
    // 添加仓库
    async addStockroom() {
      this.$refs.stockRoomFormRef.validate(async valid => {
        if (!valid) {
          this.$message({ showClose: true, message: '请输入必填项', type: 'error' });
          return;
        }
        let params = Object.assign(this.stockRoomForm)
        http(apiPath.user.material.create, params, "POST").then(res => {
          console.log(res)
        })
        //let data = await this.$axios.post(this.$interface.Warehouse.create, this.stockRoomForm);
        this.$message.success("添加仓库成功");
        this.dialogVisible = !this.dialogVisible;
      })
    },
    //负责人的名字选取
    async querySearchAsync(queryString, cb) {
      console.log("hhhh")
      this.queryWarehouseName = queryString;
      let params = Object.assign({ keyWord: this.queryWarehouseName })
      http(apiPath.user.material.list(1, 50), params, "GET").then(res => {
        console.log(res)
        let {result} = res;
        clearTimeout(this.timeout);//限定展示出来的时间
        this.timeout = setTimeout(() => {
          cb(result);
        }, 500 * Math.random());
      })
      // const data = await this.$axios.get(
      //   this.$interface.Employee.list(1, 50),
      //   { params: { keyWord: queryString } }
      // );
      // const result = data.result;//data是后端返回的数据
      // clearTimeout(this.timeout);//限定展示出来的时间
      // this.timeout = setTimeout(() => {
      //   cb(result.list);
      // }, 500 * Math.random());
    },
  }
})
