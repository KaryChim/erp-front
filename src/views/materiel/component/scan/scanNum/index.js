// import {
//   http,
//   apiPath
// } from "@/api"
// import Utils from '../../../basic/util'
export default {
  data() {
    return {
      //选择是与否
      isIndeterminate: false,   //默认全选
      dialogFormVisible: false,
      options: [],
      value: [],
      list: [],
      loading: false,
      states: ["Alabama", "Alaska", "Arizona",
        "Arkansas", "California", "Colorado",
        "Wyoming"],
      ruleForm: {
        material:'',
        num:''
      },
      input:'',
      id:2,
      rules:{
        material: [
          { required: true, message: '请选择物料', trigger: 'change' }
        ],
        num:[
          { required: true, message: '请输入数量', trigger: 'change' }
        ],
      },
      dialogVisible:false
    }
  },
  methods: {
    //调用toggle打开窗口
    toggle() {
      this.dialogVisible = true;
    },
  },

  mounted() {

  }
}
