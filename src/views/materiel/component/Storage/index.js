import { http, apiPath } from "@/api";
export default {
  data() {
    return {
      loading: false,
      warehouseName: "",
      page: {
        currentPage: 1,
        pageSize: 10,
        total: 10,
      },
      tableHeight: 500,
      tableHead: [],
      tableData: [],
      sonHead: [],
    };
  },
  mounted() {
    this.warehouseName = JSON.parse(localStorage.warehouseName);
    this.getMaterialInfo();
    return (() => {
      this.tableHeight =
                document.documentElement.clientHeight -
                this.$refs["table"].$el.getBoundingClientRect().top;
    })();
  },
  methods: {
    //根据仓库名获取物料信息
    async getMaterialInfo() {
      this.tableData = [];
      let index = 1;
      let params = Object.assign({
        pageNum: this.page.currentPage,
        pageSize: this.page.pageSize,
        warehouseName: this.warehouseName,
      });
      http(apiPath.user.material.materialInfo, params, "GET").then((res) => {
        res.result.table.bodies.forEach((element) => {
          element.id = index++;
          element.hasChildren = [];
          this.tableData.push(element);
        });
        console.log(res);
        this.tableHead = res.result.table.headers;
        this.sonHead = res.result.table.headers;
        console.log(this.sonHead);
        return () => {
          this.tableHeight =
                        document.documentElement.clientHeight -
                        this.$refs["table"].$el.getBoundingClientRect().top;
        };
      });
    },
    headerStyle({ row, column, rowIndex, columnIndex }) {
      console.log(row,column,columnIndex,rowIndex)
      return "tableStyle";
    },
    handleSizeChange(val) {
      this.page.pageSize = val;
      this.getMaterialInfo();
    },
    //设置表格内容的颜色
    tableRowClassName({ row, rowIndex }) {
      console.log(row,rowIndex)
      return "warning-row";
    },
    handleCurrentChange(val) {
      this.page.currentPage = val;
      this.getMaterialInfo();
    },
    async open(row) {
      let params = Object.assign({
        warehouseName: this.warehouseName,
        materialCode: row.materialCode,
        id: row.id,
      });
      http(apiPath.user.material.materialDetail, params, "GET").then((res) => {
        this.tableData[row.id - 1].hasChidren = res.result.table.bodies;
        this.sonHead = res.result.table.headers;
      });
      //   let data = await this.$axios.get(
      //     this.$interface.Warehouse.materialDetail,
      //     {
      //       params: {
      //         warehouseName: this.warehouseName,
      //         materialCode: row.materialCode,
      //         id: row.id,
      //       },
      //     }
      //   );
      //   this.tableData[row.id - 1].hasChidren = data.result.table.bodies;
      //   this.sonHead = data.result.table.headers;
    },
  },
}