import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"
import AddMenu from "./components/addMenu/index.vue"
import EditMenu from "./components/editMenu/index.vue"
import RoleAssignment from "./components/roleAssignment/index.vue"
import DeleteDepartment from "./../../home/public/Delete/index.vue"

export default created({
  props: {},
  components: {
    EditMenu,
    AddMenu,
    RoleAssignment,
    DeleteDepartment
  },
  mixins: [],
  data () {
    return {
      value: true,
      name: 'template',
      msg: "Welcome to Your Vue.js "+ name +" App",
      restaurants: [],
      state: '',
      timeout:  null,
      tableData: [
      ],
      tableHead:[
        {props:'id',
          describe:'ID'
        },
        {props:'name',
          describe:'菜单名'
        },
        {props:'path',
          describe:'路径'
        }],
      page:{
        total:10,
        pageSize:20,
        currentPage:1,
        bread:[{pid:0,name:'首页'}],
        pid:0,
      },
      loading:true
    }
  },
  computed: {},
  created () {
    this.restaurants = this.loadAll();
  },
  watch: {},
  methods: {
    loadAll() {
      return [
        { "value": "三全鲜食（北新泾店）", "address": "长宁区新渔路144号" },
        { "value": "Hot honey 首尔炸鸡（仙霞路）", "address": "上海市长宁区淞虹路661号" },
        { "value": "新旺角茶餐厅", "address": "上海市普陀区真北路988号创邑金沙谷6号楼113" },
        { "value": "泷千家(天山西路店)", "address": "天山西路438号" },
        { "value": "胖仙女纸杯蛋糕（上海凌空店）", "address": "上海市长宁区金钟路968号1幢18号楼一层商铺18-101" },
        { "value": "贡茶", "address": "上海市长宁区金钟路633号" },
        { "value": "豪大大香鸡排超级奶爸", "address": "上海市嘉定区曹安公路曹安路1685号" },
        { "value": "茶芝兰（奶茶，手抓饼）", "address": "上海市普陀区同普路1435号" },
        { "value": "十二泷町", "address": "上海市北翟路1444弄81号B幢-107" },
        { "value": "星移浓缩咖啡", "address": "上海市嘉定区新郁路817号" },
        { "value": "阿姨奶茶/豪大大", "address": "嘉定区曹安路1611号" },
        { "value": "新麦甜四季甜品炸鸡", "address": "嘉定区曹安公路2383弄55号" },
        { "value": "Monica摩托主题咖啡店", "address": "嘉定区江桥镇曹安公路2409号1F，2383弄62号1F" },
        { "value": "浮生若茶（凌空soho店）", "address": "上海长宁区金钟路968号9号楼地下一层" },
        { "value": "NONO JUICE  鲜榨果汁", "address": "上海市长宁区天山西路119号" },
        { "value": "CoCo都可(北新泾店）", "address": "上海市长宁区仙霞西路" },
        { "value": "快乐柠檬（神州智慧店）", "address": "上海市长宁区天山西路567号1层R117号店铺" },
        { "value": "Merci Paul cafe", "address": "上海市普陀区光复西路丹巴路28弄6号楼819" },
        { "value": "猫山王（西郊百联店）", "address": "上海市长宁区仙霞西路88号第一层G05-F01-1-306" },
        { "value": "枪会山", "address": "上海市普陀区棕榈路" },
        { "value": "纵食", "address": "元丰天山花园(东门) 双流路267号" },
        { "value": "钱记", "address": "上海市长宁区天山西路" },
        { "value": "壹杯加", "address": "上海市长宁区通协路" },
        { "value": "唦哇嘀咖", "address": "上海市长宁区新泾镇金钟路999号2幢（B幢）第01层第1-02A单元" },
        { "value": "爱茜茜里(西郊百联)", "address": "长宁区仙霞西路88号1305室" },
        { "value": "爱茜茜里(近铁广场)", "address": "上海市普陀区真北路818号近铁城市广场北区地下二楼N-B2-O2-C商铺" },
        { "value": "鲜果榨汁（金沙江路和美广店）", "address": "普陀区金沙江路2239号金沙和美广场B1-10-6" },
        { "value": "开心丽果（缤谷店）", "address": "上海市长宁区威宁路天山路341号" },
      ];
    },
    // querySearchAsync(queryString, cb) {
    //   var params = Object.assign({keyWord:queryString},{size:100})
    //   http(apiPath.account.fuzzyDepartment, params, "GET").then(res => {
    //     clearTimeout(this.timeout);
    //     this.timeout = setTimeout(() => {
    //       cb(res.result);
    //     }, 1000 * Math.random());
    //   }).catch(err => {
    //     // console.log(err)
    //     this.$message({
    //       message: err.message.content,
    //       type: 'error'
    //     });
    //   })
    // },
    getMenuList(){
      this.loading=true
      var params = Object.assign({pid:this.page.pid},{pageNum:this.page.currentPage},{pageSize:this.page.pageSize})
      http(apiPath.account.menuList, params, "GET").then(res => {
        console.log(res)
        console.log(res.result.records)
        this.tableData=res.result.records
        this.page.total=res.result.total
        this.loading=false
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    querySearchAsync(queryString, cb) {
      var results = queryString ? this.restaurants.filter(this.createStateFilter(queryString)) : this.restaurants;

      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        cb(results);
      }, 3000 * Math.random());
    },
    createStateFilter(queryString) {
      return (state) => {
        return (state.value.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
      };
    },
    handleSelect(item) {
      console.log(item);
    },
    handleSizeChange(val) {
      this.page.pageSize=val
      this.fresh()
    },
    handleCurrentChange(val) {
      this.page.currentPage=val
      this.fresh()
    },
    openDialog(name,row){
      console.log(row)
      console.log(this.$refs[name].toggle(row,this.page))
    },
    handleClick(row,value) {
      console.log(row);
      console.log(value)
      var params={menuId:row.id,display:value}
      http(apiPath.account.menuDisplay, params, "POST",'f').then(res => {
        this.$message({
          message: res.message,
          type: 'success'
        });
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    son(row){
      var bread={pid:row.id,name:row.name}
      // eslint-disable-next-line prefer-destructuring
      this.page.bread.push(bread)
      this.page.pid=row.id
      this.getMenuList()
    },
    openDeleteDialog(row){
      var params=Object.assign({menuId:row.id})
      this.$refs['deleteDepartment'].toggle('菜单',row.name,apiPath.account.menuDelete,params,'确定删除该菜单？删除后拥有该菜单的用户也将失去相应的菜单权限')
    },
    fresh(){
      this.getMenuList()
    },
    goBread(item){
      var length=0
      this.page.pid=item.pid
      this.getMenuList()
      for(var index in this.page.bread){
        console.log(index.pid)
        if(item.pid==this.page.bread[index].pid){break;}
        length++
      }
      console.log(length)
      this.page.bread.splice(length+1,this.page.bread.length-length+1)
    }
  },
  mounted(){
    this.getMenuList()
  }
})
