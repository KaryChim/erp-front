import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        name: '',
        pid:0,
        path:'',
        menuDescribe:''
      },
      rules: {
        name: [
          { required: true, message: '请输入部门名称', trigger: 'blur' },
        ],
        path: [
          { required: true, message: '请输入部门名称', trigger: 'blur' },
        ]
      },
    };
  },
  methods: {
    handleClose() {

    },
    toggle(row,page){
      this.form.pid=page.pid
      this.dialogVisible=!this.dialogVisible
    },
    onSubmit(){
      console.log(this.form)
      this.$refs['form'].validate((valid) => {
        if (valid) {
          http(apiPath.account.menuAdd, this.form, "POST").then(res => {
            console.log(res)
            this.$parent.fresh()
            this.$message({
              message: res.message,
              type: 'success'
            });
            this.dialogVisible=false
          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content,
              type: 'error'
            });
          })
          this.dialogVisible = false
          this.$parent.fresh()
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
  }
}
