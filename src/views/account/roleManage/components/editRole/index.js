import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        name: '',
        describe: '',
        id:0,
      }
    };
  },
  methods: {
    handleClose() {

    },
    toggle(row){
      console.log(row)
      this.form.name = row.name
      this.form.describe = row.describe
      this.form.id = row.id
      this.dialogVisible=!this.dialogVisible
    },
    updateEmployee() {
      let params = Object.assign({ 
        id: this.form.id,            //ID
        describe :this.form.describe,
        name:this.form.name
      })
      http(apiPath.account.roleUpdate, params, "POST").then(res => {
        console.log(res)
        if (res.code == '20000') {
          this.$message.success("修改成功");
          this.$parent.loadAll();
          this.dialogVisible = false;
        } else {
          this.$message.error(res.result);
        }
      })
    },

  }
}
