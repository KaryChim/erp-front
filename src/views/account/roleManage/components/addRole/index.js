import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        name: '',
        describe: ''
      }
    };
  },
  methods: {
    addRole(){
      let params = Object.assign({
        name: this.form.name,         //角色名稱    
        describe: this.form.describe      //角色描述
      })
      http(apiPath.employee.roleadd, params, "POST").then(res => {
        console.log(res)
        if (res.result == "添加成功") {
          this.$message.success("添加成功");
          this.$parent.loadAll();
          this.dialogVisible = false;
        } else {
          this.$message.error(res.result);
        }
      })
    },
    handleClose() {

    },
    toggle(){
      this.dialogVisible=!this.dialogVisible
    }
  }
}
