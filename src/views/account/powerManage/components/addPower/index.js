import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        default: true,
        id: 0,
        method: "",
        name: "",
        pid: 0,
        powerDescribe: "",
        url: ""
      }
    };
  },
  methods: {
    handleClose() {

    },
    toggle(){
      this.dialogVisible=!this.dialogVisible
    },
    onSubmit(){
      console.log(this.form)
      this.$refs['form'].validate((valid) => {
        if (valid) {
          http(apiPath.account.powerAdd, this.form , "POST").then(res => {
            this.$message({
              message: res.message,
              type: 'success'
            });
            this.$parent.fresh()
            this.dialogVisible = false
          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content,
              type: 'error'
            });
          })
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    }
  }
}
