import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        id:'',
        name: '',
        url:'',
        method:'',
        powerDescribe:'',
        default:true,
      },
      row:{}
    };
  },
  methods: {
    handleClose() {

    },
    toggle(row){
      this.row=row
      this.init(row)
      this.dialogVisible=!this.dialogVisible
    },
    onSubmit(){
      console.log(this.form)
      this.$refs['form'].validate((valid) => {
        if (valid) {
          http(apiPath.account.powerUpdate, this.form, "POST").then(res => {
            console.log(res)
            this.$parent.fresh()
            this.$message({
              message: res.message,
              type: 'success'
            });
            this.dialogVisible=false
          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content,
              type: 'error'
            });
          })
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
    init(row){
      this.form.id=row.id
      this.form.name=row.name
      this.form.url=row.url
      this.form.method=row.method
      this.form.powerDescribe=row.powerDescribe
      this.form.default=row.default
      console.log(row)
    },
  }
}
