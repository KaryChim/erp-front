// import {
//   http,
//   apiPath
// } from "@/api"
import HelloWorld from 'components/HelloWorld'
import created from "mixins/created.js"

export default created({
  props: {},
  components: {
    HelloWorld
  },
  mixins: [],
  data () {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js "+ name +" App"
    }
  },
  computed: {},
  created () {

  },
  watch: {},
  methods: {

  }
})
