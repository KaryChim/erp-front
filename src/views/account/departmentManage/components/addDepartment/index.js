import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        code:'',
        name: '',
        remarks: '',
        pid:0
      },
      rules: {
        name: [
          { required: true, message: '请输入部门名称', trigger: 'blur' },
        ],
        code: [
          { required: true, message: '请输入部门代码', trigger: 'blur' },
        ],
      },
      
    };
  },
  methods: {
    handleClose() {

    },
    toggle(row,page){
      this.form.pid=page.pid
      this.dialogVisible=!this.dialogVisible
    },
    onSubmit(){
      console.log(this.form)
      this.$refs['form'].validate((valid) => {
        if (valid) {
          http(apiPath.account.departmentAdd, this.form, "POST").then(res => {
            this.$message({
              message: res.message,
              type: 'success'
            });
            this.$parent.fresh()
            this.dialogVisible = false
          }).catch(err => {
            // console.log(err)
            this.$message({
              message: err.message.content,
              type: 'error'
            });
          })
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    },
  }
}
