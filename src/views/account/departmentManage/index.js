import {
  http,
  apiPath
} from "@/api"
import created from "mixins/created.js"
import AddDepartment from "./components/addDepartment/index.vue"
import EditDepartment from "./components/editDepartment/index.vue"
import DeleteDepartment from "./../../home/public/Delete/index.vue"

export default created({
  props: {},
  components: {
    AddDepartment,
    EditDepartment,
    DeleteDepartment,
  },
  mixins: [],
  data () {
    return {
      name: 'template',
      msg: "Welcome to Your Vue.js "+ name +" App",
      departments: [],
      state: '',
      loading:true,
      timeout:  null,
      tableData: [],
      tableHead:[
        {props:'id',
          describe:'ID'
        },
        {props:'name',
          describe:'部门名称'
        },
        {props:'remarks',
          describe:'描述'
        }],
      page:{
        total:10,
        pageSize:20,
        currentPage:1,
        bread:[{pid:0,name:'首页'}],
        pid:0,
      },
    }
  },
  computed: {},
  created () {
  },
  mounted(){this.bread='';this.getDepartmentList()},
  watch: {},
  methods: {
    querySearchAsync(queryString, cb) {
      var params = Object.assign({keyWord:queryString},{size:this.page.total})
      http(apiPath.account.fuzzyDepartment, params, "GET").then(res => {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          cb(res.result);
        }, 1000 * Math.random());
      }).catch(err => {

        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    handleSelect(item) {
      console.log(item);
    },
    handleSizeChange(val) {
      this.page.pageSize=val
      this.fresh()
    },
    handleCurrentChange(val) {
      this.page.currentPage=val
      this.fresh()
    },
    openDialog(name,row){
      console.log(this.$refs[name].toggle(row,this.page))
      this.fresh()
    },
    son(row){
      var bread={pid:row.id,name:row.name}
      // eslint-disable-next-line prefer-destructuring
      this.page.bread.push(bread)
      this.page.pid=row.id

      this.getDepartmentList()
      console.log(this.page.bread)
    },
    getDepartmentList(){
      this.loading=true
      var params = Object.assign({},{pid:this.page.pid},{pageNum:this.page.currentPage},{pageSize:this.page.pageSize})
      if (this.state) params = Object.assign(params,{keyWord:this.state})
      http(apiPath.account.departmentList, params, "GET").then(res => {
        console.log(res)
        this.tableData=res.result.records
        this.page.total=res.result.total
        this.loading=false
      }).catch(err => {

        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    fuzzyDepartment(){
      var params = Object.assign({keyWord:''},{size:50})
      http(apiPath.account.fuzzyDepartment, params, "GET").then(res => {
        console.log(res)
      }).catch(err => {
        this.$message({
          message: err.message.content,
          type: 'error'
        });
      })
    },
    fresh(){
      this.getDepartmentList()
    },
    openDeleteDialog(row){
      var params=Object.assign({departmentId:row.id})
      // let params= new URLSearchParams()
      // params.append('departmentId',  row.id)
      // console.log("id"+row.id)
      this.$refs['deleteDepartment'].toggle('部门',row.name,apiPath.account.departmentDelete,params,'')
    },
    goBread(item){
      var length=0
      this.page.pid=item.pid
      this.getDepartmentList()
      for(var index in this.page.bread){
        console.log(index.pid)
        if(item.pid==this.page.bread[index].pid){break;}
        length++
      }
      console.log(length)
      this.page.bread.splice(length+1,this.page.bread.length-length+1)
    }
  },
})
