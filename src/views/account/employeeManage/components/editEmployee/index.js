import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        id:'',            //ID
        employeeNo: '',    //工號
        name: '',         //姓名
        jobName: '',      //部門
        departmentName: '',           //職位
        birth: ''              //入職時間
      },
    };
  },
  methods: {
    handleClose() {

    },
    updateEmployee() {
      let params = Object.assign({ 
        id: this.form.id,            //ID
        employeeNo: this.form.employeeNo,    //工號
        name: this.form.name,         //姓名
        jobName: this.form.jobName,      //部門
        departmentName: this.form.departmentName,           //職位
        entryTime: this.form.birth             //入職時間
      })
      http(apiPath.employee.edit, params, "POST").then(res => {
        console.log(res)
        if (res.result == true) {
          this.$message.success("修改成功");
          this.$parent.loadAll();
          this.dialogVisible = false;
        } else {
          this.$message.error(res.result);
        }
      })
    },
    toggle(row) {
      this.form.id = row.id,
      this.form.employeeNo = row.employeeNo,
      this.form.name = row.name,
      this.form.jobName = row.jobName,
      this.form.departmentName = row.departmentName,
      this.form.birth = row.birth,
      this.dialogVisible = !this.dialogVisible
    }
  }
}
