import {
  http,
  apiPath
} from "@/api"
export default {
  data() {
    return {
      dialogVisible: false,
      form: {
        // id: '',            //ID
        employeeNo: '123',    //工號
        name: '123',         //姓名
        jobName: '财务部',      //部門
        departmentName: '所有部门',           //職位
        birth: ''              //入職時間
      },
    };
  },
  methods: {
    addEmployee() {
      let params = Object.assign({            
        employeeNo: this.form.employeeNo,    //工號
        name: this.form.name,         //姓名
        jobName: this.form.jobName,      //部門
        departmentName: this.form.departmentName,           //職位
        entryTime: this.form.birth             //入職時間
      })
      http(apiPath.employee.add, params, "POST").then(res => {
        console.log(res)
        if (res.result == true) {
          this.$message.success("新增成功");
          this.$parent.loadAll();
          this.dialogVisible = false;
        } else {
          this.$message.error(res.result);
        }
      })
    },
    handleClose() {

    },
    toggle(){
      this.dialogVisible=!this.dialogVisible
    }
  }
}
