import {
  http,
  apiPath
} from "@/api"
import BreadCrumb from "./breadcrumb/index.vue"
import HomeHeader from "./header/index.vue"
import HomeLeftMenu from "./leftMenu/index.vue"
import HomeTopMenu from "./topMenu/index.vue"
import VuexHome from "mixins/vuex/home.js"
import VuexPermission from "mixins/vuex/permission"
import Socket from "common/socket/socket"
// import MaterielPage from "@/views/materiel/component/materielPage/index.vue"
import {BaseWsUrl as wsUrl} from "@/api/url"

export default {
  mounted(){

  },
  props: {},
  components: {
    HomeHeader,
    HomeLeftMenu,
    HomeTopMenu,
    BreadCrumb,
    // MaterielPage
  },
  mixins: [VuexHome, VuexPermission],
  data() {
    return {
      name: 'Home',
      show: true,
      menuList: [
        // {
        //   name:"生产管理",
        //   index:"/production",
        // },
        // {
        //   name:"物料管理",
        //   index:"/materiel",
        // },
        // {
        //   name:"财务管理",
        //   index:"/finance",
        // },
        // {
        //   name:"审核页面",
        //   index:"/examine",
        // },
        // {
        //   name:"账号管理",
        //   index:"/account",
        //   children: [
        //     {
        //       name: "员工角色管理",
        //       index:"/account/employeeRole"
        //     },
        //     {
        //       name: "角色权限管理",
        //       index:"/account/rolePower"
        //     }
        //   ]
        // },
      ],
      leftMenuList: [],
      defaultActiveUrl: "",
      tabList: [],
      tabActiveName: 'default',
      tabFirstLabel: "",
    }
  },
  computed: {},
  created() {
    this.init()
  },
  watch: {
    "$route": {
      handler: function (val) {
        this.breadCrumbSet(val);
        if (val.meta.name) {
          this.show = true;
        } else {
          this.show = false;
        }
      },
      immediate: true
    }
  },
  methods: {
    init() {
      // 获取基础配置数据(菜单列表等) TODO 后端提供获取当前用户菜单接口，此处需要控制菜单，每次刷新会重置权限
      // http(apiPath.mock.getConfig, {}, "GET").then(res => {
      //   console.log("res", res)
      //   this.menuStateSet(res.list);
      // }).catch(err => {
      //   this.$message({
      //     message: err.message.content,
      //     type: 'error'
      //   });
      // })
      let getStorage = localStorage.getItem("user-loginInfo");
      const localStroage = getStorage && JSON.parse(getStorage) || {};
      let {access_token = "", expired_time = 0} = localStroage;
      if (!access_token || expired_time < new Date().getTime()) {
        console.log("access_token已过期")
        this.$router.push('/login');
      } else {
        console.log("TODO getUserInfo")
        // 直接获取用户信息
        http(apiPath.mock.getUserInfo, {}, "GET").then(res => {
          this.loginInfoSet(Object.assign({}, {id: res.id, username: res.username}));
          // this.permissionStateSet(res.power);
          let curTime = new Date().getTime();
          // 增加过期时间
          let expired_time = curTime + 30 * 60 * 1000;
          JSON.stringify({
            username: res.username,
            access_token: res.access_token,
            sig_secret: res.sig_secret,
            time: curTime,
            expired_time
          });
          // localStorage.setItem('user-loginInfo', info);
          // 发起wsSocket链接请求
          wsUrl && new Socket(wsUrl);
        }).catch(err => {
          this.$message({
            message: err.message.content,
            type: 'error'
          });
        })
      }
      this.tabActive("/"+this.$route.path.split("/")[1])
    },
    // eslint-disable-next-line no-unused-vars
    tabActive(key) {
      this.tabActiveName = 'default';
      console.log("menuStateGet:",this.menuStateGet)
      let currentMenu = this.menuStateGet.find(e=>e.index===key)
      if(currentMenu) {
        this.tabFirstLabel = currentMenu.name;
        // this.leftMenuList = currentMenu.children ? currentMenu.children : [];
        this.leftMenuList = [currentMenu];
        console.log("leftMenuList:",this.leftMenuList)
      }
    },
    // eslint-disable-next-line no-unused-vars
    clickTab(tab, event) {
    },
    removeTab(targetName) {
      let tabs = this.tabList;
      let activeName = this.tabActiveName;
      tabs.forEach((tab, index) => {
        if (String(index) === targetName && String(index) === this.tabActiveName) {
          activeName = 'default';//如果删除了已选中的，则跳转到第一个界面
        }
      });
      this.tabActiveName = activeName;
      this.tabList = tabs.filter((tab, index) => String(index) !== targetName);
    },
    addTab(name) {
      // 若没有产生该标签则发生下面的事件
      const pathKey = this.$tabMenu.find(e => e.name === name);

      this.tabList.push(
        {
          name: pathKey.name,
          component: pathKey.name,
          describe: pathKey.describe,
          number: pathKey.number
        }
      );
      console.log("this is addTab. tabList:",this.tabList)
      this.tabActiveName = String(this.tabList.length - 1);

      console.log(this.tabActiveName);
    }
  }
}
