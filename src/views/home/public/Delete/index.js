import {http} from "@/api";

export default{
  data(){
    return{
      dialogVisible: false,
      kind:'',
      deletewhat:'',
      confirmed:'true',
      url:{},
      params:{},
      message:''
    }
  },
  methods: {
    getKind(kind){
      this.kind=kind;
    },
    getDeletewhat(deletewhat){
      this.deletewhat=deletewhat
    },
    getMessage(message){
      this.message=message
    },
    toggle(kind,deletewhat,url,params,message){
      this.dialogVisible=!this.dialogVisible
      this.url=url
      this.params=params
      this.getKind(kind)
      this.getDeletewhat(deletewhat)
      this.getMessage(message)
    },
    deleted(){

      http(this.url, this.params , "POST",'f').then(res => {
        console.log(res)
        this.url={}
        this.params={}
        this.$message({
          message: "err.message.content",
          type: 'success'
        });
        this.$parent.fresh()
        this.dialogVisible=false
        this.$message({
          message: res.message,
          type: 'success'
        });
      }).catch(err => {
        // console.log(err)
        this.$message({
          message: err.message,
          type: 'error'
        });
      })
    }
  }
}