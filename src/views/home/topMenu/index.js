import created from "mixins/created.js"
import {apiPath, http} from "@/api";
import vuexHome from "mixins/vuex/home"

export default created({
  props :{
    tabActive: Function
  },
  mixins: [vuexHome],
  data () {
    return {
      menuList: [
        {
        /*  name:"采购管理",
          index:"/purchase",
        },
        {*/
          name:"生产管理",
          index:"/production",
        },
        {
          name:"物料管理",
          index:"/materiel",
        },
        {
          name:"财务管理",
          index:"/finance",
        },
        {
          name:"审核页面",
          index:"/examine",
        },
        {
          name:"账号管理",
          index:"/account",
          children: [
            {
              name: "员工角色管理",
              index:"/account/employeeRole"
            },
            {
              name: "角色权限管理",
              index:"/account/rolePower"
            }
          ]
        },
      ],
      defaultActiveUrl: "",
      tabList: [],
      tabActiveName: 'default',
      tabFirstLabel: "",
    }
  },
  created () {
    console.log("permissionStateGet: ",this.permissionStateGet)
    this.init();
  },
  methods: {
    init(){
      this.defaultActiveUrl = this.$route.path;
      console.log("defaultActiveUrl:",this.defaultActiveUrl)
    },
    // eslint-disable-next-line no-unused-vars
    handleSelect(key, keyPath) {
      console.log("key:",key)
      this.$router.push({path: key});
      console.log(this.$props)
      this.$props.tabActive(key)
      // this.tabActiveName = 'default';
      // this.tabFirstLabel = this.menuList.find(e=>e.index===key).name;
    },
    // eslint-disable-next-line no-unused-vars
    clickTab(tab, event) {
    },
    removeTab(targetName) {
      let tabs = this.tabList;
      let activeName = this.tabActiveName;
      tabs.forEach((tab, index) => {
        if (String(index) === targetName && String(index) === this.tabActiveName) {
          activeName = 'default';//如果删除了已选中的，则跳转到第一个界面
        }
      });
      this.tabActiveName = activeName;
      this.tabList = tabs.filter((tab, index) => String(index) !== targetName);
    },
    addTab(name) {
      console.log(name);
      //若没有产生该标签则发生下面的事件
      // const pathKey = this.$tabMenu.find(e => e.name === name);
      // this.tabList.push(
      //   {
      //     name: pathKey.name,
      //     component: pathKey.name,
      //     describe: pathKey.describe,
      //     number: pathKey.number
      //   }
      // );
      // this.tabActiveName = String(this.tabList.length - 1);
    },
    logout() {
      http(apiPath.mock.logout,{},'GET').then(()=>{
        localStorage.setItem('user-loginInfo',"{}");
        this.loginInfoSet({});
        console.log('clear');
        this.$router.push('/login');
      }).catch(()=>{
        console.log('falied');
      })
    }
  }
})
