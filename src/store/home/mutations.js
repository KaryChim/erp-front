const mutations = {
  breadCrumbSet(state,data){
    state.breadCrumbState = data;
  },
  loginInfoSet(state,data){
    state.loginInfoState = data;
  }
}

export default mutations;