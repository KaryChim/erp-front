import {returnState} from "../util";
const getters = {
  layoutMenusGet(state){
    let layoutMenus = returnState(state, 'menus');
    return layoutMenus;
  },
  userNameGet(state){
    let userInfo = returnState(state, 'username');
    return userInfo;
  },
  accessTokenGet(state){
    let userInfo = returnState(state, 'access_token');
    console.log("access_token",userInfo)
    return userInfo;
  },
  breadCrumbGet:state=>state.breadCrumbState,
  loginInfoGet:state=>state.loginInfoState
}

export default getters
