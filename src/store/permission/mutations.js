import {addChildren} from "../util"

const mutations = {
  permissionStateSet(state, data) {
    let permissionState = {};
    data.forEach(el => {
      let {path} = el;
      permissionState[path] = el;
    })

    localStorage.setItem("user-loginInfo-premission", JSON.stringify(permissionState))
    state.permissionState = permissionState;
  },

  menuStateSet(state, data) {
    let menuState = [];
    data.forEach(el => {
      // let {path,name} = el;
      addChildren(el)
      menuState.push(el)
    })
    localStorage.setItem("user-loginInfo-menu", JSON.stringify(menuState))
    state.menuState = menuState;
  }
}

export default mutations;
