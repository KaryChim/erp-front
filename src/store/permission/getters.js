const getters = {
  permissionStateGet:state=>{
    let {permissionState} = state;
    if(Object.keys(permissionState).length){
      return permissionState;
    }else{
      return JSON.parse(localStorage.getItem("user-loginInfo-premission"));
    }
  },
  menuStateGet:state=>{
    let {menuState} = state;
    console.log("menuState");
    console.log(menuState);
    if(menuState.length){
      return menuState;
    }else{
      return JSON.parse(localStorage.getItem("user-loginInfo-menu"));
    }
  }
}

export default getters
