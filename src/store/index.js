import Vue from 'vue'
import Vuex from 'vuex'
import Template from './template'
import Home from './home'
import Permission from './permission'



Vue.use(Vuex)


export default new Vuex.Store({
  modules: {
    Template,
    Home,
    Permission
  }
});
