/*
 * @Author: chiachan163
 * @Date: 2020/10/18 3:18 上午
 * @Last Modified by: chiachan163
 * @Last Modified time: 2020/10/18 3:18 上午
 */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/home/index.vue'
import Login from '@/views/login/index.vue'
import NoRoute from '@/views/no-route/index.vue'
// import User from './user'
//import Purchase from './purchase'
import Production from './production'
import Materiel from './materiel'
import Finance from './finance'
import Examine from './examine'
import Account from './account'


Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    component: Home,
    children: [
      {
        path:"/",
        redirect: '/login',
      },
      //Purchase,
      Materiel,
      Production,
      Finance,
      Examine,
      Account,
    ],
  },
  {
    path: "/login",
    component: Login
  },
  {
    path: '/404',
    title: '404',
    component: NoRoute
  },
  // {
  //   path:"*",
  //   redirect: '/404',
  // },
]


const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

const router = new VueRouter({
  routes,
});


export default router;
