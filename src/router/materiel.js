const Materiel = resolve => require.ensure([], () => resolve(require('@/views/materiel/index.vue')), 'user')

export default {
  path: "/materiel",
  component: Materiel,
  children:[],
  meta:{
    name:"物料管理"
  }
}
