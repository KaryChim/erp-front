// import Route from '@/views/route/index.vue'
const Purchase = resolve => require.ensure([], () => resolve(require('@/views/purchase/index.vue')), 'user')

export default {
  path: "/purchase",
  component: Purchase,
  children:[],
  meta:{
    name:"采购管理"
  }
}
