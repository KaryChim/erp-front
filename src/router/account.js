import Route from '@/views/route/index.vue'
const DepartmentManage = resolve => require.ensure([], () => resolve(require('@/views/account/departmentManage/index.vue')), 'user')
const EmployeeManage = resolve => require.ensure([], () => resolve(require('@/views/account/employeeManage/index.vue')), 'user')
const MenuManage = resolve => require.ensure([], () => resolve(require('@/views/account/menuManage/index.vue')), 'user')
const PowerManage = resolve => require.ensure([], () => resolve(require('@/views/account/powerManage/index.vue')), 'user')
const RoleManage = resolve => require.ensure([], () => resolve(require('@/views/account/roleManage/index.vue')), 'user')

export default {
  path: "/account",
  component: Route,
  children:[
    {
      path:"/",
      redirect: '/account/departmentManage',
    },
    {
      path:"/account/departmentManage",
      component: DepartmentManage,
      meta:{
        name:"员工角色管理"
      }
    },
    {
      path:"/account/employeeManage",
      component: EmployeeManage,
      meta:{
        name:"角色权限管理"
      }
    },
    {
      path:"/account/menuManage",
      component: MenuManage,
      meta:{
        name:"员工角色管理"
      }
    },
    {
      path:"/account/powerManage",
      component: PowerManage,
      meta:{
        name:"角色权限管理"
      }
    },
    {
      path:"/account/roleManage",
      component: RoleManage,
      meta:{
        name:"员工角色管理"
      }
    },
  ],
  meta:{
    name:"账号管理"
  }
}
