export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}

const material={
  bomTypeList:'warehouse/bom/typeList',
  bomGetByTypeId: (...args)=> "warehouse/bom/getByTypeId" + pathUrl(args),
  bomDelete:'warehouse/bom/delete',
  materialByKeyWord:'material/byKeyword',
  //bomField:'/warehouse/bom/bomField',
  bomCreate:"warehouse/bom/create",
  bomUpdate:"warehouse/bom/update",
  bomById: "warehouse/bom/byId",
  bomGetTreeById:"warehouse/bom/getTreeById",
  bomAddType: "warehouse/bom/addType",
  bomExistByTypeId:"warehouse/bom/bomExistByTypeId",
  bomDeleteType:"warehouse/bom/deleteType",
  bomUpdateTypeInfo:"warehouse/bom/updateTypeInfo",
  //bills
  billCreateBills:'bills/createBills',
  billDocumentNo:"bills/documentNo",
  setDocumentNo:"task/setDocumentNo",
  getPickingCreateInfo:"bills/getPickingCreateInfo",
  stockInfo:"bills/stockInfo",
}

export default material;
