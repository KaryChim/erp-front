export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}

const employee = {
  employeeList: (...args) => "employee/list" + pathUrl(args),//获取下拉框模糊查询的工号与姓名
  //list: (...args) => "employee/list" + pathUrl(args),//获取下拉框模糊查询的工号与姓名
  employeeRoles: "role/list",     //分页查询角色信息
  list: "employee/list" ,                       //分页查询员工信息
  edit: "employee/editEmployee"  ,      //管理员修改员工信息
  add:"employee/createEmployee",
  delete: "employee/delete", //删除员工
  roledelete: "role/delete", //删除角色
  roleadd: "role/add",
  getEmployeeHoldRoles:"employee/getEmployeeHoldRoles",
  deleteEmployeeHoldRoles:"employee/deleteEmployeeHoldRoles",
  addEmployeeHoldRoles:"employee/addEmployeeHoldRoles",
  fuzzy:"employee/fuzzy"
}

export default employee;
