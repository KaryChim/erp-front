export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}
const product={
  rawMaterialByPlanNo:"product/rawMaterialByPlanNo",
  rawMaterialField:"product/rawMaterialField",
  finishedProductField:"product/finishedProductField",
  productList:"product/productList",
  materialBom:"product/materialBom",
  materialListForBomByCode:"product/productBom"
}
export default product;
