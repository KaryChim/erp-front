export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}
const plan={
  list:(...args)=>"plan/list"+ pathUrl(args),
  create: "plan/create",
  copy: 'plan/copy',
  delete: "plan/delete",
  detail:"plan/detail",
  plan:arg => "plan/"+arg,
  update:"plan/update"

}
export default plan;
