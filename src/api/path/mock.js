const path = {
  login: "demo/login",
  logout: "demo/logout",
  list:"demo/list",
  detail:"demo/detail",
  getUserInfo:"demo/user/get_user_info",
  getConfig:"demo/power/get_menu",
}
export default path;
