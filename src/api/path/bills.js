export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}
const bills={
  allBills:  (...args)=> "/bills/allBills" + pathUrl(args),
  documentNo: "/bills/documentNo",

}
export default bills;
