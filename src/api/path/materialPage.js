export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}
const materialPage = {
  list: "material/list",
  byKeyword: "material/byKeyword",
  groupList: "materialGroup/list",
  add: "material/add",
  unit: "base/unit/fuzzy",
  unitType: "base/unitType/fuzzy",
  byId: "material/byId",
  byMaterialId: "material/logistics/byMaterialId",
  materialModify: "material/modify",
  delete: "material/delete",
  logisticsModify: "material/logistics/modify",
  groupCreate: "materialGroup/create",
  groupDelete: "materialGroup/delete",
  groupModify: "materialGroup/modify",
  groupGetByName: "materialGroup/getByName",
}
export default materialPage ;
