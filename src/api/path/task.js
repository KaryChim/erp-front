export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}
const task={
  list:(...args)=>"task/list"+ pathUrl(args),
  setDocumentNo:"task/setDocumentNo",
  accomplish:"task/accomplish",
  pause:"task/pause",
  createTask:"task/create",
  start:"task/start",
  revocation:"task/revocation",
  delete:"task/delete",
  taskDetail:"task/detail",
  //以下是生产工序管理
  createPublicTask:"taskManage/create",
  deletePublicTask:"taskManage/delete",
  listTask:(...args)=> "taskManage/list" + pathUrl(args),
  publicTaskDetail:"taskManage/detail",
  publicTaskUpdate:"taskManage/update",
}
export default task;