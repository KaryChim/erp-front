export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}

const examine = {
  myPendingPlans: (...args) => "/examine/myPendingPlans" + pathUrl(args),
  plans: (...args) => "/examine/plans" + pathUrl(args),
  bomList: "/examine/bom/list",
  taskInfo: (...args) => "/examine/all/" + pathUrl(args),
  auditStatusPlans: "/examine/auditStatus/plans",
  billList: "/examine/bill/list",
  batchExamine: "examine/batchExamine",
  taskDetail: "/examine/detail",
  bomDetail: "examine/bom/byId",
  billsDetail: (...args) => "/examine/bill/bills" + pathUrl(args), //双击获取单据详情
}

export default examine;