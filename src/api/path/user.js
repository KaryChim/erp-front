export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}
const user = {
  login: 'login',
  logout: 'v1/public/do_logout',
  getUserInfo: 'v1/user/get_user_info',
}
const material = {
  bomTypeList:'warehouse/bom/typeList',
  bomGetByTypeId: (...args)=> "warehouse/bom/getByTypeId/" + pathUrl(args),
  bomDelete:'warehouse/bom/delete',
  materialByKeyWord:'material/byKeyword',
  list: (...args) => "warehouse/list" + pathUrl(args),
  create: "warehouse/create",
  delete: "warehouse/delete",
  exportWarehouseData: "warehouse/exportWarehouseData",
  modify: "warehouse/modify",
  materialInfo: "warehouse/materialInfo",
  materialDetail: "warehouse/materialDetail",
}
const fuzzy = {
  getByKeyWord:  (...args)=>'fuzzy/getByKeyWord/' + pathUrl(args),
  listWarehouseByKeyWord: "fuzzy/listWarehouseByKeyWord",
  listMaterialByKeyWord: "fuzzy/listMaterialByKeyWord",
}
const employee = {
  list: (...args) => "employee/list" + pathUrl(args),
}
const MaterialPage ={
  list: "material/list",
  byKeyword: "material/byKeyword",
  groupList: "materialGroup/list",
  add: "material/add",
  unit: "base/unit/fuzzy",
  unitType: "base/unitType/fuzzy",
  byId: "material/byId",
  byMaterialId: "material/logistics/byMaterialId",
  materialModify: "material/modify",
  delete: "material/delete",
  logisticsModify: "material/logistics/modify",
  groupCreate: "materialGroup/create",
  groupDelete: "materialGroup/delete",
  groupModify: "materialGroup/modify",
  groupGetByName: "materialGroup/getByName",
}
export default { user, material, fuzzy, employee,MaterialPage};
