export function pathUrl(args) {
  let pathParams = '';
  args.forEach(arg => pathParams += '/' + arg);
  return pathParams;
}

const fuzzy = {
  getByKeyWord:  (...args)=>'fuzzy/getByKeyWord/' + pathUrl(args),
  material:'fuzzy/material',
  listWarehouseByKeyWord:"fuzzy/listWarehouseByKeyWord",
  unit:'base/unit/fuzzy',
  fuzzyMaterial:'fuzzy/material',
  supplier:"base/supplier/fuzzy",
  courier:'base/courier/fuzzy',
  department:'department/fuzzy',
  customer:'base/customer/fuzzy',
  currency:'/fuzzy/currency',
  pickingPurpose:'base/pickingPurpose/fuzzy',
  taskName:"taskManage/fuzzy",
  planNameFuzzyQuery: 'fuzzy/planNameFuzzyQuery',
  warehouse: 'fuzzy/warehouse'
}
export default fuzzy;
