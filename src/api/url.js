let ENV = process.env.BUILD_ENV;
let devUrl = "https://qlserver.tyu.wiki/api";
// let devUrl = "https://hfserver.g2mtu.cn/api";
// let prodUrl = "https://qlserver.tyu.wiki/";
let prodUrl = "https://qlerp.tyu.wiki/api";
let devWsUrl = "";
let prodWsUrl = "let wsUrl = 'wss://qlserver.tyu.wiki/ws'";

const baseUrl = ENV == "production" ? prodUrl : devUrl;
export const BaseWsUrl = ENV == "production" ? prodWsUrl : devWsUrl;
export default baseUrl;
