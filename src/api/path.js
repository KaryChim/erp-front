// // api 接口目录
import User from "./path/user"
import Product from "./path/product"
import Plan from "./path/plan"
import Task from "./path/task"
import Permission from "./path/permission"
import Material from "./path/material"
import Fuzzy from "./path/fuzzy"
import Employee from './path/employee'
import Examine from "./path/examine"
import Bills from "./path/bills"
import MaterialPage from "./path/materialPage"
import Account from './path/account'

const path = {
  user:User,
  product:Product,
  plan:Plan,
  task:Task,
  permission:Permission,
  material:Material,
  bills:Bills,
  fuzzy:Fuzzy,
  employee:Employee,
  materialPage:MaterialPage,
  mock:{},
  examine: Examine,
  account:Account
}

if(process.env.BUILD_ENV == 'development'){
  const Mock = require("./path/mock")
  path.mock = Mock.default
}
export default path;
