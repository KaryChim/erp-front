import Permission from "./vuex/permission"
export default function (sfc) {
  sfc.mixins = sfc.mixins || [];
  sfc.mixins.push(Permission);
  return sfc;
}