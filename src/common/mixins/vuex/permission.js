import { createNamespacedHelpers } from 'vuex'
const { mapGetters,mapMutations } = createNamespacedHelpers('Permission');

export default  {
  computed:{
    ...mapGetters(["permissionStateGet","menuStateGet"]),
  },
  methods:{
    ...mapMutations(["permissionStateSet","menuStateSet"]),
  }
};
