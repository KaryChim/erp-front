import { createNamespacedHelpers } from 'vuex'
const { mapGetters,mapMutations } = createNamespacedHelpers('Home');

export default  {
  computed:{
    ...mapGetters(["breadCrumbGet","loginInfoGet","userNameGet","accessTokenGet"]),
  },
  methods:{
    ...mapMutations(["breadCrumbSet","loginInfoSet"]),
  }
};
