import messageEventHandle from "./message_event";
import User from "@/store/home"

let instance;

export const Mtype = {
  TypeCall: 1, // 客户端发送请求
  TypeReply: 2, // 服务端回复请求
  TypePush: 3, // 服务端主动推送
  TypeAuthCall: 4, // 客户端发起鉴权请求
  TypeAuthReply: 5 // 服务端回复鉴权请求
}

export default class Socket{
  constructor(url,socketHandler){
    if(instance) return instance;
    instance = this;
    this.url = url;
    // 业务处理方法
    this.socketHandler = socketHandler;
    // 保持心跳定时器
    this.keepLiveTimer = null;
    // 重连定时器
    this.reconnectTimer = null;
    // ws链接状态
    this.wsConnect = false;
    // ws开启后处理逻辑
    this.openHanlder = this.openEventHandler.bind(this);
    // ws信息接收处理逻辑
    this.messageHandler = this.messageEventHandler.bind(this);
    this.FileReader = new FileReader();
    // ws关闭处理逻辑
    this.closeHandler = this.closeEventHandler.bind(this);
    this.init();
  }
  init(){
    this.ws = new WebSocket(this.url);
    clearInterval(this.keepLiveTimer);
    clearInterval(this.reconnectTimer);
    this.ws.addEventListener('open', this.openHanlder);
    this.ws.addEventListener('message', this.messageHandler);
    this.ws.addEventListener('error', (error) => {
      console.warn("error", error);
    });
    this.ws.addEventListener('close', this.closeHandler);
  }
  openEventHandler(){
    this.wsConnect = true;
    // console.log('open');
    this.send({
      seq: 1,
      mtype: Mtype.TypeAuthCall,
      bodyCodec: 115,
      body: `access_token_=${User.state.access_token}&auth_type_=template_ws`,
    });
    this.keepLiveTimer = setInterval(() => {
      // console.log("heartbeat")
      this.send({
        seq: 1,
        mtype: 1,
        serviceMethod: "/heartbeat",
        bodyCodec: 115,
        meta: "hb_=20"
      });
    }, 30000)
  }
  messageEventHandler(event){
    try{
      if(event){
        // console.log(event)
        let blob = event.data;
        let Reader = new FileReader();
        Reader.onload = ()=>{
          let msg = JSON.parse(Reader.result);//内容就在这里
          // console.log('msg',msg)
          // 在这里对服务端推送过来的数据进行处理
          if (msg.body !== "") {
            this.replayHandle(msg.body);
            messageEventHandle(msg)
          }
        };
        Reader.readAsText(blob);
      }
    }catch(err){
      console.warn(err);
    }
  }
  closeEventHandler(){
    clearInterval(this.reconnectTimer);
    // console.log('close');
    let that = this;
    that.wsConnect = false;
    this.reconnectTimer = setInterval(() => {
      // console.log("reconnect")
      if (!that.wsConnect) {
        that.init()
      } else {
        clearInterval(that.reconnectTimer)
      }
    }, 2000)
  }
  send(json){
    let data = json;
    data = JSON.stringify(data);
    this.ws.send(data);
  }
  // 回复客户端消息,防止服务器重复推送内容
  replayHandle(body){
    console.log("replayHandle body:",body)
    let _body = JSON.parse(body);
    let {group_type,group_id,seq} = _body;
    if(group_type&&group_id&&seq){
      let params = Object.assign({},{ group_type: group_type,group_id:group_id,seq:seq,_uid:User.state.id })
      this.send({
        seq: 1,
        mtype: Mtype.TypeCall,
        bodyCodec: 115,
        serviceMethod:"/call_notice",
        body: JSON.stringify(params)
      })
    }
  }
}
