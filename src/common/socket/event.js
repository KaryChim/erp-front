import PubSub from 'pubsub-js';

/* eslint-disable */
let supportsPassive = false;
try {
  const opts = {};
  Object.defineProperty(opts, 'passive', {
    get() {
      supportsPassive = true;

    }
  });
  window.addEventListener('test-passive', null, opts);
} catch (e) {
}

export function on(target, event, handler, passive = false) {
  target.addEventListener(
    event,
    handler,
    supportsPassive ? {capture: false, passive} : false
  );
}

export function off(target, event, handler) {
  target.removeEventListener(event, handler);
}

export function prevent(e) {
  e.stopPropagation();
  e.nativeEvent && e.nativeEvent.stopImmediatePropagation();
}


function createEventBus() {
  // 注册事件的命名空间
  let nameSpace = {
    // ws开头的是基于webSocket的订阅/派发
    // template空间
    wsTemplate: {},
    // 刷新团队OKR
    wsUserInfoUpdate: {},
    // 重新全部流程
    wsClear: {}
  }
  // 声明eventBus
  let eventBus = {};
  let nameSpaceKeys = Object.keys(nameSpace);
  nameSpaceKeys.forEach(key => {
    let childKeys = Object.keys(nameSpace[key]);
    if (childKeys.length) {
      childKeys.forEach(childKey => {
        let fnkey = `${key}${childKey}`;
        injectFn(fnkey)
      })
    } else {
      injectFn(key)
    }
  })

  // 注入fn
  function injectFn(key) {
    eventBus[key] = {};
    eventBus[key].pub = (arg) => {
      PubSub.publish(key, arg)
    };
    eventBus[key].sub = (fn) => {
      return PubSub.subscribe(key, (_, message) => {
        fn(message)
      })
    };
    eventBus[key].unsub = (value) => {
      PubSub.unsubscribe(value)
    };
  }

  return eventBus;
}

export const eventBus = createEventBus();

// 节流函数
export const throttle = function (func, wait, options) {
  let context, args, result;
  let timeout = null;
  // 上次执行时间点
  let previous = 0;
  if (!options) options = {};
  // 延迟执行函数
  let later = function () {
    // 若设定了开始边界不执行选项，上次执行时间始终为0
    previous = options.leading === false ? 0 : Date.parse(new Date());
    timeout = null;
    result = func.apply(context, args);
    if (!timeout) context = args = null;
  };
  return function () {
    let now = Date.parse(new Date());
    // 首次执行时，如果设定了开始边界不执行选项，将上次执行时间设定为当前时间。
    if (!previous && options.leading === false) previous = now;
    // 延迟执行时间间隔
    let remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (args.length && args[0].eventPhase) {
      args[0].persist();
    }
    // 延迟时间间隔remaining小于等于0，表示上次执行至此所间隔时间已经超过一个时间窗口
    // remaining大于时间窗口wait，表示客户端系统时间被调整过
    if (remaining <= 0 || remaining > wait) {
      clearTimeout(timeout);
      timeout = null;
      previous = now;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
      //如果延迟执行不存在，且没有设定结尾边界不执行选项
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
};

// 防抖函数
export const debounce = (func, wait = 300) => {
  let timer;
  return function () {
    let context = this;
    let args = arguments;

    if (timer) clearTimeout(timer);

    timer = setTimeout(() => {
      func.apply(context, args)
    }, wait)
  }
}
