import {eventBus} from "common/socket/event"
import {Mtype} from "common/socket/socket"

// 调度器
class Scheduler{
  constructor(){
    // 队列执行
    this.eventStack = {
      // 用户
      user:[],
      // 其他事件
      event: [],
    };

    this.eventMap = {
      // template事件
      "template/deal": {
        id:1,
        func:()=>{
          // 使用订阅器派发
          eventBus.wsTemplate.pub();
          console.log('excute:执行template事件')
        },
      },
      // 刷新团队okr列表
      "user_info/update":{
        id:2,
        func:()=>{
          // 使用订阅器派发
          eventBus.wsUserInfoUpdate.pub();
          console.log('excute:更新当前用户信息')
        },
      },

    }
  }
  // 事件触发器
  trigger(type){
    let eventStack = this.eventStack[type];
    if(!eventStack.length) return;
    let eventStackStatic = JSON.parse(JSON.stringify(eventStack))
    // console.log('ws trigger:eventStack',eventStackStatic);
    eventStackStatic.forEach(task=>{
      let {target_uri:eventKey} = task;
      let excute = this.eventMap[eventKey];
      if(excute){
        // 执行任务
        excute.func();
        // 清空任务
        this.delete(type,task);
      }
    })
  }
  // 事件删除
  delete(type,item){
    if(!type||!item){
      console.warn('ws delete 缺少必要参数');
      return;
    }
    let eventStack = this.eventStack[type];

    eventStack.forEach((task,index)=>{
      if(task.seq===item.seq){
        eventStack.splice(index,1);
      }
    })
  }
  // 事件增加
  add(type,item){
    if(!type||!item){
      console.warn('ws add 缺少必要参数');
      return;
    }
    let eventStack = this.eventStack[type];
    let allowTag = true;
    let stackLength = eventStack.length;
    if(stackLength){
      // 队列存在 判断seq顺序,然后再加入队列  并且不是同一业务
      let lastItem = eventStack[stackLength-1];
      allowTag = item.seq > lastItem.seq && lastItem.target_uri!==item.target_uri;
    }
    allowTag && this.eventStack[type].push(item);
    // console.log(`ws add ${type} ${JSON.stringify(this.eventStack[type])}`)
  }
  run(){
    // 根据队列锁 每1000ms 执行一次
    // console.log('ws run')
    setTimeout(()=>{
      this.run();
    },1000)
    this.trigger('user');
  }
}

let wsScheduler = new Scheduler();
wsScheduler.run();

let messageEventHandle = (msg) => {
  switch (msg.mtype) {
  case Mtype.TypeCall:
    break;
  case Mtype.TypeReply:
    break;
  case Mtype.TypePush:
    // eslint-disable-next-line no-case-declarations
    let _body = JSON.parse(msg.body)
    // eslint-disable-next-line no-case-declarations
    let {group_type} = _body;
    wsScheduler.add(group_type,_body);
    break;
  case Mtype.TypeAuthCall:
    break;
  case Mtype.TypeAuthReply:
    break;
  default:
    break;
  }
}

export default messageEventHandle
