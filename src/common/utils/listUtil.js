/*
 * @Author: chiachan163
 * @Date: 2020/12/24 7:26 下午
 * @Last Modified by: chiachan163
 * @Last Modified time: 2020/12/24 7:26 下午
 * 列表处理工具栏
 */

/**
 将列表处理为树
 @param {any[]} list
 */
export function listToTree(list) {
// hash
  const map = {}
  for (const item of list) {
    map[item.id] = item
  }
  for (const item of list) {
    if (!(item.pid > 0)) {
      continue
    }
    const parent = map[item.pid]
    if (typeof parent.children === 'undefined') {
      parent.children = []
    }
    parent.children.push(item)
  }
  return list.filter(i => i.pid === 0)
}
