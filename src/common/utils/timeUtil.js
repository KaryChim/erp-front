function fillZero(number){
  return number > 9 ? number  : '0'+number;
}

export const goTimeTOJsTime = time=>{
  if(Number(time) && String(time).length==10){
    time = time*1000;
  }
  return time;
}

export const formatTime = (value, format) => {
  if (!value) {
    return '--';
  }
  value = goTimeTOJsTime(value);
  value = typeof value == 'string' ? value.replace(/-/g, '/') : value;
  let time = new Date(value)
  let str = format || 'yyyy-MM-dd';
  let Week = ['日', '一', '二', '三', '四', '五', '六'];

  str = str.replace(/yyyy|YYYY/, time.getFullYear());
  str = str.replace(/yy|YY/, fillZero(time.getYear() % 100));
  str = str.replace(/MM/, fillZero(time.getMonth() + 1));
  str = str.replace(/M/g, time.getMonth() + 1);
  str = str.replace(/w|W/g, Week[time.getDay()]);
  str = str.replace(/dd|DD/, fillZero(time.getDate()));
  str = str.replace(/d|D/g, time.getDate());
  str = str.replace(/hh|HH/, fillZero(time.getHours()));
  str = str.replace(/h|H/g, time.getHours());
  str = str.replace(/mm/, fillZero(time.getMinutes()));
  str = str.replace(/m/g, time.getMinutes());
  str = str.replace(/ss|SS/, fillZero(time.getSeconds()));
  str = str.replace(/s|S/g, time.getSeconds());
  return str;
};


export const formatCountDown = (time,type)=>{
  time = goTimeTOJsTime(time);
  let hours = fillZero(parseInt(time / 1000 / 60 / 60)); //计算剩余的小时
  let minutes = fillZero(parseInt(time / 1000 / 60 % 60));//计算剩余的分钟
  let seconds = fillZero(parseInt(time / 1000 % 60));//计算剩余的秒
  if(hours>99){
    return "--"
  }
  if(type=='minute'){
    // 仅返回分钟格式
    minutes = fillZero(parseInt(time / 1000 / 60));
    return `${minutes}:${seconds}`
  }
  return `${hours}:${minutes}:${seconds}`
}

export const formatJsTimeByTimeInt = (timeInt)=>{
  if(typeof timeInt === 'number'){
    let time = timeInt.toString()
    let reg = /^(\d{4})(\d{2})(\d{2})/
    return time.replace(reg,"$1-$2-$3")
  }
  return timeInt
}

export const deepClone = (obj)=>{
  if(typeof obj != 'object') return obj;
  return JSON.parse(JSON.stringify(obj))
}
