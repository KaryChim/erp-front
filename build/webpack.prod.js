/*
 * @Author: chiachan163
 * @Date: 2020/10/18 2:57 上午
 * @Last Modified by: chiachan163
 * @Last Modified time: 2020/10/18 2:57 上午
 */

const path = require('path');
const webpack = require('webpack');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin')
const merge = require("webpack-merge");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackConfigBase = require('./webpack.base.js');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HappyPack = require('happypack');
const os = require('os');
const happyThreadPool = HappyPack.ThreadPool({size: os.cpus().length});


const webpackConfigProd = {
  mode: "production",
  performance: {
    hints: false
  },
  output: {
    filename: 'js/[name].[chunkhash:8].js',
    publicPath: './',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "postcss-loader"
        ]
      },
      {
        test: /\.less$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader", "postcss-loader", "less-loader"
        ]
      },
    ],
  },
  optimization: {
    runtimeChunk: {
      name: "manifest"
    },
    splitChunks: {
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.(css|less)/,
          chunks: 'all',
          enforce: true,
        },
        commons: {
          name: 'commons',
          chunks: 'initial',
          minChunks: 2,
          reuseExistingChunk: true
        },
        nodeModules: {
          name(module) {
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
            return `chunk-node.${packageName.replace('@', '')}`;
          },
          test: /[\\/]node_modules[\\/]/,
          minChunks: 1,
          chunks: 'initial',
          priority: 1
        },
        elementUI: {
          name(module) {
            const packageName = module.context.match(/[\\/]node_modules[\\/]element-ui[\\/](.*)([\\/]|$)/)[1];
            return `chunk-element.${packageName.replace('@', '')}`;
          },
          minSize: 20000,
          priority: 20,
          minChunks: 1,
          chunks: 'all',
          test: /[\\/]node_modules[\\/]element-ui[\\/]/
        },
      },
    },
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[contenthash:8].css',
      chunkFilename: 'css/app.[contenthash:12].css'
    }),
    // 托管cdn
    new CopyWebpackPlugin([{
      from: path.resolve(__dirname, '../static/js'),
      to: path.resolve(__dirname, '../dist/static/js'),
    }]),
    new CopyWebpackPlugin([{
      from: path.resolve(__dirname, '../static/ueditor'),
      to: path.resolve(__dirname, '../dist/static/ueditor'),
    }]),
    new OptimizeCssAssetsPlugin({
      assetNameRegExp: /\.css$/g,
      cssProcessor: require('cssnano'),
      cssProcessorOptions: {discardComments: {removeAll: true}},
      canPrint: true,
    }),
    new webpack.HashedModuleIdsPlugin(),
    new HappyPack({
      id: 'babel',
      loaders: ['babel-loader?cacheDirectory'],
      threadPool: happyThreadPool,
    }),
    // new webpack.DllReferencePlugin({
    //     context: __dirname,
    //     manifest: require('../static/js/vendors-dll-manifest.json'),
    // }),
    new HtmlWebpackPlugin({
      chunksSortMode: 'none',
      filename: 'index.html',
      template: 'index.html',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      }
    })
  ],
}

module.exports = merge(webpackConfigBase, webpackConfigProd);
