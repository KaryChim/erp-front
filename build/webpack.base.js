/*
 * @Author: chiachan163
 * @Date: 2020/10/18 2:33 上午
 * @Last Modified by: chiachan163
 * @Last Modified time: 2020/10/18 2:33 上午
 */

var path = require('path');
var webpack = require('webpack');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const {VueLoaderPlugin} = require('vue-loader');


function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

function assetsPath(_path) {
  const assetsSubDirectory = 'static'
  return path.posix.join(assetsSubDirectory, _path)
}

module.exports = {
  entry: resolve('src/main.js'), // 项目的入口文件，webpack会从main.js开始，把所有依赖的js都加载打包
  output: {
    path: resolve('dist'), // 项目的打包文件路径
    // publicPath: '/dist/', // 通过devServer访问路径
    filename: '[name].js' // 打包后的文件名
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      '@': resolve('src'),
      'assets': resolve('src/assets'),
      'common': resolve('src/common'),
      'mixins': resolve('src/common/mixins'),
      'styles': resolve('src/common/styles'),
      'components': resolve('src/components')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: [
          {
            loader: 'vue-loader',
          }
        ]
      },
      {
        test: /\.js$/,
        include: [resolve('src')],
        exclude: /node_modules/,
        use: 'babel-loader?cacheDirectory',
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: assetsPath('img/[name].[hash:7].[ext]')
          }
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            name: assetsPath('media/[name].[hash:7].[ext]')
          }
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 10000,
            publicPath: "../",
            name: assetsPath('fonts/[name].[hash:7].[ext]'),
          }
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.BUILD_ENV': JSON.stringify(process.env.BUILD_ENV)//增加此行
    }),
    new VueLoaderPlugin(),
    new ProgressBarPlugin(),
  ],
  devServer: {
    historyApiFallback: true, //historyApiFallback设置为true那么所有的路径都执行index.html。
    overlay: true // 将错误显示在html之上
  }
};
