module.exports = {
  presets: [
    // "env",
    '@vue/cli-plugin-babel/preset',
    // "@babel/preset-env",
    // [
    //   "es2015",
    //   {
    //     "modules": false
    //   }
    // ]
  ],
  plugins: [
    [
      '@babel/plugin-transform-runtime',
      {
        corejs: false,
        helpers: true,
        regenerator: false,
        useESModules:false
      }
    ],
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-object-assign',
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ],
}
