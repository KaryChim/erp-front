module.exports = {
  devServer: {
    host: "0.0.0.0",
    port: 8070,
    https: false,
    hotOnly: false,
    proxy: null,
    disableHostCheck: true,
        
  },
  publicPath:'./' ,
  dev:{
    proxyTable:{
      '/apis':{
        target:'https://qlserver.tyu.wiki/',
        changeOrigin:true,
        pathRewrite:{
          '^/apis':''
        }
      }
    }
  }
}
